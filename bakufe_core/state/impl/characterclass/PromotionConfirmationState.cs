﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;

namespace bakufe_core.state.impl.characterclass
{
    /// <summary>
    /// This state asks for confirmation if the character shall attempt to promote into said class 
    /// and handles the promotion in case of a positive reply
    /// </summary>
    public class PromotionConfirmationState : ConfirmationState
    {
        private readonly CharacterClass selectedClass;
        private readonly int successChance;
        private readonly PromotionSelectionState selectionState;

        public PromotionConfirmationState(BakuFERealm realm, Character character, CharacterClass sClass, int chance, PromotionSelectionState previousState)
            : base(realm, character, IOPatterns.STATE_PROMOTION_CONFIRMATION_NAME)
        {
            selectedClass = sClass;
            successChance = chance;
            selectionState = previousState;

            Description = string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_DESCRIPTION, character.Name, selectedClass.Name, successChance);
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            //Push negative reply and return to the selection state
            io.PushEmptyLine();
            io.PushOutput(string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_NEGATIVE_REPLY, character.Name));
            io.PushEmptyLine();
            selectionState.ChangeToThisCharacterState(io);
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            //Remove 1 of the required seals from the characters inventory
            GenericItem requiredSeal = CharacterClassUtil.GetRequiredSealForClass(selectedClass);
            realm.RemoveItemFromCharacterInventory(io, character.ID, requiredSeal, 1);

            int rolledValue = realm.FERandom.Next(0, 100);
            if(rolledValue < successChance)
            {
                //Promotion success -> Add the new acquired class and ask if the player wants the character to change into it
                io.PushOutput(string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_POSITIVE_ATTEMPT, character.Name, selectedClass.Name));
                realm.AddAcquiredClassToCharacter(character.ID, selectedClass.CharacterClassKey);

                ChangeToNewClassState nextState = new ChangeToNewClassState(realm, character, selectedClass);
                nextState.ChangeToThisCharacterState(io);
            }
            else
            {
                //Promotion failure -> Change back to the idle state
                io.PushOutput(IOPatterns.STATE_PROMOTION_CONFIRMATION_NEGATIVE_ATTEMPT);
                ChangeCharacterToState(io, new IdleState(realm, character));
            }
        }
    }
}
