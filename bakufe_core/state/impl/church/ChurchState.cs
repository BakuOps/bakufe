﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace bakufe_core.state.impl.church
{
    /// <summary>
    /// This stage represents the church of Seiros
    /// 
    /// Here the character can pray, donate to the church or access the hospital where the character can be healed
    /// </summary>
    public class ChurchState : CharacterStateBase
    {
        public ChurchState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_CHURCH_NAME)
        {
            AllowedInput.Add(StateInputCommand.PRAY.command);
            AllowedInput.Add(StateInputCommand.DONATE.command);
            AllowedInput.Add(StateInputCommand.HOSPITAL.command);
            AllowedInput.Add(StateInputCommand.LEAVE.command);
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            //Build the description
            //First check if the current state is also the church state
            CharacterStateBase lastState = realm.GetCharactersCurrentState(character.ID);
            bool isCurrentlyInChurch = lastState.GetType() == typeof(ChurchState) || lastState.GetType() == typeof(PrayingState) || lastState.GetType() == typeof(ChurchDonationState);

            if (isCurrentlyInChurch)
            {
                Description += string.Format(IOPatterns.STATE_CHURCH_DESCRIPTION_START_REMAIN, character.Name);
            }
            else
            {
                Description += string.Format(IOPatterns.STATE_CHURCH_DESCRIPTION_START_ENTER, character.Name);
            }
            Description += string.Format(IOPatterns.STATE_CHURCH_DESCRIPTION_MAIN, character.Name);

            base.ChangeToThisCharacterState(io);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (inputCommand.key == StateInputCommand.LEAVE.key)
            {
                //Leave the church -> IdleState
                io.PushOutput(string.Format(IOPatterns.STATE_CHURCH_LEAVE, character.Name));
                IdleState idleState = new IdleState(realm, character);
                idleState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.PRAY.key)
            {
                PrayingState prayingState = new PrayingState(realm, character);
                prayingState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.DONATE.key)
            {
                ChurchDonationState donationState = new ChurchDonationState(realm, character);
                donationState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.HOSPITAL.key)
            {
                HospitalState hospitalState = new HospitalState(realm, character, this);
                hospitalState.ChangeToThisCharacterState(io);
            }
        }
    }
}
