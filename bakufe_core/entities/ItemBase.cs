﻿using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// Base class for the item
    /// </summary>
    public abstract class ItemBase
    {
        /// <summary>
        /// The id to reference what kind of Item this is
        /// </summary>
        public string ItemID { get; set; }

        /// <summary>
        /// The Item's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A brief description of the item that will be displayed to the user
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The current amount of items currently found in this stack
        /// </summary>
        public int CurrentStackSize { get; set; }

        /// <summary>
        /// The maximum amount of items of that type that can be stacked into a single inventory slot
        /// </summary>
        public int MaxStackSize { get; set; }


        public int Price { get; protected set; }

        /// <summary>
        /// Base constructor only needed for serialization and testing purposes, don't use this anywhere else!!!
        /// </summary>
        public ItemBase()
        {

        }

        protected ItemBase(string itemID, string name, string description, int maxStackSize, int price)
        {
            ItemID = itemID;
            Name = name;
            Description = description;
            MaxStackSize = maxStackSize;
            CurrentStackSize = 1;
            Price = price;
        }

        protected ItemBase(string itemID, string name, string description, int currentStackSize, int maxStackSize, int price) : this(itemID, name, description, currentStackSize, price)
        {
            MaxStackSize = maxStackSize;
        }



        /// <summary>
        /// Returns whether or not the item of this type is stackable or not
        /// </summary>
        /// <returns>whether or not the item of this type is stackable or not</returns>
        public bool IsStackable()
        {
            return MaxStackSize > 1;
        }

        public virtual int GetSellValue()
        {
            return Price / 2;
        }

        public virtual string GetItemStringRepresentation()
        {
            return Name;
        }

        public virtual string GetFullItemDescription()
        {
            return string.Format(IOPatterns.ITEM_ITEMBASE_FULL_DESCRIPTION, Description, CurrentStackSize, GetSellValue());
        }
    }
}
