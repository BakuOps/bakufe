﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.persistence.character;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace bakufe_test.persistence
{
    [TestFixture]
    public class CharacterPersistenceTextfileTest
    {

        [SetUp]
        public void CleanUpTestfile()
        {
            string pathToTestfile = CharacterPersistenceTextfile.DEFAULT_CHARACTER_PATH + ORIG_ID + CharacterPersistenceTextfile.FILE_TYPE;
            if (File.Exists(pathToTestfile))
            {
                File.Delete(pathToTestfile);
            }
        }


        private static readonly string TESTER_NAME = "Tester";
        private static readonly string TESTER_ID = "TesterID";
        private static readonly int TESTER_LEVEL = 3;
        private static readonly int TESTER_EQUIPPED_WEAPON_SLOT = 0;
        private static readonly int TESTER_EQUIPPED_ACCESSORY_SLOT = -1;
        private static readonly int TESTER_CURRENT_CLASS_INDEX = 0;
        private static readonly int TESTER_CURRENT_HP = 15;
        private static readonly int TESTER_CURRENT_EXP = 69;
        private static readonly int[] TESTER_ATTRIBUTES = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
        private static readonly WeaponHandling[] TESTER_WEAPONRANKS = new WeaponHandling[] { 
            new WeaponHandling(WeaponType.SWORD),
            new WeaponHandling(WeaponType.LANCE),
            new WeaponHandling(WeaponType.AXE, WeaponRank.C),
            new WeaponHandling(WeaponType.BOW),
            new WeaponHandling(WeaponType.GAUNTLET),
            new WeaponHandling(WeaponType.ANIMA),
            new WeaponHandling(WeaponType.DARK),
            new WeaponHandling(WeaponType.LIGHT)
        };
        private static readonly SkillKey[] TESTER_EQUIPPED_SKILLS = new SkillKey[]{SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
        private static readonly AcquiredCharacterClass TESTER_CLASS = new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false);
        private static readonly int TESTER_GOLD = 123;

        [Test]
        public void ConvertDataStringToCharacter_CorrectData()
        {
            string pathToTestfile = "characters/TesterID.bfec";
            string characterDataString = File.ReadAllText(pathToTestfile);

            CharacterPersistenceTextfile characterPersistenceTextfile = new CharacterPersistenceTextfile();

            CharacterLoadingResult characterResult = characterPersistenceTextfile.ConvertDataStringToCharacter(TESTER_ID, characterDataString);

            Assert.IsTrue(characterResult.Success);
            Assert.NotNull(characterResult.LoadedCharacter);
            Assert.AreEqual(0, characterResult.ErrorList.Count);

            Character character = characterResult.LoadedCharacter;

            Assert.AreEqual(TESTER_NAME, character.Name);
            Assert.AreEqual(TESTER_ID, character.ID);
            Assert.AreEqual(TESTER_LEVEL, character.Level);
            Assert.AreEqual(TESTER_EQUIPPED_WEAPON_SLOT, character.EquippedWeaponSlot);
            Assert.AreEqual(TESTER_EQUIPPED_ACCESSORY_SLOT, character.EquippedAccessorySlot);
            Assert.AreEqual(TESTER_CURRENT_HP, character.CurrentHP);
            Assert.AreEqual(TESTER_CURRENT_EXP, character.CurrentEXP);
            Assert.AreEqual(TESTER_ATTRIBUTES, character.Attributes);
            for(int i = 0; i < TESTER_WEAPONRANKS.Length; i++)
            {
                Assert.AreEqual(TESTER_WEAPONRANKS[i], character.WeaponRanks[i]);
            }
            Assert.AreEqual(TESTER_EQUIPPED_SKILLS, character.EquippedSkills);
            Assert.AreEqual(TESTER_GOLD, character.Gold);
            Assert.AreEqual(TESTER_CURRENT_CLASS_INDEX, character.CurrentClassIndex);
            Assert.IsTrue(character.AcquieredCharacterClasses.Count == 1);
            Assert.AreEqual(TESTER_CLASS, character.AcquieredCharacterClasses[0]);

            List<SkillKey> TesterLearnedSkills = new List<SkillKey>
            {
                SkillKey.MAG_PLUS,
                SkillKey.STR_PLUS
            };

            Assert.AreEqual(TesterLearnedSkills, character.LearnedSkills);

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            ironSword.RemainingUses = 27;
            Accessory speedRing = AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING);

            Assert.AreEqual(ironSword, character.Inventory[0]);
            Assert.AreEqual(speedRing, character.Inventory[3]);
        }

        private static readonly string ORIG_NAME = "OcDonutStell";
        private static readonly string ORIG_ID = "TotallyOriginal";
        private static readonly int ORIG_LEVEL = 5;
        private static readonly AcquiredCharacterClass ORIG_CLASS_0 = new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER), 60, true);
        private static readonly AcquiredCharacterClass ORIG_CLASS_1 = new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK), 45, false);
        private static readonly int ORIG_CURRENT_HP = 18;
        private static readonly int ORIG_CURRENT_EXP = 69;
        private static readonly int ORIG_CURRENT_CLASS_INDEX = 1;
        private static readonly int[] ORIG_ATTRIBUTES = ORIG_CLASS_1.Class.BaseAttributes;
        private static readonly WeaponHandling[] ORIG_WEAPONRANKS = new WeaponHandling[] { 
            new WeaponHandling(WeaponType.SWORD, WeaponRank.E),
            new WeaponHandling(WeaponType.LANCE, WeaponRank.E),
            new WeaponHandling(WeaponType.AXE, WeaponRank.E),
            new WeaponHandling(WeaponType.BOW, WeaponRank.E),
            new WeaponHandling(WeaponType.GAUNTLET, WeaponRank.E),
            new WeaponHandling(WeaponType.ANIMA, WeaponRank.C),
            new WeaponHandling(WeaponType.DARK, WeaponRank.D),
            new WeaponHandling(WeaponType.LIGHT, WeaponRank.B)
        };
        private static readonly SkillKey[] ORIG_EQUIPPED_SKILLS = new SkillKey[] { SkillKey.HP_PLUS, SkillKey.MAG_PLUS, SkillKey.DUMMY, SkillKey.UNARMED_COMBAT_PLUS, SkillKey.DUMMY };
        private static readonly int ORIG_GOLD = 1234;

        [Test]
        public void CompleteTest_Save_And_Load()
        {
            List<AcquiredCharacterClass> acquiredCharacterClasses = new List<AcquiredCharacterClass>();
            acquiredCharacterClasses.Add(ORIG_CLASS_0);
            acquiredCharacterClasses.Add(ORIG_CLASS_1);

            //Create the new and original character
            Character originalCharacter = new Character(
                ORIG_NAME,
                ORIG_ID,
                ORIG_LEVEL,
                ORIG_ATTRIBUTES,
                ORIG_CURRENT_CLASS_INDEX,
                ORIG_WEAPONRANKS,
                ORIG_EQUIPPED_SKILLS,
                acquiredCharacterClasses,
                ORIG_CURRENT_HP,
                ORIG_CURRENT_EXP,
                InventoryUtil.NO_SLOT_AVAILABLE,
                InventoryUtil.NO_SLOT_AVAILABLE,
                ORIG_GOLD
            );

            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            fireTome.RemainingUses -= 12;

            Weapon trainingGauntlets = WeaponCollection.GetWeaponByKey(WeaponKey.TRAINING_GAUNTLETS_PLUS);
            trainingGauntlets.RemainingUses -= 25;

            Accessory accessory = AccessoryCollection.GetAccessoryByKey(AccessoryKey.CRITICAL_RING);

            originalCharacter.Inventory[0] = fireTome;
            originalCharacter.Inventory[1] = accessory;
            originalCharacter.Inventory[2] = trainingGauntlets;

            InventoryUtil.EquipOrUnequipItem(originalCharacter, 0);
            InventoryUtil.EquipOrUnequipItem(originalCharacter, 1);

            //Persist them
            CharacterPersistenceTextfile characterPersistenceTextfile = new CharacterPersistenceTextfile();
            characterPersistenceTextfile.PersistCharacter(originalCharacter.ID, originalCharacter);

            //Now steal them!
            Character totallyDifferentCharacter = characterPersistenceTextfile.LoadCharacter(originalCharacter.ID);
            Assert.NotNull(totallyDifferentCharacter);

            //Compare them to the initial
            Assert.AreEqual(originalCharacter.Name, totallyDifferentCharacter.Name);
            Assert.AreEqual(originalCharacter.ID, totallyDifferentCharacter.ID);
            Assert.AreEqual(originalCharacter.Level, totallyDifferentCharacter.Level);
            Assert.AreEqual(originalCharacter.CurrentClassIndex, totallyDifferentCharacter.CurrentClassIndex);
            Assert.AreEqual(originalCharacter.CurrentHP, totallyDifferentCharacter.CurrentHP);
            Assert.AreEqual(originalCharacter.CurrentEXP, totallyDifferentCharacter.CurrentEXP);
            Assert.AreEqual(originalCharacter.EquippedWeaponSlot, totallyDifferentCharacter.EquippedWeaponSlot);
            Assert.AreEqual(originalCharacter.EquippedAccessorySlot, totallyDifferentCharacter.EquippedAccessorySlot);
            Assert.AreEqual(originalCharacter.LearnedSkills.Count, totallyDifferentCharacter.LearnedSkills.Count);
            Assert.AreEqual(originalCharacter.GetCurrentClass(), totallyDifferentCharacter.GetCurrentClass());
            Assert.AreEqual(originalCharacter.AcquieredCharacterClasses.Count, totallyDifferentCharacter.AcquieredCharacterClasses.Count);
            for(int i = 0; i < originalCharacter.AcquieredCharacterClasses.Count; i++)
            {
                Assert.AreEqual(originalCharacter.AcquieredCharacterClasses[i], totallyDifferentCharacter.AcquieredCharacterClasses[i]);
            }

            for (int i = 0; i < originalCharacter.Attributes.Length; i++)
            {
                Assert.AreEqual(originalCharacter.Attributes[i], totallyDifferentCharacter.Attributes[i]);
            }

            for (int i = 0; i < originalCharacter.WeaponRanks.Length; i++)
            {
                Assert.AreEqual(originalCharacter.WeaponRanks[i], totallyDifferentCharacter.WeaponRanks[i]);
            }

            for (int i = 0; i < originalCharacter.EquippedSkills.Length; i++)
            {
                Assert.AreEqual(originalCharacter.EquippedSkills[i], totallyDifferentCharacter.EquippedSkills[i]);
            }

            for (int i = 0; i < originalCharacter.Inventory.Length; i++)
            {
                Assert.AreEqual(originalCharacter.Inventory[i], totallyDifferentCharacter.Inventory[i]);
            }

            //Finally clean up the created save file
            CleanUpTestfile();
        }
    }
}
