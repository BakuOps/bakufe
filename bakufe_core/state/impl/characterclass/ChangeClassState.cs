﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.characterclass
{
    /// <summary>
    /// This state handles the player freely changing between the classes they have already acquired
    /// </summary>
    public class ChangeClassState : NumericInputState
    {
        /// <summary>
        /// Holding list of the classes available for the character to switch into, excluding the current one
        /// </summary>
        private readonly List<AcquiredCharacterClassValueHolder> availableClasses = new List<AcquiredCharacterClassValueHolder>();

        /// <summary>
        /// Standart state constructor
        /// </summary>
        /// <param name="realm">The realm this state takes place in</param>
        /// <param name="character">The character who this state belongs to</param>
        public ChangeClassState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_CLASS_CHANGE_NAME)
        {
            string descriptionItems = "";

            MinInputValue = 0;

            // Loop over the acquired classes and add them to the basket excluding the characters current one
            for (int i = 0; i < character.AcquieredCharacterClasses.Count; i++)
            {
                if(i != character.CurrentClassIndex)
                {
                    descriptionItems += string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_TEMPLATE, availableClasses.Count, character.AcquieredCharacterClasses[i].Class.Name);
                    availableClasses.Add(new AcquiredCharacterClassValueHolder(character.AcquieredCharacterClasses[i], i));
                }
            }

            //Not count - 1 since the remain option is added manually
            MaxInputValue = availableClasses.Count;

            descriptionItems += string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_NO_CHANGE, character.AcquieredCharacterClasses.Count - 1, character.GetCurrentClass().Name);
            Description = string.Format(IOPatterns.STATE_CLASS_CHANGE_DESCRIPTION, character.Name, descriptionItems);
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (availableClasses.Count == 0)
            {
                //In case there are no other classes to change into, put the character back into the idle state
                io.PushOutput(Description);
                io.PushOutput(string.Format(IOPatterns.STATE_CLASS_CHANGE_NO_CHANGE_POSSIBLE, character.Name));
                ChangeCharacterToState(io, new IdleState(realm, character));
            } 
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if(InputValue == MaxInputValue)
            {
                //Max Input value is always the stay in this class option -> Do nothing
                io.PushOutput(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_NO_CHANGE_DONE, character.Name, character.GetCurrentClass().Name));
            }
            else
            {
                //Otherwise change the character over to the new class
                realm.ChangeCharactersCurrentClass(io, character.ID, availableClasses[InputValue].listIndex);
            }

            ChangeCharacterToState(io, new IdleState(realm, character));
        }
    }

    /// <summary>
    /// Valueholder class to map the acquired character classes to their current index in the list
    /// </summary>
    class AcquiredCharacterClassValueHolder
    {
        public readonly AcquiredCharacterClass acquiredCharacterClass;
        public readonly int listIndex;

        public AcquiredCharacterClassValueHolder(AcquiredCharacterClass acquiredCharacterClass, int listIndex)
        {
            this.acquiredCharacterClass = acquiredCharacterClass;
            this.listIndex = listIndex;
        }
    }
}
