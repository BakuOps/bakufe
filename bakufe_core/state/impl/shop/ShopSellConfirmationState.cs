﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.shop
{
    /// <summary>
    /// This state is just a confirmation whether or not the character want's to buy the previously picked item
    /// </summary>
    public class ShopSellConfirmationState : ConfirmationState
    {
        private readonly Weapon weapon;
        private readonly ShopSellState followUpState;

        public ShopSellConfirmationState(BakuFERealm realm, Character character, Weapon weap, ShopSellState sellState)
            : base(realm, character, IOPatterns.STATE_SHOP_SELL_CONFIRMATION_NAME)
        {
            weapon = weap;
            followUpState = sellState;
            Description = string.Format(IOPatterns.STATE_SHOP_SELL_CONFIRMATION_DESCRIPTION, weapon.Name, weapon.GetSellValue());
            Description += IOPatterns.STATE_CONFIRM_YESNO_SUFFIX;
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            io.PushOutput(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DECLINE);
            ChangeCharacterToState(io, followUpState);
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            io.PushOutput(string.Format(IOPatterns.STATE_SHOP_SELL_CONFIRMATION_PURCHASE, character.Name, weapon.Name, weapon.Price));

            ItemBase[] inventory = character.Inventory;
            bool weaponRemoved = false;
            Weapon weap;
            for(int i = 0; i < inventory.Length; i++)
            {
                if(inventory[i] != null && i != character.EquippedWeaponSlot && inventory[i].GetType() == typeof(Weapon))
                {
                    weap = (Weapon)inventory[i];
                    if (weap.Equals(weapon))
                    {
                        inventory[i] = null;
                        weaponRemoved = true;
                        break;
                    }
                }
            }

            if (weaponRemoved)
            {
                character.Gold += weapon.GetSellValue();
                InventoryUtil.SortInventory(character);
            }

            realm.AutosaveCharacterIfActivated(character);

            List<Weapon> remainingSellableWeapons = InventoryUtil.GetWeaponsThatCanBeSold(character);
            if(remainingSellableWeapons.Count == 0)
            {
                ChangeCharacterToState(io, new ShopState(realm, character, this));
            }
            else
            {
                io.PushOutput(IOPatterns.STATE_SHOP_SELL_CONFIRMATION_KEEP_ON);
                ShopSellState sellState = new ShopSellState(realm, character);
                sellState.ChangeToThisCharacterState(io);
            }
        }
    }
}
