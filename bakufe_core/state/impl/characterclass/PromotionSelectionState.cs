﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System.Collections.Generic;

namespace bakufe_core.state.impl.characterclass
{
    /// <summary>
    /// Lists the classes that the character can promote into and offers a choice for them.
    /// The actual promotion will be handled in another state
    /// </summary>
    public class PromotionSelectionState : NumericInputState
    {
        //The list of classes available for the character to promote into
        private readonly List<CharacterClass> availableClasses = new List<CharacterClass>();
        //This dictionary holds the chances of the character promoting into said class
        private readonly Dictionary<CharacterClassKey, int> promtionChanceMap = new Dictionary<CharacterClassKey, int>();

        public PromotionSelectionState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_PROMOTION_NAME)
        {
            //Find all the available classes
            List<CharacterClass> potentialClasses = CharacterClassUtil.GetClassAvailableToBeAquired(character);

            string itemDescriptions = "";
            foreach (CharacterClass characterClass in potentialClasses)
            {
                //Calculate the chance of the character promoting into that class
                int promotionChance = CharacterClassUtil.GetClassExamSuccessChance(character, characterClass);
                if(promotionChance >= 30)
                {
                    //In case the success chance is less them 30, the character is unable to attempt the promotion
                    promtionChanceMap.Add(characterClass.CharacterClassKey, promotionChance);
                    itemDescriptions += string.Format(IOPatterns.STATE_PROMOTION_OPTION_TEMPLATE, availableClasses.Count, characterClass.Name, promotionChance, CharacterClassUtil.GetRequiredSealForClass(characterClass));
                    availableClasses.Add(characterClass);
                }
            }
            itemDescriptions += string.Format(IOPatterns.STATE_PROMOTION_OPTION_NO_CHANGE, availableClasses.Count);

            MinInputValue = 0;
            //Not count - 1 since the decline option is added manually
            MaxInputValue = availableClasses.Count;
            MaxInputValue = availableClasses.Count;
            Description = string.Format(IOPatterns.STATE_PROMOTION_DESCRIPTION, character.Name, itemDescriptions);
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (availableClasses.Count == 0)
            {
                //In case there are no other classes to change into, put the character back into the idle state
                io.PushOutput(Description);
                io.PushOutput(string.Format(IOPatterns.STATE_PROMOTION_NO_PROMOTION_AVAILABLE, character.Name));
                ChangeCharacterToState(io, new IdleState(realm, character));
            }
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (InputValue == MaxInputValue)
            {
                //Max Input value is always to back out of the selection -> Back to the idle state
                ChangeCharacterToState(io, new IdleState(realm, character));
            }
            else
            {
                //Check if the character carries the requires seal
                CharacterClass selectedClass = availableClasses[InputValue];
                GenericItem requiredSeal = CharacterClassUtil.GetRequiredSealForClass(selectedClass);
                bool doesCharacterPossesTheRequiredSeal = realm.DoesCharacterCarryItem(character.ID, requiredSeal.ItemID);
                if (doesCharacterPossesTheRequiredSeal)
                {
                    //If the character carries the seal -> Attempt the promotion
                    PromotionConfirmationState confirmState = new PromotionConfirmationState(realm, character, selectedClass, promtionChanceMap[selectedClass.CharacterClassKey], this);
                    confirmState.ChangeToThisCharacterState(io);
                }
                else
                {
                    //Character does not carry the required seal -> Back to the selection
                    io.PushEmptyLine();
                    io.PushOutput(string.Format(IOPatterns.STATE_PROMOTION_SEAL_MISSING, character.Name, requiredSeal.Name));
                    io.PushOutput(Description);
                }
            }
        }
    }
}
