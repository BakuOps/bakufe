﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum WeaponAttribute
    {
        MAX_USES = 0,
        MIGHT = 1,
        HIT = 2,
        CRIT = 3,
        RANGE_MIN = 4,
        RANGE_MAX = 5,
        WEIGHT = 6
    }
}
