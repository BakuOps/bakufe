﻿using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// Represents a character class the character has aquired via passing the exam or other ways.
    /// Holds the information of which CharacterClass object it is reffering to, how much experience 
    /// got collected for that class as well as the information if the class is mastered or not.
    /// </summary>
    public class AcquiredCharacterClass
    {
        /// <summary>
        /// The referenced CharacterClass object 
        /// </summary>
        public CharacterClass Class { get; private set; }

        /// <summary>
        /// The amount of exp collected for this class. Should not exceed the amount that is required to master the given CharacterClass
        /// </summary>
        public int CurrentClassExperience { get; private set; }

        /// <summary>
        /// Boolean flag representing if the class is mastered or not
        /// </summary>
        public bool IsMastered { get; private set; }

        /// <summary>
        /// Creates an Object for the given CharacterClass
        /// CurrentClassExperience = 0 and IsMastered = false
        /// </summary>
        /// <param name="cClass">The given CharacterClassObject</param>
        public AcquiredCharacterClass(CharacterClass cClass){
            Class = cClass;

            CurrentClassExperience = 0;
            IsMastered = false;
        }

        /// <summary>
        /// Creates an object with the given parameters
        /// </summary>
        /// <param name="cClass">The given CharacterClassObject</param>
        /// <param name="cClass">The given amount of experience collected</param>
        /// <param name="isMastered">The flag if the class was mastered or not</param>
        public AcquiredCharacterClass(CharacterClass cClass, int exp, bool isMastered)
        {
            Class = cClass;
            CurrentClassExperience = exp;
            IsMastered = isMastered;

            int expRequiredToMaster = ExperienceUtil.GetExperienceNeedToMasterClass(Class);
            if (CurrentClassExperience >= expRequiredToMaster)
            {
                CurrentClassExperience = expRequiredToMaster;
            }
        }

        /// <summary>
        /// Adds the given amount of experience to the class and checks if the the class is mastered or not afterwards
        /// </summary>
        /// <param name="expereinceToAdd">The given amount to add</param>
        public void AddExperienceToAcquiredClass(int expereinceToAdd)
        {
            if (!IsMastered)
            {
                int expereinceRequiredToMaster = ExperienceUtil.GetExperienceNeedToMasterClass(Class);
                CurrentClassExperience += expereinceToAdd;
                if(CurrentClassExperience >= expereinceRequiredToMaster)
                {
                    CurrentClassExperience = expereinceRequiredToMaster;
                    IsMastered = true;
                }
            }
        }

        public string GetCurrentClassExperienceRepresentation()
        {
            if (IsMastered)
            {
                return IOPatterns.CHARACTER_CLASS_MASTERED_REPRESENTATION;
            }
            else
            {
                return string.Format(IOPatterns.CHARACTER_CLASS_NOT_YET_MASTERED_PATTERN, CurrentClassExperience, ExperienceUtil.GetExperienceNeedToMasterClass(Class));
            }
        }

        public override bool Equals(object obj)
        {
            return obj is AcquiredCharacterClass @class &&
                   EqualityComparer<CharacterClass>.Default.Equals(Class, @class.Class) &&
                   CurrentClassExperience == @class.CurrentClassExperience &&
                   IsMastered == @class.IsMastered;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
