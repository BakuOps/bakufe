﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum Attribute
    {
        HP = 0,
        STR = 1,
        MAG = 2,
        DEX = 3,
        SPD = 4,
        LCK = 5,
        DEF = 6,
        RES = 7
    }
}
