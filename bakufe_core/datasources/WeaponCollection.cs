﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources
{
    public class WeaponCollection
    {
        private static readonly int TRAINING_WEAPON_BASE_PRICE = 200;
        private static readonly int IRON_WEAPON_BASE_PRICE = 300;
        private static readonly int IRON_ALTERNATIVE_WEAPON_BASE_PRICE = 350;
        private static readonly int STEEL_WEAPON_BASE_PRICE = 400;
        private static readonly int EXOTIC_WEAPON_BASE_PRICE = 800;
        private static readonly int RARE_WEAPON_BASE_PRICE = 1000;
        private static readonly double PLUS_PRICE_MODIFIER = 1.5;

        private static readonly Dictionary<WeaponKey, Weapon> WeaponDictionary = GetWeaponDictionary();

        public static Weapon GetWeaponByKey(WeaponKey key)
        {
            if (!WeaponDictionary.ContainsKey(key))
            {
                return null;
            } else
            {
                return WeaponDictionary[key].ShallowCopy();
            }
        }

        private static Dictionary<WeaponKey, Weapon> GetWeaponDictionary() => new Dictionary<WeaponKey, Weapon>() {
                //---------------------------------------------------
                // Swords
                //---------------------------------------------------
                { WeaponKey.TRAINING_SWORD, new Weapon(
                    "Training Sword",
                    "This simple sword is perfect for training purposes.",
                    WeaponKey.TRAINING_SWORD,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 3, 100, 0, 1, 1, 4},
                    new WeaponModification[] {},
                    TRAINING_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.TRAINING_SWORD_PLUS, new Weapon(
                    "Training Sword+",
                    "A reinforced training sword, for those who take their training seriously.",
                    WeaponKey.TRAINING_SWORD_PLUS,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 65, 3, 100, 0, 1, 1, 2},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(TRAINING_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.RAPIER, new Weapon(
                    "Rapier",
                    "A sword designed to exploit the weaknesses of armored and cavalry units.",
                    WeaponKey.RAPIER,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 40, 7, 95, 10, 1, 1, 5},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_ARMORED, WeaponModification.EFFECTIVE_AGAINST_CAVALRY},
                    EXOTIC_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.RAPIER_PLUS, new Weapon(
                    "Rapier+",
                    "An elegant reinforced rapier, put to good use against armored and cavalry units.",
                    WeaponKey.RAPIER_PLUS,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 45, 9, 95, 20, 1, 1, 5},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_ARMORED, WeaponModification.EFFECTIVE_AGAINST_CAVALRY},
                    GetPlusWeaponPrice(EXOTIC_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.IRON_SWORD, new Weapon(
                    "Iron Sword",
                    "A standard sword made of iron - simple but effective.",
                    WeaponKey.IRON_SWORD,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 40, 5, 90, 0, 1, 1, 5},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.IRON_SWORD_PLUS, new Weapon(
                    "Iron Sword+",
                    "Reinforced in the forge's flames, this iron sword shines with a new brilliance.",
                    WeaponKey.IRON_SWORD_PLUS,
                    WeaponType.SWORD,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 45, 6, 100, 0, 1, 1, 5},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(IRON_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.STEEL_SWORD, new Weapon(
                    "Steel Sword",
                    "A weighty steel sword that deals significant blows.",
                    WeaponKey.STEEL_SWORD,
                    WeaponType.SWORD,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 8, 85, 0, 1, 1, 10},
                    new WeaponModification[] {},
                    STEEL_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.STEEL_SWORD_PLUS, new Weapon(
                    "Steel Sword+",
                    "A reinforced steel sword made strong and remade stronger.",
                    WeaponKey.STEEL_SWORD_PLUS,
                    WeaponType.SWORD,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 55, 10, 85, 0, 1, 1, 10},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(STEEL_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.RUNIC_BLADE, new Weapon(
                    "Runic Blade",
                    "A blade with ancient runes carved into it, which glow ominously as it strikes down.",
                    WeaponKey.RUNIC_BLADE,
                    WeaponType.SWORD,
                    WeaponRank.A,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 30, 16, 90, 5, 1, 1, 9},
                    new WeaponModification[] {WeaponModification.ADAPTIVE},
                    RARE_WEAPON_BASE_PRICE
                    )
                },

                //---------------------------------------------------
                // Lances
                //---------------------------------------------------
                { WeaponKey.TRAINING_LANCE, new Weapon(
                    "Training Lance",
                    "This simple lance is perfect for training purposes.",
                    WeaponKey.TRAINING_LANCE,
                    WeaponType.LANCE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 4, 90, 0, 1, 1, 5},
                    new WeaponModification[] {},
                    TRAINING_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.TRAINING_LANCE_PLUS, new Weapon(
                    "Training Lance+",
                    "A reinforced training lance, for those who take their training seriously.",
                    WeaponKey.TRAINING_LANCE_PLUS,
                    WeaponType.LANCE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 65, 4, 90, 0, 1, 1, 3},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(TRAINING_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.IRON_LANCE, new Weapon(
                    "Iron Lance",
                    "A standard lance made of iron - simple but effective.",
                    WeaponKey.IRON_LANCE,
                    WeaponType.LANCE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 30, 6, 80, 0, 1, 1, 6},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.IRON_LANCE_PLUS, new Weapon(
                    "Iron Lance+",
                    "Reinforced in the forge's flames, this iron lance shines with a new brilliance.",
                    WeaponKey.IRON_LANCE_PLUS,
                    WeaponType.LANCE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 35, 7, 90, 0, 1, 1, 6},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(IRON_WEAPON_BASE_PRICE)
                    )
                },

                //---------------------------------------------------
                // Axes
                //---------------------------------------------------
                { WeaponKey.TRAINING_AXE, new Weapon(
                    "Training Axe",
                    "This simple axe is perfect for training purposes.",
                    WeaponKey.TRAINING_AXE,
                    WeaponType.AXE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 6, 80, 0, 1, 1, 6},
                    new WeaponModification[] {},
                    TRAINING_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.TRAINING_AXE_PLUS, new Weapon(
                    "Training Lance+",
                    "A reinforced training axe, for those who take their training seriously.",
                    WeaponKey.TRAINING_AXE_PLUS,
                    WeaponType.AXE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 65, 6, 80, 0, 1, 1, 4},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(TRAINING_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.IRON_AXE, new Weapon(
                    "Iron Axe",
                    "A standard axe made of iron - simple but effective.",
                    WeaponKey.IRON_AXE,
                    WeaponType.AXE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 45, 8, 70, 0, 1, 1, 7},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.IRON_AXE_PLUS, new Weapon(
                    "Iron Axe+",
                    "Reinforced in the forge's flames, this iron axe shines with a new brilliance.",
                    WeaponKey.IRON_AXE_PLUS,
                    WeaponType.AXE,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 45, 9, 75, 0, 1, 1, 7},
                    new WeaponModification[] {},
                    GetPlusWeaponPrice(IRON_WEAPON_BASE_PRICE)
                    )
                },

                //---------------------------------------------------
                // Bows
                //---------------------------------------------------
                { WeaponKey.TRAINING_BOW, new Weapon(
                    "Training Bow",
                    "This simple bow is perfect for training purposes.",
                    WeaponKey.TRAINING_BOW,
                    WeaponType.BOW,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 3, 90, 0, 2, 2, 5},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS},
                    TRAINING_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.TRAINING_BOW_PLUS, new Weapon(
                    "Training Bow+",
                    "A reinforced training bow, for those who take training seriously.",
                    WeaponKey.TRAINING_BOW_PLUS,
                    WeaponType.BOW,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 65, 3, 90, 0, 2, 2, 3},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS},
                    GetPlusWeaponPrice(TRAINING_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.IRON_BOW, new Weapon(
                    "Iron Bow",
                    "A standard bow made of iron - simple but effective.",
                    WeaponKey.IRON_BOW,
                    WeaponType.BOW,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 40, 6, 85, 0, 2, 2, 6},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    IRON_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.IRON_BOW_PLUS, new Weapon(
                    "Iron Bow+",
                    "Reinforced in the forge's flames, this iron bow shines with a new brilliance.",
                    WeaponKey.IRON_BOW_PLUS,
                    WeaponType.BOW,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 40, 7, 95, 0, 2, 2, 5},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    GetPlusWeaponPrice(IRON_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.STEEL_BOW, new Weapon(
                    "Steel Bow",
                    "A weighty steel bow that deals significant blows.",
                    WeaponKey.STEEL_BOW,
                    WeaponType.BOW,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 9, 80, 0, 2, 2, 11},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    STEEL_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.STEEL_BOW_PLUS, new Weapon(
                    "Steel Bow+",
                    "A reinforced steel bow made strong and remade stronger.",
                    WeaponKey.STEEL_BOW_PLUS,
                    WeaponType.BOW,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 11, 80, 0, 2, 2, 10},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    GetPlusWeaponPrice(STEEL_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.MINI_BOW, new Weapon(
                    "Mini Bow",
                    "A bow that can be used at close range.",
                    WeaponKey.MINI_BOW,
                    WeaponType.BOW,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 4, 90, 0, 1, 2, 3},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    IRON_ALTERNATIVE_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.MINI_BOW_PLUS, new Weapon(
                    "Mini Bow+",
                    "A reinforced mini bow.",
                    WeaponKey.MINI_BOW_PLUS,
                    WeaponType.BOW,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 55, 6, 90, 10, 1, 2, 3},
                    new WeaponModification[] {WeaponModification.EFFECTIVE_AGAINST_FLIERS },
                    GetPlusWeaponPrice(IRON_ALTERNATIVE_WEAPON_BASE_PRICE)
                    )
                },

                //---------------------------------------------------
                // Gauntets
                //---------------------------------------------------
                { WeaponKey.TRAINING_GAUNTLETS, new Weapon(
                    "Training Gauntlets",
                    "Simple gauntlets perfect for training purposes. Wielder strikes twice when initiating combat.",
                    WeaponKey.TRAINING_GAUNTLETS,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 70, 0, 90, 5, 1, 1, 1},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    TRAINING_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.TRAINING_GAUNTLETS_PLUS, new Weapon(
                    "Training Gauntlets+",
                    "Reinforced training gauntlets for serious training, wearer attacks twice when initiating combat.",
                    WeaponKey.TRAINING_GAUNTLETS_PLUS,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 90, 0, 100, 5, 1, 1, 1},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    GetPlusWeaponPrice(TRAINING_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.IRON_GAUNTLETS, new Weapon(
                    "Iron Gauntlets",
                    "Standard iron gauntlets-simple but effective. Wielder strikes twice when initiating combat.",
                    WeaponKey.IRON_GAUNTLETS,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 50, 1, 85, 5, 1, 1, 3},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    IRON_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.IRON_GAUNTLETS_PLUS, new Weapon(
                    "Iron Gauntlets+",
                    "Reinforced in the forge's flames, wearer attacks twice when initiating combat.",
                    WeaponKey.IRON_GAUNTLETS_PLUS,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 60, 1, 95, 5, 1, 1, 3},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    GetPlusWeaponPrice(IRON_WEAPON_BASE_PRICE)
                    )
                },
                { WeaponKey.STEEL_GAUNTLETS, new Weapon(
                    "Steel Gauntlets",
                    "Weighty steel gauntlets that deal heavy blows. Wielder strikes twice when initiating combat.",
                    WeaponKey.STEEL_GAUNTLETS,
                    WeaponType.GAUNTLET,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 60, 3, 80, 5, 1, 1, 5},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    STEEL_WEAPON_BASE_PRICE
                    )
                },
                { WeaponKey.STEEL_GAUNTLETS_PLUS, new Weapon(
                    "Steel Gauntlets+",
                    "Reinforced steel gauntlets remade stronger, wearer attacks twice when initiating combat.",
                    WeaponKey.STEEL_GAUNTLETS_PLUS,
                    WeaponType.GAUNTLET,
                    WeaponRank.D,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 70, 3, 85, 5, 1, 1, 5},
                    new WeaponModification[] {WeaponModification.BRAVE },
                    GetPlusWeaponPrice(STEEL_WEAPON_BASE_PRICE)
                    )
                },


                //---------------------------------------------------
                // Tomes - Anima
                //---------------------------------------------------
                { WeaponKey.FIRE, new Weapon(
                    "Fire",
                    "Basic fire magic.",
                    WeaponKey.FIRE,
                    WeaponType.ANIMA,
                    WeaponRank.E,
                    WeaponDamageType.MAGICAL,
                    new int[]{ 30, 5, 90, 0, 1, 2, 3},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },

                { WeaponKey.WIND, new Weapon(
                    "Wind",
                    "Basic wind magic.",
                    WeaponKey.WIND,
                    WeaponType.ANIMA,
                    WeaponRank.E,
                    WeaponDamageType.MAGICAL,
                    new int[]{ 30, 4, 100, 10, 1, 2, 2},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },

                { WeaponKey.THUNDER, new Weapon(
                    "Thunder",
                    "Basic thunder magic.",
                    WeaponKey.THUNDER,
                    WeaponType.ANIMA,
                    WeaponRank.E,
                    WeaponDamageType.MAGICAL,
                    new int[]{ 30, 6, 80, 5, 1, 2, 4},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },


                //---------------------------------------------------
                // Tomes - Dark
                //---------------------------------------------------
                { WeaponKey.MIASMA, new Weapon(
                    "Miasma",
                    "Basic dark magic.",
                    WeaponKey.MIASMA,
                    WeaponType.DARK,
                    WeaponRank.E,
                    WeaponDamageType.MAGICAL,
                    new int[]{ 30, 6, 80, 5, 1, 2, 5},
                    new WeaponModification[] {},
                    IRON_WEAPON_BASE_PRICE
                    )
                },

                //---------------------------------------------------
                // Tomes - Light
                //---------------------------------------------------
                { WeaponKey.NOSFERATU, new Weapon(
                    "Nosferatu",
                    "Restores HP equal to half of the damage dealt.",
                    WeaponKey.NOSFERATU,
                    WeaponType.LIGHT,
                    WeaponRank.E,
                    WeaponDamageType.MAGICAL,
                    new int[]{ 30, 3, 80, 5, 1, 2, 8},
                    new WeaponModification[] { WeaponModification.VAMPIRIC},
                    IRON_WEAPON_BASE_PRICE
                    )
                },

                //---------------------------------------------------------
                // "Unarmed weapons" - Replacements for unarmed characters
                //---------------------------------------------------------
                { WeaponKey.UNARMED, new Weapon(
                    "Fists",
                    "Who needs a weapon when you can punch people?",
                    WeaponKey.UNARMED,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 1000, 0, 120, 0, 1, 1, 0},
                    new WeaponModification[] {WeaponModification.BRAVE},
                    0
                    )
                },
                { WeaponKey.UNARMED_PLUS, new Weapon(
                    "Trained fists",
                    "Fearsome weapons, wielded by veterans of the art of brawling",
                    WeaponKey.UNARMED_PLUS,
                    WeaponType.GAUNTLET,
                    WeaponRank.E,
                    WeaponDamageType.PHYSICAL,
                    new int[]{ 1000, 3, 130, 3, 1, 1, 0},
                    new WeaponModification[] {WeaponModification.BRAVE},
                    0
                    )
                }
            };

        private WeaponCollection()
        {

        }

        private static int GetPlusWeaponPrice(int basePrice)
        {
            return (int)((double)basePrice * PLUS_PRICE_MODIFIER);
        }
    }
}
