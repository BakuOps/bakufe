﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_test.entities.util;
using bakufe_test.states;
using NUnit.Framework;
using System;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_test.entities
{

    [TestFixture]
    public class ExperienceTest : TestWithARealm
    {
        private static readonly string EXPERIENCE_TESTER_ID = "Experience";
        private static readonly string EXPERIENCE_TESTER_NAME = "ExperienceTester";
        private static readonly int EXPERIENCE_TESTER_CURRENT_EXP = 69;
        private static readonly int EXPERIENCE_TESTER_INITIAL_LEVEL = 1;
        private static readonly int EXPERIENCE_TESTER_INITIAL_HP = 20;
        private static readonly int EXPERIENCE_TESTER_INITIAL_STR = 8;
        private static readonly int EXPERIENCE_TESTER_INITIAL_MAG = 3;
        private static readonly int EXPERIENCE_TESTER_INITIAL_DEX = 6;
        private static readonly int EXPERIENCE_TESTER_INITIAL_SPD = 6;
        private static readonly int EXPERIENCE_TESTER_INITIAL_LCK = 4;
        private static readonly int EXPERIENCE_TESTER_INITIAL_DEF = 5;
        private static readonly int EXPERIENCE_TESTER_INITIAL_RES = 2;

        private static readonly int EXPERIENCE_TESTER_SOLDIER_EXPERIENCE = 45;

        [Test]
        public void AddExperienceToCharacter_AddRegularExperience()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);

            Assert.AreEqual(EXPERIENCE_TESTER_CURRENT_EXP, currentCharacter.CurrentEXP);

            int experienceToAdd = 10;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, experienceToAdd, 0, 0, WeaponType.SWORD);

            Assert.AreEqual(EXPERIENCE_TESTER_CURRENT_EXP + experienceToAdd, currentCharacter.CurrentEXP);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_LEVEL, currentCharacter.Level);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, experienceToAdd)));
        }

        [Test]
        public void AddExperienceToCharacter_AddExperienceLevelUp()
        {
            PredictableBakuFERandom bakuFERandom = new PredictableBakuFERandom();
            bakuFERandom.SequenceValues.Add(0);
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID, bakuFERandom);

            Assert.AreEqual(EXPERIENCE_TESTER_CURRENT_EXP, currentCharacter.CurrentEXP);

            int experienceToAdd = 100;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, experienceToAdd, 0, 0, WeaponType.SWORD);

            Assert.AreEqual(EXPERIENCE_TESTER_CURRENT_EXP + experienceToAdd - ExperienceUtil.GetXPRequiredForNextLevelUp(currentCharacter.Level - 1), currentCharacter.CurrentEXP);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_LEVEL + 1, currentCharacter.Level);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, experienceToAdd)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_LELVELUP, EXPERIENCE_TESTER_NAME, EXPERIENCE_TESTER_INITIAL_LEVEL + 1)));

            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_HP + 1, currentCharacter.Attributes[(int)Attribute.HP]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_STR + 1, currentCharacter.Attributes[(int)Attribute.STR]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_MAG + 1, currentCharacter.Attributes[(int)Attribute.MAG]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_DEX + 1, currentCharacter.Attributes[(int)Attribute.DEX]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_SPD + 1, currentCharacter.Attributes[(int)Attribute.SPD]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_LCK + 1, currentCharacter.Attributes[(int)Attribute.LCK]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_DEF + 1, currentCharacter.Attributes[(int)Attribute.DEF]);
            Assert.AreEqual(EXPERIENCE_TESTER_INITIAL_RES + 1, currentCharacter.Attributes[(int)Attribute.RES]);

            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.HP.ToString(), currentCharacter.Attributes[(int)Attribute.HP] - 1, currentCharacter.Attributes[(int)Attribute.HP])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.STR.ToString(), currentCharacter.Attributes[(int)Attribute.STR] - 1, currentCharacter.Attributes[(int)Attribute.STR])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.MAG.ToString(), currentCharacter.Attributes[(int)Attribute.MAG] - 1, currentCharacter.Attributes[(int)Attribute.MAG])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.DEX.ToString(), currentCharacter.Attributes[(int)Attribute.DEX] - 1, currentCharacter.Attributes[(int)Attribute.DEX])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.SPD.ToString(), currentCharacter.Attributes[(int)Attribute.SPD] - 1, currentCharacter.Attributes[(int)Attribute.SPD])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.LCK.ToString(), currentCharacter.Attributes[(int)Attribute.LCK] - 1, currentCharacter.Attributes[(int)Attribute.LCK])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.DEF.ToString(), currentCharacter.Attributes[(int)Attribute.DEF] - 1, currentCharacter.Attributes[(int)Attribute.DEF])));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, Attribute.RES.ToString(), currentCharacter.Attributes[(int)Attribute.RES] - 1, currentCharacter.Attributes[(int)Attribute.RES])));
        }

        [Test]
        public void AddExperienceToClass_AddRegularExperience()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);

            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentAcquiredClass().Class.CharacterClassKey);
            Assert.AreEqual(EXPERIENCE_TESTER_SOLDIER_EXPERIENCE, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.AreEqual(false, currentCharacter.GetCurrentAcquiredClass().IsMastered);

            int experienceToAdd = 10;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, 0, experienceToAdd, 0, WeaponType.SWORD);

            Assert.AreEqual(EXPERIENCE_TESTER_SOLDIER_EXPERIENCE + experienceToAdd, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.IsFalse(currentCharacter.GetCurrentAcquiredClass().IsMastered);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_CLASS_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, experienceToAdd)));
        }

        [Test]
        public void AddExperienceToClass_AddRegularExperience_MultipleAcquiredClasses()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);

            currentCharacter.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BRAWLER)));
            currentCharacter.CurrentClassIndex = 1;

            Assert.AreEqual(2, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.AreEqual(CharacterClassKey.BRAWLER, currentCharacter.GetCurrentAcquiredClass().Class.CharacterClassKey);
            Assert.AreEqual(0, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.AreEqual(false, currentCharacter.GetCurrentAcquiredClass().IsMastered);

            int experienceToAdd = 10;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, 0, experienceToAdd, 0, WeaponType.SWORD);

            //Check the new brawler class that has received the experience
            Assert.AreEqual(CharacterClassKey.BRAWLER, currentCharacter.GetCurrentAcquiredClass().Class.CharacterClassKey);
            Assert.AreEqual(experienceToAdd, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.IsFalse(currentCharacter.GetCurrentAcquiredClass().IsMastered);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_CLASS_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, experienceToAdd)));
            //Check if the previous soldier class remains untouched
            currentCharacter.CurrentClassIndex = 0;
            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentAcquiredClass().Class.CharacterClassKey);
            Assert.AreEqual(EXPERIENCE_TESTER_SOLDIER_EXPERIENCE, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.AreEqual(false, currentCharacter.GetCurrentAcquiredClass().IsMastered);
        }

        [Test]
        public void AddExperienceToClass_ClassMastered()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);

            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentAcquiredClass().Class.CharacterClassKey);
            Assert.AreEqual(EXPERIENCE_TESTER_SOLDIER_EXPERIENCE, currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.AreEqual(false, currentCharacter.GetCurrentAcquiredClass().IsMastered);

            int experienceToAdd = 20;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, 0, experienceToAdd, 0, WeaponType.SWORD);

            Assert.IsFalse(EXPERIENCE_TESTER_SOLDIER_EXPERIENCE + experienceToAdd == currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.IsTrue(ExperienceUtil.GetExperienceNeedToMasterClass(currentCharacter.GetCurrentClass()) == currentCharacter.GetCurrentAcquiredClass().CurrentClassExperience);
            Assert.IsTrue(currentCharacter.GetCurrentAcquiredClass().IsMastered);
            Assert.IsTrue(currentCharacter.LearnedSkills.Contains(SkillKey.DEF_PLUS));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_CLASS_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, experienceToAdd)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_CLASS_MASTERED, EXPERIENCE_TESTER_NAME, currentCharacter.GetCurrentClass().Name)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.CHARACTER_CLASS_MASTERED_SKILL_LEARNED, EXPERIENCE_TESTER_NAME, SkillCollection.SkillDictionary[SkillKey.DEF_PLUS].Name)));
        }

        [Test]
        public void AddExperienceToWeaponRank_AddRegularExperience()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);
            WeaponType weaponType = WeaponType.AXE;
            WeaponHandling weaponHandling = currentCharacter.WeaponRanks[(int)weaponType];
            int startingAxeExperience = 0;
            WeaponRank initialRank = WeaponRank.C;

            Assert.AreEqual(startingAxeExperience, weaponHandling.CurrentWeaponExperience);
            Assert.AreEqual(WeaponType.AXE, weaponHandling.WeaponType);
            Assert.AreEqual(initialRank, weaponHandling.WeaponRank);

            int experienceToAdd = 20;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, 0, 0, experienceToAdd, weaponType);

            Assert.AreEqual(initialRank, weaponHandling.WeaponRank);
            Assert.AreEqual(startingAxeExperience + experienceToAdd, weaponHandling.CurrentWeaponExperience);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_ADD, currentCharacter.Name, experienceToAdd)));
        }

        [Test]
        public void AddExperienceToWeaponRank_AddExperienceWeaponRankUP()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);
            WeaponType weaponType = WeaponType.AXE;
            WeaponHandling weaponHandling = currentCharacter.WeaponRanks[(int)weaponType];
            int startingAxeExperience = 0;
            WeaponRank initialRank = WeaponRank.C;

            Assert.AreEqual(startingAxeExperience, weaponHandling.CurrentWeaponExperience);
            Assert.AreEqual(WeaponType.AXE, weaponHandling.WeaponType);
            Assert.AreEqual(initialRank, weaponHandling.WeaponRank);

            int experienceToAdd = 85;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, 0, 0, experienceToAdd, weaponType);

            Assert.AreEqual(ExperienceUtil.GetFollowUpWeaponRank(initialRank), weaponHandling.WeaponRank);
            Assert.AreEqual(startingAxeExperience + experienceToAdd - ExperienceUtil.GetExperienceToIncreaseWeaponRank(initialRank), weaponHandling.CurrentWeaponExperience);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_ADD, currentCharacter.Name, experienceToAdd)));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_WEAPONRANK_UP, currentCharacter.Name, WeaponUtil.GetReadableNameForWeaponType(weaponType), initialRank, ExperienceUtil.GetFollowUpWeaponRank(initialRank))));
        }

        [Test]
        public void AddAllExperienceAtOnce_CharacterMaxedOut()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(EXPERIENCE_TESTER_ID);
            currentCharacter.Level = ExperienceUtil.MAX_LEVEL;
            currentCharacter.AcquieredCharacterClasses[0] = new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 60, true);
            WeaponType weaponType = WeaponType.AXE;
            WeaponHandling weaponHandling = new WeaponHandling(weaponType, WeaponRank.S, 200);
            currentCharacter.WeaponRanks[(int)weaponType] = weaponHandling;

            Assert.AreEqual(ExperienceUtil.MAX_LEVEL, currentCharacter.Level);
            Assert.IsTrue(currentCharacter.GetCurrentAcquiredClass().IsMastered);
            Assert.AreEqual(WeaponRank.S, currentCharacter.WeaponRanks[(int)weaponType].WeaponRank);

            int characterExperienceToAdd = 85;
            int classExperienceToAdd = 85;
            int weaponExperienceToAdd = 85;
            StateTestInputOutput io = new StateTestInputOutput();

            realm.AddExperienceToCharacter(io, currentCharacter.ID, characterExperienceToAdd, classExperienceToAdd, weaponExperienceToAdd, weaponType);

            Assert.IsFalse(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, characterExperienceToAdd)));
            Assert.IsFalse(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_CLASS_EXPERIENCE_ADD, EXPERIENCE_TESTER_NAME, classExperienceToAdd)));
            Assert.IsFalse(io.DoesIOObjectContainString(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_ADD, currentCharacter.Name, weaponExperienceToAdd)));
        }
    }
}