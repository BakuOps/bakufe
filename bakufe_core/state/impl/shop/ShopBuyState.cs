﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.shop
{
    /// <summary>
    /// This state creates and displays the shop's inventory and let's the character pick what they want to buy
    /// </summary>
    public class ShopBuyState : NumericInputState
    {
        readonly bool isInventoryFull = false;
        readonly List<WeaponKey> weaponKeys = new List<WeaponKey>();

        public ShopBuyState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_SHOP_BUY_NAME)
        {
            isInventoryFull = InventoryUtil.GetNextFreeInventorySlot(character) == InventoryUtil.NO_SLOT_AVAILABLE;
            if (isInventoryFull)
            {
                Description = IOPatterns.STATE_SHOP_BUY_DESCRIPTION_INV_FULL;
            }
            else
            {
                //Build shop
                string shopInventory = IOPatterns.STATE_SHOP_BUY_SHOPINVENTORY_HEADER;
                weaponKeys = InventoryUtil.GetShopInventoryForCharacter(character);
                MinInputValue = 0;
                MaxInputValue = weaponKeys.Count;
                shopInventory += BuildShopInventoryString(weaponKeys);
                shopInventory += IOPatterns.STATE_SHOP_BUY_SHOPINVENTORY_HINT;
                Description = shopInventory;
            }
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (isInventoryFull)
            {
                ChangeCharacterToState(io, new ShopState(realm, character, this));
            }
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (InputValue == 0)
            {
                io.PushOutput(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DECLINE);
                ShopState shopState = new ShopState(realm, character, this);
                ChangeCharacterToState(io, shopState);
            }
            else
            {
                WeaponKey weaponKey = weaponKeys[InputValue - 1];
                Weapon weapon = WeaponCollection.GetWeaponByKey(weaponKey);

                ShopBuyConfirmState shopBuyConfirmState = new ShopBuyConfirmState(realm, character, weapon, this);
                ChangeCharacterToState(io, shopBuyConfirmState);
            }
        }

        private string BuildShopInventoryString(List<WeaponKey> weaponKeys)
        {
            string shopInventory = "";
            Weapon weapon;
            string numberString;
            for (int i = 0; i < weaponKeys.Count; i++)
            {
                weapon = WeaponCollection.GetWeaponByKey(weaponKeys[i]);
                if (i < 10)
                {
                    numberString = " " + (i + 1);
                }
                else
                {
                    numberString = i + 1 + "";
                }

                shopInventory += string.Format(IOPatterns.STATE_SHOP_BUY_SHOPINVENTORY_LINE, numberString, weapon.Name, weapon.Price);
            }

            return shopInventory;
        }
    }
}
