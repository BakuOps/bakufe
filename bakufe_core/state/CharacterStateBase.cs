﻿using bakufe_core.entities;
using bakufe_core.io;
using bakufe_core.io.log;
using bakufe_core.state.impl;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state
{
    /// <summary>
    /// Base implementation for a game state that handle some of the sahred logic
    /// </summary>
    public abstract class CharacterStateBase
    {
        /// <summary>
        /// Default text in case the State get's exited
        /// TODO: Extract somewhere
        /// </summary>
        protected static readonly string STATE_EXITED = "Action canceled!";

        /// <summary>
        /// The name of the current State
        /// To identify what is currently getting on
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// The state's description that will be displayed to the player via IO in most cases
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Reference to the character that this state is all about
        /// </summary>
        protected readonly Character character;

        /// <summary>
        /// This list contains the patterns for the input that is allowed to influence the current state
        /// </summary>
        public readonly List<string> AllowedInput;

        /// <summary>
        /// Reference to the realm to influence things depending on the character's input
        /// </summary>
        protected BakuFERealm realm;

        protected readonly ILogger logger = BakuFELogProvider.Logger;

        /// <summary>
        /// Flag to check if the state is canceled or not
        /// In case it is, the character returns back into the idle state
        /// </summary>
        protected bool isCanceled = false;

        /// <summary>
        /// Holds the user's input command
        /// This is cleared after each input and eases up command handling in the implementing states
        /// </summary>
        protected StateInputCommand inputCommand = null;

        public CharacterStateBase(BakuFERealm realm, Character character, string name)
        {
            this.realm = realm;
            this.character = character;
            Name = name;

            AllowedInput = new List<string>();
        }

        public CharacterStateBase(BakuFERealm realm, Character character, string name, string description)
        {
            this.realm = realm;
            this.character = character;
            Name = name;
            Description = description;

            AllowedInput = new List<string>();
        }

        /// <summary>
        /// Handles the player's input and extracts the input command used.
        /// In case the input is a valid command HandleInputValidCommand will be called.
        /// </summary>
        /// <param name="io">The IO object that contains the player's input.</param>
        public virtual void HandleInput(IInputOutput io)
        {
            //Clear the input command to prevent the last command from messing up the current cycle
            inputCommand = null;

            string input = io.GetInput().ToLower();

            //Check if the state is canceled
            if(input == StateInputCommand.CANCEL.ToString())
            {
                isCanceled = true;
                io.PushOutput(STATE_EXITED);
                ClearCurrentContext(io);
            }
            else if (input == StateInputCommand.AVAILABLE_INPUTS.ToString())
            {
                io.PushOutput(String.Format(IOPatterns.STATE_INPUT_AVAILABLE, GetInputOptions()));
            }
            else
            {
                //Extract the command from the user input
                //First check if the input is allowed
                if (!IsInputAllowed(input))
                {
                    //if not, push an error message to the output
                    io.PushOutput(String.Format(IOPatterns.STATE_INPUT_AVAILABLE, GetInputOptions()));
                }
                else
                {
                    //If the input is allowed try to extract the command from it
                    inputCommand = StateInputCommand.GetInputCommandByCommand(input);
                    //And call the abstract method to handle the input in the implementation
                    HandleInputValidCommand(io);
                }
            }
        }

        /// <summary>
        /// Changes things around according to the input command
        /// This is is where most of the magic happens as well besides ChangeToThisCharacterState
        /// </summary>
        /// <param name="io">The IO object that contains the player's input.</param>
        public abstract void HandleInputValidCommand(IInputOutput io);

        /// <summary>
        /// Checks if an input is a legal input for this state
        /// </summary>
        /// <param name="input">the io object</param>
        /// <returns>if an input is a legal input for this state</returns>
        protected virtual bool IsInputAllowed(string input)
        {
            return AllowedInput.Contains(input.ToLower());
        }

        /// <summary>
        /// Changes the given character's state to the current one.
        /// </summary>
        /// <param name="io">The IO object that contains the player's input</param>
        public virtual void ChangeToThisCharacterState(IInputOutput io)
        {
            ChangeCharacterToState(io, this);
        }

        /// <summary>
        /// Changes the character to the given state.
        /// </summary>
        /// <param name="io">The IO object that contains the player's input</param>
        /// <param name="state">The new state the character get's changed into</param>
        public void ChangeCharacterToState(IInputOutput io, CharacterStateBase state)
        {
            logger.Information(IOPatterns.STATE_CHANGE_LOG, character.Name, this.Name);
            realm.SetCharacterState(character.ID, state);

            io.PushEmptyLine();
            io.PushOutput(String.Format(IOPatterns.STATE_INPUT_REPORT, state.Name));
            io.PushOutput(state.Description);
        }

        /// <summary>
        /// Prints out the allowed input options
        /// </summary>
        /// <returns></returns>
        protected string GetInputOptions()
        {
            return String.Join(", ", AllowedInput);
        }

        /// <summary>
        /// Clear's the current character's context and set them in the idle state
        /// </summary>
        protected void ClearCurrentContext(IInputOutput io)
        {
            IdleState idleState = new IdleState(realm, character);
            idleState.ChangeToThisCharacterState(io);
        }

        #region General overrides

        public override string ToString()
        {
            return Name.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is CharacterStateBase @base &&
                   Name == @base.Name &&
                   EqualityComparer<Character>.Default.Equals(character, @base.character) &&
                   EqualityComparer<BakuFERealm>.Default.Equals(realm, @base.realm) &&
                   isCanceled == @base.isCanceled;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Description, character, AllowedInput, realm, logger, isCanceled);
        }

        #endregion
    }
}
