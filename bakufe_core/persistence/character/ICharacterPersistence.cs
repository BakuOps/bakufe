﻿using bakufe_core.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.persistence.character
{
    public interface ICharacterPersistence
    {
        /// <summary>
        /// Persists the characters with the implementet persistence strategy
        /// </summary>
        /// <param name="character">The character that should be persisted</param>
        /// <param name="characterId">The character's unique identifier</param>
        /// <returns>The result of the action</returns>
        public bool PersistCharacter(string characterId, Character character);

        /// <summary>
        /// Loads the character with the implemented strategy
        /// </summary>
        /// <param name="characterId">The character's unique identifier</param>
        /// <returns>The character that was persisted</returns>
        public Character LoadCharacter(string characterId);

        /// <summary>
        /// Loads all existing characters
        /// </summary>
        /// <returns>List of all characters</returns>
        public List<Character> LoadAllCharacters();

        /// <summary>
        /// Checks if a character with the given Id was persisted with this strategy before.
        /// </summary>
        /// <param name="characterId">The character's unique identifier</param>
        /// <returns>Whether the character was persisted before/exists in this strategy</returns>
        public bool DoesCharacterExist(string characterId);
    }
}
