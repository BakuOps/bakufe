﻿using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// Represents a weapon rank requirement for a class promotion
    /// </summary>
    public class ClassPromotionRequirement
    {
        /// <summary>
        /// The weapon type this requirement is referring to
        /// </summary>
        public WeaponType WeaponType { private set; get; }

        /// <summary>
        /// The weapon rank this requirement is referring to
        /// </summary>
        public WeaponRank WeaponRank { private set; get; }

        public ClassPromotionRequirement(WeaponType weaponType, WeaponRank weaponRank)
        {
            WeaponType = weaponType;
            WeaponRank = weaponRank;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return WeaponType.ToString() + ":" + WeaponRank.ToString();
        }
    }
}
