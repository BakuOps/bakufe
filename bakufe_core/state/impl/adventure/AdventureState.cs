﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.adventure
{
    public class AdventureState : CharacterStateBase
    {
        public AdventureState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_ADVENTURE_NAME)
        {
            //Stud for the adventure logic for now...Will be expanded in the future
            Description = String.Format("As {0} walks along a road, they slip over a pebble. As they open their eyes on the ground, they spot a coin on the road!", character.Name);
            Description += String.Format("\n{0} lost 1 HP", character.Name);
            Description += String.Format("\n{0} found 1 gold", character.Name);
            character.CurrentHP--;
            character.Gold++;
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            base.ChangeToThisCharacterState(io);
            ChangeCharacterToState(io, new IdleState(realm, character));
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            //Nothing to do here since this state changes straight back into the church state
        }
    }
}
