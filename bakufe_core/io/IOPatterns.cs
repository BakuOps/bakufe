﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io
{
    /// <summary>
    /// This class contains all the patterns used to push output into IInputOutput-Objects as static strings 
    /// </summary>
    public class IOPatterns
    {
        //---------------------------------------------------
        // Battle
        //---------------------------------------------------
        public static readonly string BATTLE_VERSUS_STRING = "{0} (Lvl {1} {2}, {3}) vs. {4} (Lvl {5} {6}, {7})";
        public static readonly string BATTLE_ROUND_START = "-- Round {0} starts --";
        public static readonly string BATTLE_ROUND_START_STATUS = "{0} (HP: {1}/{2}) vs. {3} (HP: {4}/{5})";
        public static readonly string BATTLE_ATTACK_START = "{0} attacks!";
        public static readonly string BATTLE_ATTACK_CRITICAL_HIT = "Critical hit!";
        public static readonly string BATTLE_ATTACK_MISS = "{0}'s attack misses!";
        public static readonly string BATTLE_ATTACK_DAMAGE = "{0} hits {1} and deals {2} damage!";
        public static readonly string BATTLE_CHARACTER_DEFEATED = "{0} is defeated, {1} wins the fight!";
        public static readonly string BATTLE_TIMEOUT = "The battle times out! With {0}% health remaining, {1} wins the fight against {2}, who had {3}%";
        public static readonly string BATTLE_TIMEOUT_TRUE_TIE = "The battle times out! Since both character's got the same {0}% of their initial health remaining, {1} wins the battle cause they threw the first punch!";
        public static readonly string BATTLE_SKILL_ACTIVATION = "{0}'s '{1}' activates!";
        public static readonly string BATTLE_SKILL_VAMPIRIC_HEALING = "{0} recovers {1}HP through vampiric healing.";

        //---------------------------------------------------
        // Logger
        //---------------------------------------------------
        public static readonly string LOGGER_BATTLE_ATTACK_HIT_CALCULATION = "{0} Hit: {1}, Avoid: {2}, True Hit: {3}, Hit result: {4}";
        public static readonly string LOGGER_BATTLE_ATTACK_CRIT_CALCULATION = "{0} Crit: {1}, Crit avoid: {2}, Crit chance: {3}, Roll: {4}, Crit result: {5}";
        public static readonly string LOGGER_BATTLE_ATTACK_DAMAGE_CALCULATION = "{0} Attack value: {1}, Defense value: {2}, Damage: {3}";

        //---------------------------------------------------
        // Character loading
        //---------------------------------------------------
        public static readonly string CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM = "Unable to pass enums value '{0}' to type '{1}'.";
        public static readonly string CHARACTER_LOADING_INVALID_ACQUIREDCLASS_DATA = "Illegal acquired class data: '{0}'.";
        public static readonly string CHARACTER_LOADING_INVALID_ITEM_DATA = "Illegal item data: '{0}'.";
        public static readonly string CHARACTER_LOADING_INVALID_WEAPONHANDLING_DATA = "Illegal weapon handling data: '{0}'.";
        public static readonly string CHARACTER_LOADING_INVALID_OR_MISSING_VALUE = "Invalid or missing value for field '{0}': '{1}'";
        public static readonly string CHARACTER_LOADING_SLOT_DOESNT_MATCH_INVENTORY_STATE = "Equipped slot '{0}' doesn't match the current inventory state.";

        //---------------------------------------------------
        // Character Experience
        //---------------------------------------------------
        public static readonly string CHARACTER_EXPERIENCE_ADD = "{0} gained {1}exp!";
        public static readonly string CHARACTER_EXPERIENCE_LELVELUP = "Level up!\n{0} reached level {1}!";
        public static readonly string CHARACTER_EXPERIENCE_STATCHANGE = "{0}: {1} --> {2}\n";
        public static readonly string CHARACTER_EXPERIENCE_LOG_STATROLL = "Character: {0}, Stat: {1}, Growthrate: {2} Roll: {3}";

        public static readonly string CHARACTER_CLASS_EXPERIENCE_ADD = "{0} gained {1} class exp!";
        public static readonly string CHARACTER_CLASS_MASTERED = "{0} mastered the '{1}' class!";
        public static readonly string CHARACTER_CLASS_MASTERED_SKILL_LEARNED = "{0} learned the skill '{1}'!";

        public static readonly string CHARACTER_WEAPON_EXPERIENCE_ADD = "{0} gained {1} weapon exp!";
        public static readonly string CHARACTER_WEAPON_EXPERIENCE_WEAPONRANK_UP = "{0}'s weapon rank in handling {1} increased: {2} --> {3}";

        //---------------------------------------------------
        // Character Class Experience
        //---------------------------------------------------
        public static readonly string CHARACTER_CLASS_NOT_YET_MASTERED_PATTERN = "{0}/{1}";
        public static readonly string CHARACTER_CLASS_MASTERED_REPRESENTATION = "Mastered";

        public static readonly string CHARACTER_ACQUIRED_CLASS_PATTERN = "{0} ({1})";

        //---------------------------------------------------
        // Items and Inventory
        //---------------------------------------------------
        public static readonly string INVENTORY_ITEM_PATTERN = "{0})  {1} {2}\n";
        public static readonly string ITEM_ADDED_TO_INVENTORY = "{0} was added to {1}'s inventory!";
        public static readonly string ITEM_REMOVE_SINGLE = "{0} was removed from {1}'s inventory!";
        public static readonly string ITEM_REMOVE_MULTIPLE = "{0} {1} were removed from {2}'s inventory!";

        public static readonly string ITEM_ITEMBASE_FULL_DESCRIPTION = "Description:\n{0}\n\nStack size: {1}\nSelling price:{2} per item";
        public static readonly string ITEM_WEAPON_FULL_DESCRIPTION = "{0}\nDescription:\n{1}";
        public static readonly string ITEM_ACCESSORY_FULL_DESCRIPTION = "{0}\nDescription:\n{1}";

        public static readonly string ITEM_WEAPON_ATTRIBUTE_PATTERN = "{0}: {1}\n";
        public static readonly string ITEM_ACCESSORY_ATTRIBUTE_PATTERN = "{0}: {1}\n";

        //---------------------------------------------------
        // Weapon readable names
        //---------------------------------------------------
        public static readonly string WEAPON_TYPE_SWORD = "swords";
        public static readonly string WEAPON_TYPE_LANCE = "lances";
        public static readonly string WEAPON_TYPE_AXE = "axes";
        public static readonly string WEAPON_TYPE_BOW = "bows";
        public static readonly string WEAPON_TYPE_GAUNTLET = "gauntlets";
        public static readonly string WEAPON_TYPE_ANIMA = "anima tomes";
        public static readonly string WEAPON_TYPE_DARK = "dark tomes";
        public static readonly string WEAPON_TYPE_LIGHT = "light tomes";

        //---------------------------------------------------
        // States
        //---------------------------------------------------
        #region States
        public static readonly string STATE_CHANGE_LOG = "Changed {0}'s state to {1}";
        public static readonly string STATE_INPUT_AVAILABLE = "This state allows the following inputs: {0}";
        public static readonly string STATE_INPUT_ILLEGAL = "Illegal input: '{0}'\nThis state allows the following inputs: {1}";
        public static readonly string STATE_INPUT_REPORT = "Action: {0}";
        public static readonly string STATE_INPUT_TO_LOW = "Illegal Input: The input '{0}' is lower than the accepted minimum '{1}'";
        public static readonly string STATE_INPUT_TO_HIGH = "Illegal Input: The input '{0}' is higher than the accepted maximum '{1}'";

        public static readonly string STATE_CONFIRM_YESNO_SUFFIX = "\n[Reply with y(es) or n(o)]";

        // Idle
        public static readonly string STATE_IDLE_NAME = "Idle";
        public static readonly string STATE_IDLE_DESCRIPTION = "{0} is currently idling.";

        // Save
        public static readonly string STATE_SAVE_NAME = "Save character";
        public static readonly string STATE_SAVE_DESCRIPTION = "Do you want to save {0}'s character data (and more than likely overwrite existing data)?";
        public static readonly string STATE_SAVE_POSITIVE_RESULT = "{0}'s data has been successfully saved!";
        public static readonly string STATE_SAVE_NEGATIVE_RESULT = "There was an error saving {0}'s!";
        public static readonly string STATE_SAVE_DECLINED = "Save-Action aborted!";

        // Church
        public static readonly string STATE_CHURCH_NAME = "Church";
        public static readonly string STATE_CHURCH_DESCRIPTION_START_ENTER = "{0} enters the local church of Seiros.";
        public static readonly string STATE_CHURCH_DESCRIPTION_START_REMAIN = "{0} is currently standing the local church of Seiros.";
        public static readonly string STATE_CHURCH_DESCRIPTION_MAIN = "\nBesides the luxurious mess hall, there is a hospital located in the west wing where your wounds can be treated in return for a small donation.\nWhat shall {0} do? Pray to the goddess, donate to the church or enter the hospital?";
        public static readonly string STATE_CHURCH_LEAVE = "{0} decides to leave the church for now.";

        public static readonly string STATE_PRAY_NAME = "Pray";
        public static readonly string STATE_PRAY_DESCRIPTION_1 = "{0} walks up in front of the altar and prays to the goddess...\n...But nothing really happens";
        public static readonly string STATE_PRAY_DESCRIPTION_2 = "{0} walks up in front of the altar and prays to the goddess...\nWhile doing so, {0} feels like a burden had been lifted of their shoulders.";
        public static readonly string STATE_PRAY_DESCRIPTION_3 = "{0} walks up in front of the altar and prays to the goddess...\nAs {0} prays, the buzzing around her grows more and more silent. As gentle breeze enters the church through the great main entrance while the warm rays of the sun glister through the coloured glass. {0} feels blessed as they turn back to the main hall.";
        public static readonly string STATE_PRAY_DESCRIPTION_4 = "{0} walks up in front of the altar and prays to the goddess...\n{0} twitches as a loud thunder can be heard from a distance. After looking around {0} nervously shuffles away from the altar.";

        public static readonly string STATE_CHURCHDONATION_NAME = "Donation";
        public static readonly string STATE_CHURCHDONATION_DESCRIPTION = "{0} approaches the priestess who handles the donations behind a small counter. She raises her head and starts to speak in a soft tone.\n'Hello, are you here to donate? No matter the amount, every donation is appreciated by the goddess.'\n\nHow much gold do you want to donate?\n(Min:0 Max:{1})";
        public static readonly string STATE_CHURCHDONATION_AMOUNT = "{0} donated {1} gold to the church of Seiros, which got accepted happily by the priestess.";
        public static readonly string STATE_CHURCHDONATION_NO_DONATION = "{0} decides to donate nothing and excuses themselves for now.";

        // Hospital
        public static readonly string STATE_HOSPITAL_NAME = "Hospital";
        public static readonly string STATE_HOSPITAL_DESCRIPTION = "{0} enters the hospital in the west wing of the church of Seiros. A nurse approached {0} as they were waiting at the reception.\n";
        public static readonly string STATE_HOSPITAL_DESCRIPTION_FULL_HEALTH = "'You don't seem to be in need of our services, but if you ever are, you know where to find us', she tells {0} before getting back to her work.\n{0} decides to leave the hospital for now.";
        public static readonly string STATE_HOSPITAL_DESCRIPTION_NEED_HEALING = "'Oh dear, it seems like you came to the right place.', she tells {0}, 'Let us start your treatment right away, it will cost you {1} gold, is that alright?'";
        public static readonly string STATE_HOSPITAL_DESCRIPTION_NEED_HEALING_NOT_ENOUGH_FUNDS = "\n'Not enough funds?' the nurse asks 'Well, we can't treat you for free, just pay what you can for now and give back to the church some time later alright?'\n[The new price will be {0} gold]";
        public static readonly string STATE_HOSPITAL_TREATMENT = "\n{0}'s wounds got treated with healing magic, letting them leave in top form.";
        public static readonly string STATE_HOSPITAL_TREATMENT_DECLINED = "\n{0} declines the offer and decides to leave the hospital for now.";

        // Shop
        public static readonly string STATE_SHOP_NAME = "Shop";
        public static readonly string STATE_SHOP_DESCRIPTION = "{0} enters the local weapon shop. A scruffy man, looking like a smith looks at them from behind the counter. 'What can I do for ya?' they ask, 'Wanna buy or sell some weapons?'";
        public static readonly string STATE_SHOP_DESCRIPTION_RETURN = "So, anything else I can do for you? Wanna buy or sell some weapons?";
        public static readonly string STATE_SHOP_LEAVE = "{0} decides to leave the shop for now.";
        public static readonly string STATE_SHOP_BUY = "'You want to buy something eh? Well, here is what I can offer to you...'";
        public static readonly string STATE_SHOP_SELL = "'You want to sell me your goods? Sure, so show me what you brought in...'";

        public static readonly string STATE_SHOP_BUY_NAME = "Shop - Buy";
        public static readonly string STATE_SHOP_BUY_DESCRIPTION_INV_FULL = "'You want to buy something eh? Well, I don't think you already got your hands full. Do you want to sell me something or come back later once you actually got space in your pockets?'";
        public static readonly string STATE_SHOP_BUY_SHOPINVENTORY_HEADER = "- Shop inventory -\n 0) Don't buy anything\n";
        public static readonly string STATE_SHOP_BUY_SHOPINVENTORY_LINE = "{0}) {1} {2}gold\n"; 
        public static readonly string STATE_SHOP_BUY_SHOPINVENTORY_HINT = "[input the number to buy]";

        public static readonly string STATE_SHOP_BUY_CONFIRMATION_NAME = "Shop - Buy - Confirm";
        public static readonly string STATE_SHOP_BUY_CONFIRMATION_DESCRIPTION = "'So you want to buy {0} for {1} gold?'";
        public static readonly string STATE_SHOP_BUY_CONFIRMATION_DESCRIPTION_NOT_ENOUGH_MONEY = "'I am sorry, but it seems like you don't have the funds needed to purchase that.'";
        public static readonly string STATE_SHOP_BUY_CONFIRMATION_DECLINE = "'Changed your mind? No problem, are you interessted in something else?'";
        public static readonly string STATE_SHOP_BUY_CONFIRMATION_PURCHASE = "{0} purchased {1} for {2} gold.\n'Thanks for your patronage, do you want to buy anything else?'";

        public static readonly string STATE_SHOP_SELL_NAME = "Shop - Sell";
        public static readonly string STATE_SHOP_SELL_NOTHING_SELLABLE = "'Seems like there is nothing you can show me, don't waste my time!'";
        public static readonly string STATE_SHOP_SELL_SHOPINVENTORY_HEADER = "- Sellable weapons -\n 0) Don't sell anything\n";

        public static readonly string STATE_SHOP_SELL_CONFIRMATION_NAME = "Shop - Sell - Confirm";
        public static readonly string STATE_SHOP_SELL_CONFIRMATION_DESCRIPTION = "'So you want to sell your {0}? I can offer you {1} gold for it in this condition, is that alright?'";
        public static readonly string STATE_SHOP_SELL_CONFIRMATION_PURCHASE = "{0} sold {1} for {2} gold.\n'Thanks for your patronage.'";
        public static readonly string STATE_SHOP_SELL_CONFIRMATION_KEEP_ON = "'Do you want to sell anything else?'";

        // Class change
        public static readonly string STATE_CLASS_CHANGE_NAME = "Change class";
        public static readonly string STATE_CLASS_CHANGE_DESCRIPTION = "What class shall {0} change into?\n{1}\n(Input the number of the class you want the character to change into)";
        public static readonly string STATE_CLASS_CHANGE_OPTION_NO_CHANGE = "{0}) Remain in the current class ({1})";
        public static readonly string STATE_CLASS_CHANGE_OPTION_NO_CHANGE_DONE = "{0} remains in their current class: {1}";
        public static readonly string STATE_CLASS_CHANGE_OPTION_CLASS_CHANGE = "{0} changed their class to {1}";
        public static readonly string STATE_CLASS_CHANGE_NO_CHANGE_POSSIBLE = "\n{0} remains in their current class as there are no classes they could currently change into.\n";
        public static readonly string STATE_CLASS_CHANGE_OPTION_TEMPLATE = "{0}) {1}\n";

        // Promotion
        public static readonly string STATE_PROMOTION_NAME = "Promotion - Select";
        public static readonly string STATE_PROMOTION_DESCRIPTION = "What class shall {0} attempt to promote into?\n{1}\n(Input the number of the class you want the character to attempt to promote into)";
        public static readonly string STATE_PROMOTION_OPTION_TEMPLATE = "{0}) {1} ({2}%, {3})\n";
        public static readonly string STATE_PROMOTION_OPTION_NO_CHANGE = "{0}) Back\n";
        public static readonly string STATE_PROMOTION_NO_PROMOTION_AVAILABLE = "\nThere are currently no classes available {0} could promote into.\n";
        public static readonly string STATE_PROMOTION_SEAL_MISSING = "\n{0} doesn't carry the {1}, that is required to attempt that promotion.\n";

        public static readonly string STATE_PROMOTION_CONFIRMATION_NAME = "Promotion - Confirmation";
        public static readonly string STATE_PROMOTION_CONFIRMATION_DESCRIPTION = "Do you want {0} to attempt promoting into the class '{1}'?\nChance of success: {2}%\n(The required seal will be comsumed regardless of the outcome)";
        public static readonly string STATE_PROMOTION_CONFIRMATION_NEGATIVE_REPLY = "Do you want {0} to attempt to promote into another class?";
        public static readonly string STATE_PROMOTION_CONFIRMATION_POSITIVE_ATTEMPT = "Success! {0} acquired the class '{1}'!";
        public static readonly string STATE_PROMOTION_CONFIRMATION_NEGATIVE_ATTEMPT = "Too bad, better luck next time!";

        public static readonly string STATE_PROMOTION_CONFIRMATION_CHANGE_NAME = "Promotion - Change to new class";
        public static readonly string STATE_PROMOTION_CONFIRMATION_CHANGE_DESCRIPTION = "Shall {0} change to the newly acquired class '{1}' right now?";

        // Iventory
        public static readonly string STATE_INVENTORY_NAME = "{0}'s inventory";
        public static readonly string STATE_INVENTORY_DESCRIPTION = "Gold: {0} gold\n\n{1}\n(Enter the number of a valid slot to get more information about that item/to close the inventory)";
        public static readonly string STATE_INVENTORY_CLOSE_OPTION = "{0}) Close inventory";
        public static readonly string STATE_INVENTORY_EMPTY_SLOT = "Slot {0} is currently empty.";
        public static readonly string STATE_INVENTORY_CLOSED = "Inventory closed.";

        public static readonly string STATE_INVENTORY_DETAIL_NAME = "{0}'s inventory {1} - {2}";
        public static readonly string STATE_INVENTORY_DETAIL_TITLE_EMPTY_SLOT = "Empty";

        // CharacterDetail
        public static readonly string STATE_CHARACTER_DETAIL_NAME = "{0}'s status";
        public static readonly string STATE_CHARACTER_DETAIL_DESCRIPTION = 
            "Name: {0}\nLevel: {1}\nExperience: {2}/{3}\n\n" + 
            "Attributes:\n{4}\n" + 
            "Current Class: {5}\n" + 
            "Class experience: {6}\n\n" + 
            "Other acquired classes:\n{7}\n\n" + 
            "Weapon ranks:\n{8}\n";
        public static readonly string STATE_CHARACTER_DETAIL_ATTRIBUTE_PATTERN = "{0}: {1}\n";
        public static readonly string STATE_CHARACTER_DETAIL_WEAPON_RANK_PATTERN = "{0}: {1}({2}/{3})\n";

        // Adventure
        public static readonly string STATE_ADVENTURE_NAME = "Adventure";

        #endregion

        //Private constructor
        private IOPatterns() { }
    }
}
