﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum CharacterClassLevel
    {
        BEGINNER,
        INTERMEDIATE,
        ADVANCED,
        MASTER
    }
}
