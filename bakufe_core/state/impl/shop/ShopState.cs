﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.shop
{
    /// <summary>
    /// Shop where the character can buy or sell weapons.
    /// </summary>
    public class ShopState : CharacterStateBase
    {
        public ShopState(BakuFERealm realm, Character character, CharacterStateBase previousState)
            : base(realm, character, IOPatterns.STATE_SHOP_NAME)
        {
            if (previousState.GetType() == typeof(ShopBuyState) || previousState.GetType() == typeof(ShopSellState) || previousState.GetType() == typeof(ShopSellConfirmationState))
            {
                Description = IOPatterns.STATE_SHOP_DESCRIPTION_RETURN;
            }
            else
            {
                Description = string.Format(IOPatterns.STATE_SHOP_DESCRIPTION, character.Name);
            }


            AllowedInput.Add(StateInputCommand.BUY.command);
            AllowedInput.Add(StateInputCommand.SELL.command);
            AllowedInput.Add(StateInputCommand.LEAVE.command);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (inputCommand.key == StateInputCommand.LEAVE.key)
            {
                io.PushOutput(string.Format(IOPatterns.STATE_SHOP_LEAVE, character.Name));
                IdleState idleState = new IdleState(realm, character);
                idleState.ChangeToThisCharacterState(io);
            }
            if (inputCommand.key == StateInputCommand.BUY.key)
            {
                io.PushOutput(IOPatterns.STATE_SHOP_BUY);
                ShopBuyState shopBuyState = new ShopBuyState(realm, character);
                shopBuyState.ChangeToThisCharacterState(io);
            }
            if (inputCommand.key == StateInputCommand.SELL.key)
            {
                io.PushOutput(IOPatterns.STATE_SHOP_SELL);
                ShopSellState shopSellState = new ShopSellState(realm, character);
                shopSellState.ChangeToThisCharacterState(io);
            }
        }
    }
}
