﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum WeaponType
    {
        //Physical weapons
        SWORD = 0,
        LANCE = 1,
        AXE = 2,
        BOW = 3,
        GAUNTLET = 4,
        //Tomes
        ANIMA = 5,
        DARK = 6,
        LIGHT = 7
    }
}
