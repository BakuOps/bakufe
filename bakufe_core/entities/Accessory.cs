﻿using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_core.entities
{
    public class Accessory : ItemBase
    {
        public AccessoryKey AccessoryKey { get; set; }

        public int[] AttributeBoni { get; set; }

        public int[] ComabtBoni { get; set; }

        #region Constructors

        /// <summary>
        /// Base constructor only needed for serialization and testing purposes, don't use this anywhere else!!!
        /// </summary>
        public Accessory()
        {
            AttributeBoni = new int[Enum.GetNames(typeof(Attribute)).Length];
            ComabtBoni = new int[Enum.GetNames(typeof(WeaponAttribute)).Length];
        }

        public Accessory(string name, string description, int price, AccessoryKey accessoryKey, int[] attributeBoni, int[] comabtBoni) 
            : base(accessoryKey.ToString(), name, description, 1, price)
        {
            AccessoryKey = accessoryKey;
            AttributeBoni = attributeBoni;
            ComabtBoni = comabtBoni;
        }

        public Accessory ShallowCopy()
        {
            return (Accessory)MemberwiseClone();
        }

        #endregion

        public override string GetFullItemDescription()
        {
            String statsString = "";

            enums.Attribute[] attributes = (Attribute[])Enum.GetValues(typeof(enums.Attribute));
            for(int i = 0; i < attributes.Length; i++)
            {
                enums.Attribute attribute = attributes[i];
                if(AttributeBoni[(int)attribute] > 0)
                {
                    statsString += string.Format(IOPatterns.ITEM_ACCESSORY_ATTRIBUTE_PATTERN, Enum.GetName(typeof(enums.Attribute), attribute), AttributeBoni[(int)attribute]);
                }
            }

            WeaponAttribute[] wAttributes = (WeaponAttribute[])Enum.GetValues(typeof(WeaponAttribute));
            for (int i = 0; i < wAttributes.Length; i++)
            {
                WeaponAttribute wAttribute = wAttributes[i];
                if (ComabtBoni[(int)wAttribute] > 0)
                {
                    statsString += string.Format(IOPatterns.ITEM_ACCESSORY_ATTRIBUTE_PATTERN, Enum.GetName(typeof(WeaponAttribute), wAttribute), ComabtBoni[(int)wAttribute]);
                }
            }

            return string.Format(IOPatterns.ITEM_ACCESSORY_FULL_DESCRIPTION, statsString, Description);
        }

        public override bool Equals(object obj)
        {
            return obj is Accessory accessory &&
                   AccessoryKey == accessory.AccessoryKey;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AccessoryKey);
        }
    }
}
