﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources
{
    public class GenericItemCollection
    {
        private static readonly int SEAL_MAX_STACKSIZE = 5;

        private static readonly int BEGINNER_SEAL_PRICE = 100;
        private static readonly int INTERMEDIATE_SEAL_PRICE = 200;
        private static readonly int ADVANCED_SEAL_PRICE = 1000;
        private static readonly int MASTER_SEAL_PRICE = 2000;

        private static Dictionary<GenericItemKey, GenericItem> GenericItemDictionary = GetGenericItemDictionary();

        public static GenericItem GetGenericItemByKey(GenericItemKey key)
        {
            if (!GenericItemDictionary.ContainsKey(key))
            {
                return null;
            }
            else
            {
                return GenericItemDictionary[key].ShallowCopy();
            }
        }

        private static Dictionary<GenericItemKey, GenericItem> GetGenericItemDictionary()
        {
            return new Dictionary<GenericItemKey, GenericItem>() {
                {
                    GenericItemKey.BEGINNER_SEAL,
                    new GenericItem(
                        "Beginner seal",
                        "This seal allows the character to attempt a class exam for an beginner class. It will be consumed regardless of the outcome once the attempt is made.",
                        SEAL_MAX_STACKSIZE,
                        BEGINNER_SEAL_PRICE,
                        GenericItemKey.BEGINNER_SEAL
                    )
                },
                {
                    GenericItemKey.INTERMEDIATE_SEAL,
                    new GenericItem(
                        "Intermediate seal",
                        "This seal allows the character to attempt a class exam for an intermediate class. It will be consumed regardless of the outcome once the attempt is made.",
                        SEAL_MAX_STACKSIZE,
                        INTERMEDIATE_SEAL_PRICE,
                        GenericItemKey.INTERMEDIATE_SEAL
                        
                    )
                },
                {
                    GenericItemKey.ADVANCED_SEAL,
                    new GenericItem(
                        "Advanced seal",
                        "This seal allows the character to attempt a class exam for an advanced class. It will be consumed regardless of the outcome once the attempt is made.",
                        SEAL_MAX_STACKSIZE,
                        ADVANCED_SEAL_PRICE,
                        GenericItemKey.ADVANCED_SEAL
                    )
                },
                {
                    GenericItemKey.MASTER_SEAL,
                    new GenericItem(
                        "Master seal",
                        "This seal allows the character to attempt a class exam for an master class. It will be consumed regardless of the outcome once the attempt is made.",
                        SEAL_MAX_STACKSIZE,
                        MASTER_SEAL_PRICE,
                        GenericItemKey.MASTER_SEAL
                    )
                }
            };
        }

        private GenericItemCollection()
        {

        }
    }
}
