# BakuFE

This is another side project I am currently developping actively in my free time to combine my hobby of coding with my love for the Fire Emblem fanchise. This project is set out to be a non-profit, non-commercial fan game that results out of my passion for coding and the desire to contribute something to this fandom.

## Current Version: [Release 0.6.0](https://gitlab.com/BakuOps/bakufe/-/tags/Release_0.6.0)

### An overview of the current changes can be found in the [Changelog](https://gitlab.com/BakuOps/bakufe/-/blob/developer/CHANGELOG)