﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources.keys
{
    public enum WeaponKey
    {
        //Swords
        TRAINING_SWORD,
        TRAINING_SWORD_PLUS,
        RAPIER,
        RAPIER_PLUS,
        IRON_SWORD,
        IRON_SWORD_PLUS,
        STEEL_SWORD,
        STEEL_SWORD_PLUS,

        RUNIC_BLADE,

        //Lances
        TRAINING_LANCE,
        TRAINING_LANCE_PLUS,
        IRON_LANCE,
        IRON_LANCE_PLUS,

        //Axes
        TRAINING_AXE,
        TRAINING_AXE_PLUS,
        IRON_AXE,
        IRON_AXE_PLUS,

        //Bows
        TRAINING_BOW,
        TRAINING_BOW_PLUS,
        IRON_BOW,
        IRON_BOW_PLUS,
        STEEL_BOW,
        STEEL_BOW_PLUS,
        MINI_BOW,
        MINI_BOW_PLUS,

        //Gauntlets
        TRAINING_GAUNTLETS,
        TRAINING_GAUNTLETS_PLUS,
        IRON_GAUNTLETS,
        IRON_GAUNTLETS_PLUS,
        STEEL_GAUNTLETS,
        STEEL_GAUNTLETS_PLUS,

        //Tomes - Anima
        FIRE,
        WIND,
        THUNDER,

        //Tomes - Dark
        MIASMA,

        //Tomes - Lights
        NOSFERATU,

        //Unarmed combat
        UNARMED,
        UNARMED_PLUS
    }
}
