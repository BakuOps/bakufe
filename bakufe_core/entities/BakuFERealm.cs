﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_core.io.log;
using bakufe_core.io.provider;
using bakufe_core.persistence.character;
using bakufe_core.state;
using bakufe_core.state.impl;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    public class BakuFERealm
    {
        private static readonly ILogger Logger = BakuFELogProvider.Logger;

        private readonly IInputOutputProvider inputOutputProvider;
        private readonly ICharacterPersistence characterPersistence;
        private readonly bool autosaveAfterEachCharacterAction = false;

        private readonly Dictionary<string, Character> CharacterMap = new Dictionary<string, Character>();
        private readonly Dictionary<string, CharacterStateBase> StateMap = new Dictionary<string, CharacterStateBase>();

        public readonly BakuFERandom FERandom;

        public BakuFERealm(IInputOutputProvider inputOutputProvider, ICharacterPersistence characterPersistence, BakuFERandom random, bool autosave)
        {
            this.inputOutputProvider = inputOutputProvider;
            this.characterPersistence = characterPersistence;
            this.FERandom = random;
            this.autosaveAfterEachCharacterAction = autosave;
        }

        public IInputOutput GetEmptyInputOutput()
        {
            return inputOutputProvider.GetStandardInputOutput();
        }

        /// <summary>
        /// Returns the character with the given characterID if it exists and can be loaded properly, otherwise null will be returned
        /// </summary>
        /// <param name="characterId">The given character's id</param>
        /// <returns>the character with the given characterID if it exists and can be loaded properly, otherwise null will be returned</returns>
        public Character GetCharacter(string characterId)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                return CharacterMap[characterId];
            }

            return null;
        }

        /// <summary>
        /// Removes the character with the given id from the realm
        /// </summary>
        /// <param name="characterId">The given characterId</param>
        public void RemoveCharacter(string characterId)
        {
            CharacterMap.Remove(characterId);
            StateMap.Remove(characterId);
        }


        /// <summary>
        /// Returns the current State the given character is currently in or null if there is no entry for the given id
        /// </summary>
        /// <param name="characterId">The given character's id</param>
        /// <returns>the current State the given character is currently in or null if there is no entry for the given id</returns>
        public CharacterStateBase GetCharactersCurrentState(string characterId)
        {
            if (StateMap.ContainsKey(characterId))
            {
                return StateMap[characterId];
            }

            return null;
        }

        /// <summary>
        /// Set's the state for the character with the given id
        /// </summary>
        /// <param name="characterId">The id the of the character who should be set to this state</param>
        /// <param name="state">The state the character should be set to</param>
        public void SetCharacterState(string characterId,CharacterStateBase state)
        {
            StateMap[characterId] = state;
        }


        #region Experience and Levelups

        /// <summary>
        /// Adds the given amounts of experience to the characters and autosaves them configured in the realm
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="characterExperienceToAdd">The given amount that will be awarded to the character</param>
        /// <param name="classExperienceToAdd">The given amount that will be awarded to the character's current class</param>
        /// <param name="weaponExperienceToAdd">The given amount that will be awarded to the character's weapon with the given type class</param>
        /// <param name="weaponType">The weapon type the exp that should be added to</param>
        public void AddExperienceToCharacter(IInputOutput io, string characterId, int characterExperienceToAdd, int classExperienceToAdd, int weaponExperienceToAdd, WeaponType weaponType)
        {
            if(characterExperienceToAdd > 0)
            {
                AddCharacterExperienceToCharacter(io, characterId, characterExperienceToAdd, false);
            }
            if(classExperienceToAdd > 0)
            {
                AddClassExperienceToCharacter(io, characterId, classExperienceToAdd, false);
            }
            if (weaponExperienceToAdd > 0)
            {
                AddWeaponExperienceToCharacter(io, characterId, weaponExperienceToAdd, weaponType, false);
            }
            
            AutosaveCharacterIfActivated(characterId);
        }


        /// <summary>
        /// Adds the given amount of experience to the character with the given characterId and handles the levelup in case one is achieved.
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="amountOfExpToAdd">The given amount that will be awarded to the character</param>
        /// <param name="attemptAutosave">Flag whether or not the realm should check for an autosave or not</param>
        private void AddCharacterExperienceToCharacter(IInputOutput io, string characterId, int amountOfExpToAdd, bool attemptAutosave)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];

                if(character.Level < ExperienceUtil.MAX_LEVEL)
                {
                    character.CurrentEXP += amountOfExpToAdd;
                    io.PushOutput(string.Format(IOPatterns.CHARACTER_EXPERIENCE_ADD, character.Name, amountOfExpToAdd));

                    int expRequiredForNextLevel = ExperienceUtil.GetXPRequiredForNextLevelUp(character.Level);

                    //If the current amount of EXP is equal or greater than the required amount...
                    if (expRequiredForNextLevel != ExperienceUtil.EXP_MAXED_OUT && character.CurrentEXP >= expRequiredForNextLevel)
                    {
                        LevelUpCharacter(io, character);
                    }

                    if (attemptAutosave)
                    {
                        AutosaveCharacterIfActivated(character);
                    }
                }
            }
            else
            {
                //Error handling, do nothing for now
            }
        }

        /// <summary>
        /// Handles the level up process of a character
        /// </summary>
        /// <param name="io">The io-object used for feedback</param>
        /// <param name="character">The character in questions</param>
        private void LevelUpCharacter(IInputOutput io, Character character)
        {
            io.PushOutput(String.Format(IOPatterns.CHARACTER_EXPERIENCE_LELVELUP, character.Name, character.Level + 1));

            int expRequiredForNextLevel = ExperienceUtil.GetXPRequiredForNextLevelUp(character.Level);
            //...handle the level up
            character.CurrentEXP -= expRequiredForNextLevel;
            character.Level++;
            //Handle stat growths
            int[] characterClassRates = character.GetCurrentClass().GrowthRates;

            for(int i = 0; i < characterClassRates.Length; i++)
            {
                int totalChance = characterClassRates[i];
                int roll = FERandom.Next(0, 100);

                Logger.Information(IOPatterns.CHARACTER_EXPERIENCE_LOG_STATROLL, character.ID, ((entities.enums.Attribute)i).ToString(), totalChance, roll);

                if (roll < totalChance)
                {
                    io.PushOutput(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, ((entities.enums.Attribute)i).ToString(), character.Attributes[i], character.Attributes[i] + 1));
                    character.Attributes[i]++;
                }
                else
                {
                    io.PushOutput(String.Format(IOPatterns.CHARACTER_EXPERIENCE_STATCHANGE, ((entities.enums.Attribute)i).ToString(), character.Attributes[i], character.Attributes[i]));
                }
            }
        }

        /// <summary>
        /// Adds the given amount of class expereince to the character's current class and takes action in case it get's mastered that way
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="amountOfExpToAdd">The given amount that will be awarded to the character's current class</param>
        /// <param name="attemptAutosave">Flag whether or not the realm should check for an autosave or not</param>
        private void AddClassExperienceToCharacter(IInputOutput io, string characterId, int amountOfExpToAdd, bool attemptAutosave)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                AcquiredCharacterClass currentAcquiredClass = character.GetCurrentAcquiredClass();

                if (!currentAcquiredClass.IsMastered)
                {
                    //Add the experience to the current Class
                    currentAcquiredClass.AddExperienceToAcquiredClass(amountOfExpToAdd);
                    io.PushOutput(string.Format(IOPatterns.CHARACTER_CLASS_EXPERIENCE_ADD, character.Name, amountOfExpToAdd));

                    //If the added amount causes the characterClass to be mastered...
                    if (currentAcquiredClass.IsMastered)
                    {
                        //...Add the mastery skills to the character's learned skills and add the fitting output
                        io.PushOutput(string.Format(IOPatterns.CHARACTER_CLASS_MASTERED, character.Name, currentAcquiredClass.Class.Name));
                        //TODO: Replace with logic that automatically equips the skills 
                        character.LearnedSkills.AddRange(currentAcquiredClass.Class.MasterySkills);
                        for(int i=0; i < currentAcquiredClass.Class.MasterySkills.Count; i++)
                        {
                            Skill currentSkill = SkillCollection.SkillDictionary[currentAcquiredClass.Class.MasterySkills[i]];
                            io.PushOutput(string.Format(IOPatterns.CHARACTER_CLASS_MASTERED_SKILL_LEARNED, character.Name, currentSkill.Name));
                        }
                    }

                    if (attemptAutosave)
                    {
                        AutosaveCharacterIfActivated(character);
                    }
                }
                
            }
            else
            {
                //Error handling, do nothing for now
            }
        }

        /// <summary>
        /// Adds the given amount of weapon expereince to the character's current class and takes action in case it get's mastered that way
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="amountOfExpToAdd">The given amount that will be awarded to the character's weapon with the given type class</param>
        /// <param name="weaponType">The weapon type the exp that should be added to</param>
        /// <param name="attemptAutosave">Flag whether or not the realm should check for an autosave or not</param>
        private void AddWeaponExperienceToCharacter(IInputOutput io, string characterId, int amountOfExpToAdd, WeaponType weaponType, bool attemptAutosave)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                WeaponHandling weaponHandling = character.WeaponRanks[(int)weaponType];
                WeaponRank initialRank = weaponHandling.WeaponRank;

                if(weaponHandling.WeaponRank != WeaponRank.S)
                {
                    weaponHandling.AddExperience(amountOfExpToAdd);
                    io.PushOutput(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_ADD, character.Name, amountOfExpToAdd));

                    //If the weapon rank increased, push the notification into the output
                    if (weaponHandling.WeaponRank != initialRank)
                    {
                        io.PushOutput(string.Format(IOPatterns.CHARACTER_WEAPON_EXPERIENCE_WEAPONRANK_UP, character.Name, WeaponUtil.GetReadableNameForWeaponType(weaponType), initialRank.ToString(), weaponHandling.WeaponRank.ToString()));
                    }

                    if (attemptAutosave)
                    {
                        AutosaveCharacterIfActivated(character);
                    }
                }
            }
            else
            {
                //Error handling, do nothing for now
            }
        }

        #endregion

        #region Classes & Promotion

        /// <summary>
        /// Changes the character's class to the class at the given class index
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="newClassIndex">The index of the acquiredClass that shall be equipped</param>
        public void ChangeCharactersCurrentClass(IInputOutput io, string characterId, int newClassIndex)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                character.CurrentClassIndex = newClassIndex;
                io.PushOutput(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_CLASS_CHANGE, character.Name, character.GetCurrentClass().Name));
                AutosaveCharacterIfActivated(characterId);
            }
            else
            {
                //Error handling, do nothing for now
            }
        }

        /// <summary>
        /// Adds a new AcquiredClass to the characters list of AcquiredClasses
        /// </summary>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="newClassKey">The key of the new class that shall be added</param>
        public void AddAcquiredClassToCharacter(string characterId, CharacterClassKey newClassKey)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(newClassKey)));
                AutosaveCharacterIfActivated(characterId);
            }
            else
            {
                //Error handling, do nothing for now
            }
        }

        #endregion

        #region Items

        /// <summary>
        /// Attempts to add the given item to the character's inventory
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="itemToAdd">The item that shall be added to the character's inventory</param>
        /// <returns>Whether or not the opperation was successful</returns>
        public bool AddItemToCharacterIventory(IInputOutput io, string characterId, ItemBase itemToAdd)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];

                bool result = InventoryUtil.AddItemToInventory(character, itemToAdd);

                if (result)
                {
                    io.PushOutput(String.Format(IOPatterns.ITEM_ADDED_TO_INVENTORY, itemToAdd.Name, character.Name));
                    InventoryUtil.SortInventory(character);
                    AutosaveCharacterIfActivated(characterId);
                }
                else
                {
                    //Error handling, do nothing for now
                }

                return result;
            }
            else
            {
                //Error handling, do nothing for now
                return false;
            }
        }

        /// <summary>
        /// Checks if the given character's inventory contains any item with the id
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="itemId">The id of the item that shall be checked for</param>
        /// <returns>if the given character's inventory contains any item with the id</returns>
        public bool DoesCharacterCarryItem(string characterId, string itemId)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                return InventoryUtil.IsItemContainedInInventory(character, itemId);
            }
            else
            {
                //Error handling, do nothing for now
                return false;
            }
        }

        /// <summary>
        /// Checks if the given character's inventory contains the set amount of the given item
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="itemId">The id of the item that shall be checked for</param>
        /// <param name="amount">The amount of items that shall be checked for</param>
        /// <returns>if the given character's inventory contains the set amount of the given item</returns>
        public bool DoesCharacterPossesAmountOfItems(string characterId, string itemId, int amount)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                return InventoryUtil.DoesCharacterInventoryContainTheRequiredAmount(character, itemId, amount);
            }
            else
            {
                //Error handling, do nothing for now
                return false;
            }
        }

        /// <summary>
        /// Remove the given amount of items from the character's inventory
        /// </summary>
        /// <param name="io">The IO object to push feedback into for the player to see</param>
        /// <param name="characterId">The given character's ID</param>
        /// <param name="item">The item that shall be removed</param>
        /// <param name="amount">The amount of items that shall be removed</param>
        /// <returns>If the operation was successful</returns>
        public bool RemoveItemFromCharacterInventory(IInputOutput io, string characterId, ItemBase item, int amount)
        {
            if (AssureCharacterIsLoaded(characterId))
            {
                Character character = CharacterMap[characterId];
                bool result = InventoryUtil.RemoveItemFromCharacterInventory(character, item.ItemID, amount);

                if (result)
                {

                    if(amount == 1)
                    {
                        io.PushOutput(String.Format(IOPatterns.ITEM_REMOVE_SINGLE, item.Name, character.Name));
                    }
                    else
                    {
                        io.PushOutput(String.Format(IOPatterns.ITEM_REMOVE_MULTIPLE, amount, item.Name, character.Name));
                    }
                }

                return result;
            }
            else
            {
                //Error handling, do nothing for now
                return false;
            }
        }

        #endregion

        #region Persistence

        /// <summary>
        /// Assures that the character is loaded into the realm, in case it actually exists.
        /// In case true get's returned, the character exists and is already or is now loaded inside the realm.
        /// This method returns false if either no character with that ID exists or if there is a problem loading that character.
        /// </summary>
        /// <param name="characterId">The given character's id</param>
        /// <returns>True in case the character can be loaded properly and false if it doesn't exist or there is a problem loading this character.</returns>
        private bool AssureCharacterIsLoaded(string characterId)
        {
            if (CharacterMap.ContainsKey(characterId))
            {
                //Character Loaded => true
                return true;
            }
            else
            {
                //If the character is not loaded, check if it even exists
                if (characterPersistence.DoesCharacterExist(characterId))
                {
                    //If it exists, attempt to load the character and return the result
                    return LoadCharacter(characterId);
                }
                else
                {
                    //If it doesn't exist, return false
                    return false;
                }
            }
        }

        public void AutosaveCharacterIfActivated(Character character)
        {
            AutosaveCharacterIfActivated(character.ID);
        }

        public void AutosaveCharacterIfActivated(string characterId)
        {
            if (autosaveAfterEachCharacterAction)
            {
                PersistCharacter(characterId);
            }
        }

        public bool PersistCharacter(string characterId)
        {
            if (CharacterMap.ContainsKey(characterId))
            {
                return characterPersistence.PersistCharacter(characterId, CharacterMap[characterId]);
            }

            return false;
        }

        private bool LoadCharacter(string characterId)
        {
            Character loadedCharacter = characterPersistence.LoadCharacter(characterId);
            if(loadedCharacter == null)
            {
                return false;
            }
            else
            {
                CharacterMap.Add(loadedCharacter.ID, loadedCharacter);
                IdleState idleState = new IdleState(this, loadedCharacter);
                idleState.ChangeToThisCharacterState(GetEmptyInputOutput());
                return true;
            }
        }

        #endregion
    }
}
