﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources.keys
{
    public enum SkillKey
    {
        //Dummy
        DUMMY = 0,

        //Attribute Skills
        HP_PLUS,
        STR_PLUS,
        MAG_PLUS,
        DEX_PLUS,
        SPD_PLUS,
        DEF_PLUS,
        RES_PLUS,

        //MISC
        VANTAGE,
        UNARMED_COMBAT_PLUS
    }
}
