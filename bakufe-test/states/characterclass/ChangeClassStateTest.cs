﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.io;
using bakufe_core.state;
using bakufe_core.state.impl;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.states.characterclass
{
    [TestFixture]
    public class ChangeClassStateTest : TestWithARealm
    {
        private static readonly string CLASS_TESTER_ID = "Class";

        [Test]
        public void ChangeClassState_TestPositiveClassChange()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHANGE_CLASS.command);
            io.inputs.Add(0 + "");

            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(typeof(IdleState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(CharacterClassKey.FIGHTER, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_CLASS_CHANGE, currentCharacter.Name, CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER).Name)));
        }

        [Test]
        public void ChangeClassState_RemainInClass()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHANGE_CLASS.command);
            io.inputs.Add(1 + "");

            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(typeof(IdleState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_NO_CHANGE_DONE, currentCharacter.Name, CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER).Name)));
        }

        [Test]
        public void ChangeClassState_NoChangePossible()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID);

            //Remove the second aquired class
            currentCharacter.AcquieredCharacterClasses.RemoveAt(1);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHANGE_CLASS.command);

            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(typeof(IdleState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_CLASS_CHANGE_NO_CHANGE_POSSIBLE, currentCharacter.Name)));
        }
    }
}
