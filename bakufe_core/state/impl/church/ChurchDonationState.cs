﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.church
{
    public class ChurchDonationState : NumericInputState
    {
        public ChurchDonationState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_CHURCHDONATION_NAME)
        {
            MinInputValue = 0;
            MaxInputValue = character.Gold;
            Description = string.Format(IOPatterns.STATE_CHURCHDONATION_DESCRIPTION, character.Name, MaxInputValue);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            //Subtract the gold from the character value
            character.Gold -= InputValue;
            //Print some output
            if (InputValue == 0)
            {
                io.PushOutput(string.Format(IOPatterns.STATE_CHURCHDONATION_NO_DONATION, character.Name));
            }
            else
            {
                io.PushOutput(string.Format(IOPatterns.STATE_CHURCHDONATION_AMOUNT, character.Name, InputValue));
                realm.AutosaveCharacterIfActivated(character);
            }

            //Shift back to the church state
            ChurchState churchState = new ChurchState(realm, character);
            churchState.ChangeToThisCharacterState(io);
        }
    }
}
