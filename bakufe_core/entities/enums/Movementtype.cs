﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum Movementtype
    {
        INFANTRY = 0,
        CAVALRY = 1,
        FLIER = 2,
        ARMORED = 3
    }
}
