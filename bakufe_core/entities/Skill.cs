﻿using bakufe_core.datasources.keys;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// This object represents a skill that can influence the combat in many different ways
    /// </summary>
    public class Skill
    {
        /// <summary>
        /// The skill's display name
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// A longer description that outlines the skills effect
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// The key that is used to reference this skill
        /// </summary>
        public SkillKey SkillKey { get; protected set; }

        public Skill(string name, string description, SkillKey skillKey)
        {
            Name = name;
            Description = description;
            SkillKey = skillKey;
        }
    }
}
