﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources.keys
{
    public enum CharacterClassKey
    {
        //Special Classes - Development only
        BASE,

        //Beginner Classes
        FIGHTER,
        MONK,
        MYRMIDON,
        SOLDIER,

        //Intermediate Classes
        ARCHER,
        ARMORED_KNIGHT,
        BRAWLER,
        BRIGAND,
        CAVALIER,
        DARKMAGE,
        MAGE,
        MERCENARY,
        PEGASUS_KNIGHT,
        PRIEST,
        THIEF,

        //Advanced Classes
        ASSASSIN,
        BISHOP,
        DARK_BISHOP,
        FORTRESS_KNIGHT,
        GRAPPLER,
        HERO,
        PALADIN,
        SNIPER,
        SWORDMASTER,
        WARLOCK,
        WARRIOR,
        WYVERNRIDER,

        //Master Classes
        BOW_KNIGHT,
        DARK_KNIGHT,
        FALCON_KNIGHT,
        GREAT_KNIGHT,
        GREMORY,
        HOLY_KNIGHT,
        MORTAL_SAVANT,
        WAR_MASTER,
        WYVERN_LORD
    }
}
