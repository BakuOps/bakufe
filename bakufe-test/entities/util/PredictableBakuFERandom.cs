﻿using bakufe_core.entities.util;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.entities.util
{
    public class PredictableBakuFERandom : BakuFERandom
    {
        private int nextSequenceValueSlot = 0;

        public readonly List<int> SequenceValues = new List<int>();

        public PredictableBakuFERandom():base()
        {

        }

        public PredictableBakuFERandom(int seed) : base(seed)
        {

        }

        private void progressSequence()
        {
            nextSequenceValueSlot++;
            if(nextSequenceValueSlot == SequenceValues.Count)
            {
                nextSequenceValueSlot = 0;
            }
        }

        public override int Next(int minIncluded, int maxExcluded)
        {
            int initialSequenceSlot = nextSequenceValueSlot;
            for(int i = 0; i< SequenceValues.Count; i++)
            {
                int currentValue = SequenceValues[nextSequenceValueSlot];
                if(minIncluded <= currentValue && currentValue < maxExcluded)
                {
                    nextSequenceValueSlot = initialSequenceSlot;
                    progressSequence();
                    return currentValue;
                }
            }

            nextSequenceValueSlot = initialSequenceSlot;
            progressSequence();
            return base.Next(minIncluded, maxExcluded);
        }

        public override int RollDoubleRandom(int minIncluded, int maxExcluded)
        {
            int roll1 = Next(minIncluded, maxExcluded);
            int roll2 = Next(minIncluded, maxExcluded);

            return (roll1 + roll2) / 2;
        }
    }
}
