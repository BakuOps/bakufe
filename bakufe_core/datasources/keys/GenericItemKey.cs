﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources.keys
{
    public enum GenericItemKey
    {
        //Promotion seals
        BEGINNER_SEAL,
        INTERMEDIATE_SEAL,
        ADVANCED_SEAL,
        MASTER_SEAL
    }
}
