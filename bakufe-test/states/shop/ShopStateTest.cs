﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.state;
using bakufe_core.state.impl.shop;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.states.shop
{
    [TestFixture]
    public class ShopStateTest : TestWithARealm
    {

        [Test]
        public void Test_BuyFireTome()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID);

            int startingGold = 1000;
            int fireTomeBuyCommand = 4;

            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);

            currentCharacter.Gold = startingGold;

            //Prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.SHOP.command);
            io.inputs.Add(StateInputCommand.BUY.command);
            io.inputs.Add(fireTomeBuyCommand.ToString());
            io.inputs.Add(StateInputCommand.Y.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.IsTrue(InventoryUtil.GetInventoryString(currentCharacter).Contains(fireTome.Name));
            //Amount of items in inventory = 3 => Next free inventory slot => 3
            Assert.IsTrue(InventoryUtil.GetNextFreeInventorySlot(currentCharacter) == 3);
            Assert.AreEqual(startingGold - fireTome.Price, currentCharacter.Gold);
            Assert.AreEqual(typeof(ShopBuyState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
        }

        [Test]
        public void Test_SellFireTome()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID);

            int startingGold = 1000;
            int fireTomeSellCommand = 1;

            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);

            currentCharacter.Gold = startingGold;
            currentCharacter.Inventory[InventoryUtil.GetNextFreeInventorySlot(currentCharacter)] = fireTome;

            //Prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.SHOP.command);
            io.inputs.Add(StateInputCommand.SELL.command);
            io.inputs.Add(fireTomeSellCommand.ToString());
            io.inputs.Add(StateInputCommand.Y.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.IsFalse(InventoryUtil.GetInventoryString(currentCharacter).Contains(fireTome.Name));
            //Amount of items in inventory = 2 => Next free inventory slot => 2
            Assert.IsTrue(InventoryUtil.GetNextFreeInventorySlot(currentCharacter) == 2);
            Assert.AreEqual(startingGold + fireTome.GetSellValue(), currentCharacter.Gold);
            Assert.AreEqual(typeof(ShopState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
        }

        [Test]
        public void Test_StateTransitions()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID);

            int startingGold = 1000;
            int fireTomeSellCommand = 1;
            int trainingGauntletsBuyCommand = 3;

            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            Weapon trainingGauntlets = WeaponCollection.GetWeaponByKey(WeaponKey.TRAINING_GAUNTLETS);

            currentCharacter.Gold = startingGold;
            currentCharacter.Inventory[InventoryUtil.GetNextFreeInventorySlot(currentCharacter)] = fireTome;

            //Prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.SHOP.command);
            io.inputs.Add(StateInputCommand.SELL.command);
            io.inputs.Add(fireTomeSellCommand.ToString());
            io.inputs.Add(StateInputCommand.Y.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            int goldAfterFirstTransaction = startingGold + fireTome.GetSellValue();

            Assert.IsFalse(InventoryUtil.GetInventoryString(currentCharacter).Contains(fireTome.Name));
            //Amount of items in inventory = 2 => Next free inventory slot => 2
            Assert.IsTrue(InventoryUtil.GetNextFreeInventorySlot(currentCharacter) == 2);
            Assert.AreEqual(goldAfterFirstTransaction, currentCharacter.Gold);
            Assert.AreEqual(typeof(ShopState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());

            io.Reset();
            io.inputs.Add(StateInputCommand.SELL.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);
            //Nothing to sell => you get booted to the shop state
            Assert.AreEqual(typeof(ShopState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());

            io.Reset();
            io.inputs.Add(StateInputCommand.BUY.command);
            io.inputs.Add(trainingGauntletsBuyCommand.ToString());
            io.inputs.Add(StateInputCommand.YES.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            int goldAfterSecondTransaction = goldAfterFirstTransaction - trainingGauntlets.Price;

            Assert.IsTrue(InventoryUtil.GetInventoryString(currentCharacter).Contains(trainingGauntlets.Name));
            //Amount of items in inventory = 3 => Next free inventory slot => 3
            Assert.IsTrue(InventoryUtil.GetNextFreeInventorySlot(currentCharacter) == 3);
            Assert.AreEqual(goldAfterSecondTransaction, currentCharacter.Gold);
            Assert.AreEqual(typeof(ShopBuyState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
        }
    }
}
