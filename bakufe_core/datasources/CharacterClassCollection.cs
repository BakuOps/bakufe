﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources
{
    public class CharacterClassCollection
    {
        private static readonly Dictionary<CharacterClassKey, CharacterClass> CharacterClassDictionary = GetCharacterClassDictionary();

        public static CharacterClass GetCharacterClassByKey(CharacterClassKey key)
        {
            if (!CharacterClassDictionary.ContainsKey(key))
            {
                return null;
            }
            else
            {
                return CharacterClassDictionary[key].ShallowCopy();
            }
        }

        /// <summary>
        /// Returns the list of all character classes belonging the given level
        /// </summary>
        /// <param name="level">the given level</param>
        /// <returns>the list of all character classes belonging the given level</returns>
        public static List<CharacterClass> GetCharacterClassesForLevel(CharacterClassLevel level)
        {
            List<CharacterClassLevel> levels = new List<CharacterClassLevel>();
            levels.Add(level);

            return GetCharacterClassesForLevel(levels);
        }

        /// <summary>
        /// Returns the list of all character classes belonging to those levels
        /// </summary>
        /// <param name="levels">The levels of the classes we are looking for</param>
        /// <returns>the list of all character classes belonging to those levels</returns>
        public static List<CharacterClass> GetCharacterClassesForLevel(List<CharacterClassLevel> levels)
        {
            List<CharacterClass> result = new List<CharacterClass>();

            foreach(CharacterClassKey key in CharacterClassDictionary.Keys)
            {
                CharacterClass currentClass = CharacterClassDictionary[key];
                if(levels.Contains(currentClass.Level))
                {
                    result.Add(currentClass);
                }
            }

            return result;
        }

        private static Dictionary<CharacterClassKey, CharacterClass> GetCharacterClassDictionary()
        {
            Dictionary<CharacterClassKey, CharacterClass> result = new Dictionary<CharacterClassKey, CharacterClass>()
            {
                { CharacterClassKey.BASE, new CharacterClass(
                       "Class Base",
                       CharacterClassLevel.BEGINNER,
                       CharacterClassKey.BASE,
                       Movementtype.INFANTRY,
                       new int[]{20,  5,  5,  6,  6,  4,  4,  4},
                       new int[]{ 0,  0,  0,  0,  0,  0,  0,  0},
                       new int[]{45, 40, 40, 40, 40, 40, 35, 35},
                       new SkillKey[]{ },
                       new SkillKey[]{ },
                       new ClassPromotionRequirement[]{ })
                },

                //-------------------------------------------------------------------------
                // Beginner Classes 
                // 54 BST - 20 HP, 34 on the rest
                // 1 Point of bonus on a stat
                // 15% Extra growth rates
                //-------------------------------------------------------------------------
                { CharacterClassKey.FIGHTER, new CharacterClass(
                       "Fighter",
                       CharacterClassLevel.BEGINNER,
                       CharacterClassKey.FIGHTER,
                       Movementtype.INFANTRY,
                       new int[]{20,  8,  3,  6,  6,  4,  5,  2},
                       new int[]{ 0,  1,  0,  0,  0,  0,  0,  0},
                       new int[]{55, 45, 40, 40, 40, 40, 35, 30},
                       new SkillKey[]{ },
                       new SkillKey[]{SkillKey.STR_PLUS },
                       new ClassPromotionRequirement[]{ })
                },
                { CharacterClassKey.MONK, new CharacterClass(
                       "Monk",
                       CharacterClassLevel.BEGINNER,
                       CharacterClassKey.MONK,
                       Movementtype.INFANTRY,
                       new int[]{20,  4,  8,  6,  6,  4,  3,  4},
                       new int[]{ 0,  0,  0,  0,  0,  0,  0,  1},
                       new int[]{50, 40, 40, 40, 40, 40, 35, 40},
                       new SkillKey[]{ },
                       new SkillKey[]{SkillKey.MAG_PLUS },
                       new ClassPromotionRequirement[]{ })
                },
                { CharacterClassKey.MYRMIDON, new CharacterClass(
                       "Myrmidon",
                       CharacterClassLevel.BEGINNER,
                       CharacterClassKey.MYRMIDON,
                       Movementtype.INFANTRY,
                       new int[]{20,  7,  3,  6,  6,  4,  5,  2},
                       new int[]{ 0,  0,  0,  0,  1,  0,  0,  0},
                       new int[]{55, 40, 40, 40, 45, 40, 35, 30},
                       new SkillKey[]{ },
                       new SkillKey[]{SkillKey.SPD_PLUS },
                       new ClassPromotionRequirement[]{ })
                },
                { CharacterClassKey.SOLDIER, new CharacterClass(
                       "Soldier",
                       CharacterClassLevel.BEGINNER,
                       CharacterClassKey.SOLDIER,
                       Movementtype.INFANTRY,
                       new int[]{20,  8,  3,  6,  6,  4,  5,  2},
                       new int[]{ 0,  0,  0,  1,  0,  0,  0,  0},
                       new int[]{55, 40, 40, 45, 40, 40, 35, 30},
                       new SkillKey[]{ },
                       new SkillKey[]{SkillKey.DEF_PLUS },
                       new ClassPromotionRequirement[]{ })
                },

                //-------------------------------------------------------------------------
                // Intermediate Classes 
                // 68 - 74 BST
                // ususally 4 Points of bonus on stat
                // around 25% Extra growth rates
                //-------------------------------------------------------------------------
                { CharacterClassKey.BRAWLER, new CharacterClass(
                       "Brawler",
                       CharacterClassLevel.INTERMEDIATE,
                       CharacterClassKey.BRAWLER,
                       Movementtype.INFANTRY,
                       new int[]{28,  9,  6,  7,  8,  8,  6,  1},
                       new int[]{ 1,  0,  0,  2,  2,  0,  0,  -1},
                       new int[]{75, 40, 30, 50, 50, 40, 35, 25},
                       new SkillKey[]{SkillKey.UNARMED_COMBAT_PLUS },
                       new SkillKey[]{SkillKey.UNARMED_COMBAT_PLUS },
                       new ClassPromotionRequirement[]{ new ClassPromotionRequirement(WeaponType.GAUNTLET, WeaponRank.C)})
                }
            };

            return result;
        }

        private CharacterClassCollection() { }
    }
}
