﻿using bakufe_core.entities;
using bakufe_core.io.provider;
using bakufe_core.persistence.character;
using bakufe_core.entities.util;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace bakufe
{
    public class BakuFERealmProvider
    {
        public static readonly BakuFERealm Realm = GetBakuFERealm();
            
        private static BakuFERealm GetBakuFERealm()
        {
            BakuFERealm realm = new BakuFERealm(new CommandlineInputOutputProvider(), new CharacterPersistenceTextfile(true), new BakuFERandom(), false);
            return realm;
        }
    }
}
