﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io.provider
{
    public class CommandlineInputOutputProvider : IInputOutputProvider
    {
        public IInputOutput GetStandardInputOutput()
        {
            return new CommandlineInputOutput();
        }
    }
}
