﻿using bakufe_core.entities.util;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.entities.util
{
    [TestFixture]
    public class ExperienceUtilTest
    {
        [Test]
        public void GetXPRequiredForNextLevelUp_TestCertainValues()
        {
            int XPRequiredForLevel2 = 110;
            int XPRequiredForLevel10 = 235;
            int XPRequiredForLevel15 = 379;
            int XPRequiredForLevel21 = 672;
            int XPRequiredForLevel30 = 1093;
            int XPRequiredForLevel40 = 1780;
            int XPRequiredForLevel50 = 2900;

            Assert.AreEqual(ExperienceUtil.NO_VALID_LEVEL_OUTPUT, ExperienceUtil.GetXPRequiredForNextLevelUp(-1));
            Assert.AreEqual(ExperienceUtil.NO_VALID_LEVEL_OUTPUT, ExperienceUtil.GetXPRequiredForNextLevelUp(0));
            Assert.AreEqual(XPRequiredForLevel2, ExperienceUtil.GetXPRequiredForNextLevelUp(1));
            Assert.AreEqual(XPRequiredForLevel10, ExperienceUtil.GetXPRequiredForNextLevelUp(9));
            Assert.AreEqual(XPRequiredForLevel15, ExperienceUtil.GetXPRequiredForNextLevelUp(14));
            Assert.AreEqual(XPRequiredForLevel21, ExperienceUtil.GetXPRequiredForNextLevelUp(20));
            Assert.AreEqual(XPRequiredForLevel30, ExperienceUtil.GetXPRequiredForNextLevelUp(29));
            Assert.AreEqual(XPRequiredForLevel40, ExperienceUtil.GetXPRequiredForNextLevelUp(39));
            Assert.AreEqual(XPRequiredForLevel50, ExperienceUtil.GetXPRequiredForNextLevelUp(49));
            Assert.AreEqual(ExperienceUtil.EXP_MAXED_OUT, ExperienceUtil.GetXPRequiredForNextLevelUp(50));
        }
    }
}
