﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.util
{
    /// <summary>
    /// Random class to capsule random calculations in this project with some extra util methods.
    /// It also eases up testing cause this can be replaced by a rigged variant for testing purposes,
    /// to make calculations predictable
    /// </summary>
    public class BakuFERandom
    {
        /// <summary>
        /// The default random object we will use
        /// </summary>
        private readonly Random random;

        public BakuFERandom()
        {
            this.random = new Random();
        }

        public BakuFERandom(int seed)
        {
            this.random = new Random(seed);
        }

        /// <summary>
        /// Returns a random integer contained in the range between min(included) and max(excluded)
        /// </summary>
        /// <param name="minIncluded">The minimum int value that is included in the result set</param>
        /// <param name="maxExcluded">The maximum int value that is excluded from the result set</param>
        /// <returns>a random integer contained in the range between min(included) and max(excluded)</returns>
        public virtual int Next(int minIncluded, int maxExcluded)
        {
            return random.Next(minIncluded, maxExcluded);
        }

        /// <summary>
        /// Returns the result of a double layerd rng roll, which is the average of the two rolled results
        /// </summary>
        /// <param name="minIncluded">The minimum int value that is included in the result set</param>
        /// <param name="maxExcluded">The maximum int value that is excluded from the result set</param>
        /// <returns></returns>
        public virtual int RollDoubleRandom(int minIncluded, int maxExcluded)
        {
            int roll1 = random.Next(minIncluded, maxExcluded);
            int roll2 = random.Next(minIncluded, maxExcluded);

            return (roll1 + roll2) / 2;
        }
    }
}
