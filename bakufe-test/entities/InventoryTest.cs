﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_test.states;
using Newtonsoft.Json.Bson;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_test.entities
{
    [TestFixture]
    public class InventoryTest : TestWithARealm
    {
        private static readonly string INVENTORY_ID = "Inventory";

        [Test]
        public void SortInventory_FillUpBlanksNoWeaponEquipped()
        {
            Character character = new Character
            {
                Name = "Byleth"
            };

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon steelSword = WeaponCollection.GetWeaponByKey(WeaponKey.STEEL_SWORD);
            Weapon rapier = WeaponCollection.GetWeaponByKey(WeaponKey.RAPIER);

            character.Inventory[2] = ironSword;
            character.Inventory[6] = steelSword;
            character.Inventory[4] = rapier;

            Console.WriteLine("--- Inventory before sorting ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            InventoryUtil.SortInventory(character);

            Console.WriteLine("--- Inventory after sorting ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            Assert.AreEqual(3, InventoryUtil.GetNextFreeInventorySlot(character));
            Assert.AreEqual(ironSword, character.Inventory[0]);
            Assert.AreEqual(rapier, character.Inventory[1]);
            Assert.AreEqual(steelSword, character.Inventory[2]);
        }

        [Test]
        public void SortInventory_PrioritizeEquippedWeapon()
        {
            Character character = new Character
            {
                Name = "Byleth"
            };

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon steelSword = WeaponCollection.GetWeaponByKey(WeaponKey.STEEL_SWORD);
            Weapon rapier = WeaponCollection.GetWeaponByKey(WeaponKey.RAPIER);

            character.Inventory[2] = ironSword;
            character.Inventory[6] = steelSword;
            character.Inventory[4] = rapier;

            character.EquippedWeaponSlot = 4;

            Console.WriteLine("--- Inventory before sorting ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            InventoryUtil.SortInventory(character);

            Console.WriteLine("--- Inventory after sorting ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            Assert.AreEqual(3, InventoryUtil.GetNextFreeInventorySlot(character));
            Assert.AreEqual(rapier, character.Inventory[0]);
            Assert.AreEqual(ironSword, character.Inventory[1]);
            Assert.AreEqual(steelSword, character.Inventory[2]);
        }

        [Test]
        public void EquipOrUnequipItem_EquipWeaponAndAccessory()
        {
            int[] beginnerBaseAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = beginnerBaseAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Accessory speedRing = AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING);

            character.Inventory[0] = ironSword;
            character.Inventory[1] = speedRing;

            Console.WriteLine("--- Inventory before first equipping ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // Equip Speedring first
            Assert.IsTrue(InventoryUtil.EquipOrUnequipItem(character, 1));

            Console.WriteLine("--- Inventory after first equip ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // Speedring is the only equipped item => is in slot 0
            Assert.AreEqual(speedRing, character.Inventory[0]);
            // Check if the EquippedAccessory-Pointer is set right
            Assert.AreEqual(0, character.EquippedAccessorySlot);
            // Check if the EquippedWeapon-Pointer is still not set
            Assert.AreEqual(-1, character.EquippedWeaponSlot);
            // Speedring boni get applied to the character stats
            Assert.AreEqual(beginnerBaseAttributes[(int)Attribute.SPD] + 2, character.GetAttributeValue(Attribute.SPD));

            // Equip iron sword
            Assert.IsTrue(InventoryUtil.EquipOrUnequipItem(character, 1));

            Console.WriteLine("--- Inventory after second equip ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // Iron sword is the first equipped item => is in slot 0
            Assert.AreEqual(ironSword, character.Inventory[0]);
            // Speedring is the second equipped item => is in slot 1
            Assert.AreEqual(speedRing, character.Inventory[1]);
            // Check if the pointers are set correctly
            Assert.AreEqual(0, character.EquippedWeaponSlot);
            Assert.AreEqual(1, character.EquippedAccessorySlot);
        }

        [Test]
        public void EquipOrUnequipItem_EquipWeaponCharacterCantWield()
        {
            Character character = new Character
            {
                Name = "Byleth"
            };
            Weapon steelSword = WeaponCollection.GetWeaponByKey(WeaponKey.STEEL_SWORD);

            character.Inventory[0] = steelSword;

            // Default character => All weapon ranks E, Steel sword requires D => Equip fails
            Assert.IsFalse(InventoryUtil.EquipOrUnequipItem(character, 0));
            Assert.AreEqual(-1, character.EquippedWeaponSlot);
        }

        [Test]
        public void EquipOrUnequipItem_UnequipWeaponAndAccessory()
        {
            int[] beginnerBaseAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = beginnerBaseAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Accessory speedRing = AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING);

            character.Inventory[0] = ironSword;
            character.Inventory[1] = speedRing;

            //"Equip" weapon and accesory
            character.EquippedWeaponSlot = 0;
            character.EquippedAccessorySlot = 1;

            Console.WriteLine("--- Inventory before first unequipping ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // Unequip weapon
            Assert.IsTrue(InventoryUtil.EquipOrUnequipItem(character, 0));

            Console.WriteLine("--- Inventory after first unequip ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // Weapon is unequipped, Speedring is the only equipped item => is in slot 0
            Assert.AreEqual(speedRing, character.Inventory[0]);
            // Check if the EquippedAccessory-Pointer is set right
            Assert.AreEqual(0, character.EquippedAccessorySlot);
            // Check if the EquippedWeapon-Pointer is still not set
            Assert.AreEqual(-1, character.EquippedWeaponSlot);
            // Speedring boni get applied to the character stats
            Assert.AreEqual(beginnerBaseAttributes[(int)Attribute.SPD] + 2, character.GetAttributeValue(Attribute.SPD));

            // Unequip ring
            Assert.IsTrue(InventoryUtil.EquipOrUnequipItem(character, 0));

            Console.WriteLine("--- Inventory after sencond unequip ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            // No Item equipped, since the ring was unequipped last it should remain the first item in the inventory
            Assert.AreEqual(speedRing, character.Inventory[0]);
            // Check if the EquippedAccessory-Pointer is set right
            Assert.AreEqual(-1, character.EquippedAccessorySlot);
            // Check if the EquippedWeapon-Pointer is still not set
            Assert.AreEqual(-1, character.EquippedWeaponSlot);
            // Speedring boni is not applied anymore
            Assert.AreEqual(beginnerBaseAttributes[(int)Attribute.SPD], character.GetAttributeValue(Attribute.SPD));
        }

        [Test]
        public void GetEquippedItem_ReturnEquippedWeaponsDefaultWeapon()
        {
            Character character = new Character
            {
                Name = "Byleth"
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER)));

            //No Weapon equipped => Equipped weapon pointer is at -1
            Assert.AreEqual(-1, character.EquippedWeaponSlot);
            //No Weapon equipped => GetEquippedWeapon returns the default fists
            Assert.AreEqual(WeaponKey.UNARMED, InventoryUtil.GetEquippedWeapon(character).WeaponKey);
        }


        [Test]
        public void AddItemToInventory_AddGenericItem()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(INVENTORY_ID);

            int sealsPreAdding = 3;
            int initalFreeInventorySlot = 3;
            StateTestInputOutput io = new StateTestInputOutput();

            Assert.IsTrue(currentCharacter.Inventory[1] != null);
            Assert.AreEqual(typeof(GenericItem), currentCharacter.Inventory[1].GetType());
            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));

            GenericItem masterSeals = (GenericItem)currentCharacter.Inventory[1];
            Assert.AreEqual(sealsPreAdding, masterSeals.CurrentStackSize);
            Assert.IsTrue(currentCharacter.Inventory[initalFreeInventorySlot] == null);

            Console.WriteLine("--- Initial inventory ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(currentCharacter));
            Console.WriteLine();

            //Add a seal
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(sealsPreAdding + 1, masterSeals.CurrentStackSize);
            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.IsTrue(currentCharacter.Inventory[initalFreeInventorySlot] == null);

            Console.WriteLine("--- Inventory after first add ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(currentCharacter));
            Console.WriteLine();

            //Add another seal -> Stack is now full
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(sealsPreAdding + 2, masterSeals.CurrentStackSize);
            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.IsTrue(currentCharacter.Inventory[initalFreeInventorySlot] == null);

            Console.WriteLine("--- Inventory after second add ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(currentCharacter));
            Console.WriteLine();

            //Add yet another seal -> Overflow -> New Stack
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(sealsPreAdding + 2, masterSeals.CurrentStackSize);
            Assert.AreEqual(initalFreeInventorySlot + 1, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.IsFalse(currentCharacter.Inventory[initalFreeInventorySlot] == null);
            Assert.AreEqual(typeof(GenericItem), currentCharacter.Inventory[initalFreeInventorySlot].GetType());

            GenericItem secondStackOfSeals = (GenericItem)currentCharacter.Inventory[initalFreeInventorySlot];
            Assert.AreEqual(GenericItemKey.MASTER_SEAL, secondStackOfSeals.GenericItemKey);
            Assert.AreEqual(1, secondStackOfSeals.CurrentStackSize);

            Console.WriteLine("--- Inventory after last add ---");
            Console.WriteLine(InventoryUtil.GetInventoryString(currentCharacter));
            Console.WriteLine();
        }

        [Test]
        public void DoesCharacterCarryItem_DifferentItemChecks()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(INVENTORY_ID);

            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, WeaponKey.IRON_SWORD.ToString()));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, GenericItemKey.MASTER_SEAL.ToString()));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, AccessoryKey.SPEED_RING.ToString()));

            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, WeaponKey.IRON_BOW.ToString()));
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, GenericItemKey.ADVANCED_SEAL.ToString()));
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, AccessoryKey.ACCURACY_RING.ToString()));
        }

        [Test]
        public void DoesCharacterPossesAmountOfItems_DifferentAmounts()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(INVENTORY_ID);

            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, WeaponKey.IRON_SWORD.ToString(), 1));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, GenericItemKey.MASTER_SEAL.ToString(), 1));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, GenericItemKey.MASTER_SEAL.ToString(), 2));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, GenericItemKey.MASTER_SEAL.ToString(), 3));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, AccessoryKey.SPEED_RING.ToString(), 1));

            Assert.IsFalse(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, WeaponKey.IRON_SWORD.ToString(), 2));
            Assert.IsFalse(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, AccessoryKey.SPEED_RING.ToString(), 2));
            Assert.IsFalse(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, GenericItemKey.MASTER_SEAL.ToString(), 4));
        }

        [Test]
        public void RemoveItemFromCharacterInventory_SimpleRemove()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(INVENTORY_ID);

            int sealsPreAdding = 3;
            int initalFreeInventorySlot = 3;
            StateTestInputOutput io = new StateTestInputOutput();

            Assert.IsTrue(currentCharacter.Inventory[1] != null);
            Assert.AreEqual(typeof(GenericItem), currentCharacter.Inventory[1].GetType());
            GenericItem masterSeals = (GenericItem)currentCharacter.Inventory[1];
            Assert.AreEqual(sealsPreAdding, masterSeals.CurrentStackSize);

            Assert.IsTrue(realm.RemoveItemFromCharacterInventory(io, currentCharacter.ID, masterSeals, 2));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.ITEM_REMOVE_MULTIPLE, 2, masterSeals.Name, currentCharacter.Name)));
            Assert.AreEqual(sealsPreAdding - 2, masterSeals.CurrentStackSize);
            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));

            io.Reset();

            Assert.IsTrue(realm.RemoveItemFromCharacterInventory(io, currentCharacter.ID, masterSeals, 1));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.ITEM_REMOVE_SINGLE, masterSeals.Name, currentCharacter.Name)));
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, masterSeals.ItemID));
            Assert.AreEqual(initalFreeInventorySlot - 1, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
        }

        [Test]
        public void RemoveItemFromCharacterInventory_TestStackSpanningRemove()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(INVENTORY_ID);

            int sealsPreAdding = 3;
            int initalFreeInventorySlot = 3;
            StateTestInputOutput io = new StateTestInputOutput();

            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.IsTrue(currentCharacter.Inventory[1] != null);
            Assert.AreEqual(typeof(GenericItem), currentCharacter.Inventory[1].GetType());
            GenericItem masterSeals = (GenericItem)currentCharacter.Inventory[1];
            Assert.AreEqual(sealsPreAdding, masterSeals.CurrentStackSize);

            //Add some seals to create the situation
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(sealsPreAdding + 1, masterSeals.CurrentStackSize);
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(sealsPreAdding + 2, masterSeals.CurrentStackSize);
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.AreEqual(initalFreeInventorySlot + 1, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, masterSeals.ItemID, 6));
            Assert.IsTrue(realm.AddItemToCharacterIventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL)));
            Assert.IsTrue(realm.DoesCharacterPossesAmountOfItems(currentCharacter.ID, masterSeals.ItemID, 7));

            io.Reset();
            //And take away 5 of them -> the newly created stack vanishes and there are 2 left in the initial one
            Assert.IsTrue(realm.RemoveItemFromCharacterInventory(io, currentCharacter.ID, GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL), 5));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.ITEM_REMOVE_MULTIPLE, 5, masterSeals.Name, currentCharacter.Name)));
            Assert.AreEqual(initalFreeInventorySlot, InventoryUtil.GetNextFreeInventorySlot(currentCharacter));
            Assert.AreEqual(sealsPreAdding - 1, masterSeals.CurrentStackSize);
        }
    }
}
