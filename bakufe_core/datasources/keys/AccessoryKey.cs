﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources.keys
{
    public enum AccessoryKey
    {
        //Rings
        ACCURACY_RING,
        CRITICAL_RING,
        SPEED_RING
    }
}
