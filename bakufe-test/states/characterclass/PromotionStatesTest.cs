﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.io;
using bakufe_core.state;
using bakufe_test.entities.util;
using NUnit.Framework;

namespace bakufe_test.states.characterclass
{
    [TestFixture]
    public class PromotionStatesTest : TestWithARealm
    {
        private static readonly string CLASS_TESTER_ID = "Class";

        [Test]
        public void Promotion_ProperPromotion()
        {
            //Set up the random object
            PredictableBakuFERandom random = new PredictableBakuFERandom();
            random.SequenceValues.Add(0);

            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID, random);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.PROMOTE.command);
            io.inputs.Add("0");
            io.inputs.Add(StateInputCommand.YES.command);
            io.inputs.Add(StateInputCommand.YES.command);

            GenericItem beginnerSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.BEGINNER_SEAL);
            GenericItem intermediateSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.INTERMEDIATE_SEAL);
            CharacterClass monk = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK);

            Assert.AreEqual(2, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(3, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.AreEqual(CharacterClassKey.MONK, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_POSITIVE_ATTEMPT, currentCharacter.Name, monk.Name)));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_CLASS_CHANGE, currentCharacter.Name, monk.Name)));
        }

        [Test]
        public void Promotion_ProperPromotion_RemainInCurrentClass()
        {
            //Set up the random object
            PredictableBakuFERandom random = new PredictableBakuFERandom();
            random.SequenceValues.Add(0);

            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID, random);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.PROMOTE.command);
            io.inputs.Add("1");
            io.inputs.Add(StateInputCommand.YES.command);
            io.inputs.Add(StateInputCommand.NO.command);

            GenericItem beginnerSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.BEGINNER_SEAL);
            GenericItem intermediateSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.INTERMEDIATE_SEAL);
            CharacterClass myrmidon = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MYRMIDON);
            CharacterClass soldier = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER);

            Assert.AreEqual(2, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(3, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.AreEqual(CharacterClassKey.MYRMIDON, currentCharacter.AcquieredCharacterClasses[currentCharacter.AcquieredCharacterClasses.Count - 1].Class.CharacterClassKey);
            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_POSITIVE_ATTEMPT, currentCharacter.Name, myrmidon.Name)));
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_NO_CHANGE_DONE, currentCharacter.Name, soldier.Name)));
        }

        [Test]
        public void Promotion_FailedAttempt()
        {
            //Set up the random object
            PredictableBakuFERandom random = new PredictableBakuFERandom();
            random.SequenceValues.Add(99);

            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CLASS_TESTER_ID, random);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.PROMOTE.command);
            io.inputs.Add("2");
            io.inputs.Add(StateInputCommand.YES.command);

            GenericItem beginnerSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.BEGINNER_SEAL);
            GenericItem intermediateSeal = GenericItemCollection.GetGenericItemByKey(GenericItemKey.INTERMEDIATE_SEAL);

            Assert.AreEqual(2, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(2, currentCharacter.AcquieredCharacterClasses.Count);
            Assert.AreEqual(CharacterClassKey.SOLDIER, currentCharacter.GetCurrentClass().CharacterClassKey);
            Assert.IsTrue(realm.DoesCharacterCarryItem(currentCharacter.ID, beginnerSeal.ItemID));
            Assert.IsFalse(realm.DoesCharacterCarryItem(currentCharacter.ID, intermediateSeal.ItemID));
            Assert.IsTrue(io.DoesIOObjectContainString(IOPatterns.STATE_PROMOTION_CONFIRMATION_NEGATIVE_ATTEMPT));
        }
    }
}
