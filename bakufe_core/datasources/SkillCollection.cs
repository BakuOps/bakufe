﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources
{
    public class SkillCollection
    {
        public static Dictionary<SkillKey, Skill> SkillDictionary = GetSkillDictionary();

        private static Dictionary<SkillKey, Skill> GetSkillDictionary() => new Dictionary<SkillKey, Skill>() {

            //---------------------------------------------------
            // Attribute ups
            //---------------------------------------------------
            { SkillKey.HP_PLUS, new Skill("HP +5", "Increases maximum HP by 5.", SkillKey.HP_PLUS) },
            { SkillKey.STR_PLUS, new Skill("STR +2", "Increases STR by 2.", SkillKey.STR_PLUS) },
            { SkillKey.MAG_PLUS, new Skill("MAG +2", "Increases MAG by 2.", SkillKey.MAG_PLUS) },
            { SkillKey.DEX_PLUS, new Skill("DEX +4", "Increases DEX by 4.", SkillKey.DEX_PLUS) },
            { SkillKey.SPD_PLUS, new Skill("SPD +2", "Increases SPD by 2.", SkillKey.SPD_PLUS) },
            { SkillKey.DEF_PLUS, new Skill("DEF +2", "Increases DEF by 2.", SkillKey.DEF_PLUS) },
            { SkillKey.RES_PLUS, new Skill("RES +2", "Increases RES by 2.", SkillKey.RES_PLUS) },

            //---------------------------------------------------
            // In combat
            //---------------------------------------------------
            { SkillKey.VANTAGE, new Skill("Vantage", "When foe initiates combat, unit still attacks first if HP is ≤ 50%.", SkillKey.VANTAGE) },
            { SkillKey.UNARMED_COMBAT_PLUS, new Skill("Advanced unarmed combat", "This character is rather adept at fighting with their bare fists.", SkillKey.UNARMED_COMBAT_PLUS) },
        };

        private SkillCollection() { }
    }
}
