﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.entities
{
    [TestFixture]
    public class CharacterClassTest
    {

        [Test]
        public void CharacterClass_GetMinimumStats()
        {
            int[] beginnerBaseStats = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;
            CharacterClass fighter = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER);

            //Create a character and give them the BeginnerBase Stats and Monk as a class
            Character character = new Character
            {
                Attributes = beginnerBaseStats
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            //Beginner Base Strength > Monk Strenght => GetAttributeValue returns the strength value of BeginnerBase
            Assert.AreEqual(beginnerBaseStats[(int)bakufe_core.entities.enums.Attribute.STR], character.GetAttributeValue(bakufe_core.entities.enums.Attribute.STR));

            //Change Character Class to Fighter
            character.AcquieredCharacterClasses.Clear();
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(fighter));

            //Fighter Strength > Beginner Base Strenght => GetAttributeValue returns the strength value of the Fighter Class + the Attribute bonus (+1)
            Assert.AreEqual(fighter.BaseAttributes[(int)bakufe_core.entities.enums.Attribute.STR] + fighter.AttributeBonuses[(int)bakufe_core.entities.enums.Attribute.STR], character.GetAttributeValue(bakufe_core.entities.enums.Attribute.STR));
        }
    }
}
