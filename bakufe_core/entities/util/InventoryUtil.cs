﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace bakufe_core.entities.util
{
    public class InventoryUtil
    {
        public static readonly int MAX_INVENTORY_SLOTS = 8;
        public static readonly int NO_SLOT_AVAILABLE = -1;

        private static readonly string EMPTY_INVENTORY_SLOT_REPRESENTATION = "-/-";
        private static readonly string EQUIPPED_MARKER = "(e)";

        /// <summary>
        /// Returns the index of the next free inventory slot or -1 if there is none
        /// </summary>
        /// <param name="character">The character whoose inventory will be checked</param>
        /// <returns>the index of the next free inventory slot or -1 if there is none</returns>
        public static int GetNextFreeInventorySlot(Character character)
        {
            return GetNextFreeInventorySlot(character.Inventory);
        }

        /// <summary>
        /// Returns the index of the next free inventory slot or -1 if there is none
        /// </summary>
        /// <param name="inventory">the given inventory</param>
        /// <returns>the index of the next free inventory slot or -1 if there is none</returns>
        private static int GetNextFreeInventorySlot(ItemBase[] inventory)
        {
            for (int i = 0; i < MAX_INVENTORY_SLOTS; i++)
            {
                if (inventory[i] == null)
                {
                    return i;
                }
            }

            return NO_SLOT_AVAILABLE;
        }

        /// <summary>
        /// Tries to equip the item in the selected slot onto the character.
        /// In case it is a currently not equipped item, it replaces the item in the fitting slot, if there is one
        /// If the command targets an already equipped slot the item get's unequipped.
        /// </summary>
        /// <param name="character">The character who shall be equipped</param>
        /// <param name="targetSlot">The slot that contains the item that should be equipped</param>
        /// <returns>true if the operation was successful, false if there was a problem</returns>
        public static bool EquipOrUnequipItem(Character character, int targetSlot)
        {
            if(targetSlot == character.EquippedWeaponSlot)
            {
                //If a weapon is "equipped again" unequip it
                character.EquippedWeaponSlot = -1;
                SortInventory(character);
                return true;
            }

            if (targetSlot == character.EquippedAccessorySlot)
            {
                //If a accessory is "equipped again" unequip it
                character.EquippedAccessorySlot = -1;
                SortInventory(character);
                return true;
            }

            //Check if the target slot is actually filled and if yes if it's an equipable item
            ItemBase item = character.Inventory[targetSlot];
            if(item == null)
            {
                return false;
            }
            
            if(item.GetType() == typeof(Weapon)){
                //In case it's a weapon, check if the character's weapon level is high enough that he can equip it
                Weapon weapon = (Weapon)character.Inventory[targetSlot];
                if(WeaponUtil.CanCharacterEquipWeapon(character, weapon))
                {
                    character.EquippedWeaponSlot = targetSlot;
                    SortInventory(character);
                    return true;
                }
                else
                {
                    return false;
                }
            } 
            else if (item.GetType() == typeof(Accessory))
            {
                character.EquippedAccessorySlot = targetSlot;
                SortInventory(character);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Tries to add the item to the character's inventory and returns if that action was successful or not
        /// </summary>
        /// <param name="character">The character who shall be given the item</param>
        /// <param name="item">The item that will be added </param>
        /// <returns></returns>
        public static bool AddItemToInventory(Character character, ItemBase item)
        {
            if(item.GetType() == typeof(GenericItem))
            {
                ItemBase[] inventory = character.Inventory;
                GenericItem newItem = (GenericItem)item;

                //Search for a stack to add this new item to
                for (int i = 0; i < inventory.Length; i++)
                {
                    ItemBase cItem = inventory[i];

                    //Check if the current item is a generic item
                    if (cItem != null && cItem.GetType() == typeof(GenericItem))
                    {
                        GenericItem gItem = (GenericItem)cItem;

                        //Check if the GenericItem is of the same type and if there is still room in the stack
                        if (newItem.GenericItemKey == gItem.GenericItemKey && gItem.CurrentStackSize < gItem.MaxStackSize)
                        {
                            //Add the current item to the stack
                            gItem.CurrentStackSize++;
                            return true;
                        }
                    }
                }
                //If there is no valid stack add it like a regular item
            }

            int nextFreeInventorySlot = GetNextFreeInventorySlot(character);
            if (nextFreeInventorySlot == NO_SLOT_AVAILABLE)
            {
                return false;
            }
            character.Inventory[nextFreeInventorySlot] = item;
            return true;
        }

        /// <summary>
        /// Checks if the character's inventory contains the given item
        /// </summary>
        /// <param name="character">The character who's inventory shall be checked</param>
        /// <param name="itemId">The id of the required item</param>
        /// <returns></returns>
        public static bool IsItemContainedInInventory(Character character, string itemId)
        {
            return DoesCharacterInventoryContainTheRequiredAmount(character, itemId, 1);
        }

        /// <summary>
        /// Checks if the character's inventory contains the required amount of the given item
        /// </summary>
        /// <param name="character">The character who's inventory shall be checked</param>
        /// <param name="itemId">The id of the required item</param>
        /// <param name="amount">The amount for which will be checked</param>
        /// <returns>if the character's inventory contains the required amount of the given item</returns>
        public static bool DoesCharacterInventoryContainTheRequiredAmount(Character character, string itemId, int amount)
        {
            ItemBase[] inventory = character.Inventory;
            int missingAmount = amount;
            for(int i = 0; i < MAX_INVENTORY_SLOTS; i++)
            {
                ItemBase currentItem = inventory[i];
                if(currentItem != null && currentItem.ItemID == itemId)
                {
                    missingAmount -= currentItem.CurrentStackSize;
                    if(missingAmount <= 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the given amount of items with the given Id from the character's inventory
        /// </summary>
        /// <param name="character">The character who's inventory the item shall be removed from</param>
        /// <param name="itemId">The id of the item to remove</param>
        /// <param name="amount">The amount of items that shall be removed</param>
        /// <returns>Whether or not all the required items could be removed</returns>
        public static bool RemoveItemFromCharacterInventory(Character character, string itemId, int amount)
        {
            ItemBase[] inventory = character.Inventory;
            int amountToRemove = amount;

            for(int i = MAX_INVENTORY_SLOTS - 1; i >= 0; i--)
            {
                ItemBase currentItem = inventory[i];
                if (currentItem != null && currentItem.ItemID == itemId)
                {
                    if(currentItem.CurrentStackSize > amountToRemove)
                    {
                        currentItem.CurrentStackSize -= amountToRemove;
                        SortInventory(character);
                        return true;
                    }
                    else
                    {
                        amountToRemove -= currentItem.CurrentStackSize;
                        inventory[i] = null;
                        if(amountToRemove <= 0)
                        {
                            SortInventory(character);
                            return true;
                        }
                    }
                }
            }

            SortInventory(character);

            return false;
        }

        /// <summary>
        /// Sorts the given character's inventory, prioritizing the equipped weapon and accesory to the top,
        /// while keeping the remaining order intact and filling up empty slots
        /// </summary>
        /// <param name="character">The character who's inventory should be sortet</param>
        public static void SortInventory(Character character)
        {
            ItemBase[] oldInventory = character.Inventory;
            ItemBase[] newInventory = new ItemBase[MAX_INVENTORY_SLOTS];

            //Equipped weapon will end up in the first slot if there is one
            if(character.EquippedWeaponSlot > -1)
            {
                newInventory[0] = oldInventory[character.EquippedWeaponSlot];
                oldInventory[character.EquippedWeaponSlot] = null;
                //Adjust the equipped weapon slot pointer
                character.EquippedWeaponSlot = 0;
            }
            //Find next free slot
            int nextFreeSlot = GetNextFreeInventorySlot(newInventory);
            //Pick up the equipped accessory 
            if(character.EquippedAccessorySlot > -1)
            {
                newInventory[nextFreeSlot] = oldInventory[character.EquippedAccessorySlot];
                oldInventory[character.EquippedAccessorySlot] = null;
                //Adjust the equipped accessory slot pointer
                character.EquippedAccessorySlot = nextFreeSlot;
            }

            //Transfer over the remaining items in their prior order
            nextFreeSlot = GetNextFreeInventorySlot(newInventory);
            for (int i = 0; i < MAX_INVENTORY_SLOTS; i++)
            {
                if(oldInventory[i] != null)
                {
                    newInventory[nextFreeSlot] = oldInventory[i];
                    nextFreeSlot = GetNextFreeInventorySlot(newInventory);
                }
            }

            //Swap over to the new and sorted inventory
            character.Inventory = newInventory;
        }

        /// <summary>
        /// Returns the character's equipped weapon or null if the don't wield one
        /// </summary>
        /// <param name="character">The character in question</param>
        /// <returns>the character's equipped weapon or null if the don't wield one</returns>
        public static Weapon GetEquippedWeapon(Character character)
        {
            return (Weapon)GetEquippedItem(character, true);
        }

        /// <summary>
        /// Returns the character's equipped accessory or null if the don't have one equipped
        /// </summary>
        /// <param name="character">The character in question</param>
        /// <returns>the character's equipped accessory or null if the don't have one equipped</returns>
        public static Accessory GetEquippedAccessory(Character character)
        {
            return (Accessory)GetEquippedItem(character, false);
        }

        private static ItemBase GetEquippedItem(Character character, bool isAskingForWeapon)
        {
            if (isAskingForWeapon)
            {
                if(character.EquippedWeaponSlot == NO_SLOT_AVAILABLE)
                {
                    //#2 in case the character is not wielding a weapon, replace it with the default one
                    //#6 in case the character wields unarmed plus skill, they get the upgraded version of the fists
                    if(SkillUtil.DoesCharacterHaveSkillEquipped(character, datasources.keys.SkillKey.UNARMED_COMBAT_PLUS))
                    {
                        return WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED_PLUS);
                    }
                    else
                    {
                        return WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
                    }
                }

                return character.Inventory[character.EquippedWeaponSlot];
            }
            else
            {
                //Not asking for a weapon => asking for the accesory
                if (character.EquippedAccessorySlot == NO_SLOT_AVAILABLE)
                {
                    return null;
                }

                return character.Inventory[character.EquippedAccessorySlot];
            }
        }

        /// <summary>
        /// Returns a string representation of the given character's inventory
        /// </summary>
        /// <param name="character">The given character</param>
        /// <returns>a string representation of the given character's inventory</returns>
        public static string GetInventoryString(Character character)
        {
            string result = "";
            ItemBase[] inventory = character.Inventory;

            result += String.Format("{0}'s Inventory:\n", character.Name);
            for (int i = 0; i < MAX_INVENTORY_SLOTS; i++)
            {
                string itemRepresentation;
                string equippedMarker = "";
                if (inventory[i] == null)
                {
                    itemRepresentation = EMPTY_INVENTORY_SLOT_REPRESENTATION;
                }
                else
                {
                    itemRepresentation = inventory[i].GetItemStringRepresentation();
                    if(i == character.EquippedWeaponSlot || i == character.EquippedAccessorySlot)
                    {
                        equippedMarker = EQUIPPED_MARKER;
                    }
                }

                result += string.Format(IOPatterns.INVENTORY_ITEM_PATTERN, i, itemRepresentation, equippedMarker);
            }

            return result;
        }

        /// <summary>
        /// Returns the WeaponKeys of the weapons the given character can buy at the weapon shop.
        /// This one could depends on level and other factors.
        /// For now it returns just all the available training weapons
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns>the WeaponKeys of the weapons the given character can buy at the weapon shop</returns>
        public static List<WeaponKey> GetShopInventoryForCharacter(Character character)
        {
            List<WeaponKey> result = new List<WeaponKey>();

            //Swords
            result.Add(WeaponKey.TRAINING_SWORD);

            //Lances

            //Axes

            //Bows
            result.Add(WeaponKey.TRAINING_BOW);

            //Gauntlets
            result.Add(WeaponKey.TRAINING_GAUNTLETS);

            //Anima tomes
            result.Add(WeaponKey.FIRE);

            //Dark tomes

            //Light tomes

            return result;
        }

        /// <summary>
        /// Returns the weapons that the current character can sell to the weapon shop from their inventory.
        /// Those weapon's can't be equipped.
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns>the weapons that the current character can sell to the weapon shop from their inventory</returns>
        public static List<Weapon> GetWeaponsThatCanBeSold(Character character)
        {
            ItemBase[] inventory = character.Inventory;
            List<Weapon> result = new List<Weapon>();

            for(int i = 0; i < inventory.Length; i++)
            {
                if(inventory[i] != null && i != character.EquippedWeaponSlot && inventory[i].GetType() == typeof(Weapon))
                {
                    result.Add((Weapon)inventory[i]);
                }
            }

            return result;
        }
    }
}
