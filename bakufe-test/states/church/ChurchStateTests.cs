﻿using bakufe_core.entities;
using bakufe_core.io;
using bakufe_core.state;
using bakufe_core.state.impl;
using bakufe_core.state.impl.church;
using bakufe_test.entities.util;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.states.church
{

    [TestFixture]
    public class ChurchStateTests : TestWithARealm
    {
        [Test]
        public void TestDonationState()
        {
            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID);

            int startingGold = 1000;
            int donationAmount = 100;
            currentCharacter.Gold = startingGold;

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHURCH.command);
            io.inputs.Add(StateInputCommand.DONATE.command);
            io.inputs.Add(donationAmount.ToString());

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            //Evaluate the result
            //After the donation state, the character still remains in the church state
            Assert.AreEqual(typeof(ChurchState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(startingGold - donationAmount, currentCharacter.Gold);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_CHURCHDONATION_AMOUNT, currentCharacter.Name, donationAmount)));
        }

        [Test]
        public void TestPrayerState()
        {
            PredictableBakuFERandom random = new PredictableBakuFERandom();
            random.SequenceValues.Add(0);
            random.SequenceValues.Add(5);
            random.SequenceValues.Add(8);
            random.SequenceValues.Add(9);

            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID, random);

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHURCH.command);
            io.inputs.Add(StateInputCommand.PRAY.command);
            io.inputs.Add(StateInputCommand.PRAY.command);
            io.inputs.Add(StateInputCommand.PRAY.command);
            io.inputs.Add(StateInputCommand.PRAY.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            //Evaluate the result
            //After the praying state, the character still remains in the church state
            Assert.AreEqual(typeof(ChurchState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_PRAY_DESCRIPTION_1, CHARACTER_TESTER_NAME)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_PRAY_DESCRIPTION_2, CHARACTER_TESTER_NAME)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_PRAY_DESCRIPTION_3, CHARACTER_TESTER_NAME)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_PRAY_DESCRIPTION_4, CHARACTER_TESTER_NAME)));
        }

        [Test]
        public void TestHospitalState()
        {
            int startingGold = 100;
            int healingCost = 25;

            //Setup test realm
            BakuFERealm realm = CreateEmptyRealmAndLoadCharacter(CHARACTER_TESTER_ID);

            currentCharacter.Gold = startingGold;

            //prepare io object
            StateTestInputOutput io = (StateTestInputOutput)realm.GetEmptyInputOutput();
            io.inputs.Add(StateInputCommand.CHURCH.command);
            io.inputs.Add(StateInputCommand.HOSPITAL.command);
            io.inputs.Add(StateInputCommand.YES.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            //Evaluate the result
            //After the healing state, the character still remains in the church
            Assert.AreEqual(typeof(ChurchState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(CHARACTER_TESTER_MAXHP, currentCharacter.CurrentHP);
            Assert.AreEqual(startingGold - healingCost, currentCharacter.Gold);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_HOSPITAL_TREATMENT, CHARACTER_TESTER_NAME)));

            //Clear the io object and head in there once more, without the need for healing 
            io.Reset();
            io.inputs.Add(StateInputCommand.HOSPITAL.command);

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            string expectedString = string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION, CHARACTER_TESTER_NAME) + string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION_FULL_HEALTH, CHARACTER_TESTER_NAME);

            Assert.AreEqual(typeof(ChurchState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.IsTrue(io.DoesIOObjectContainString(expectedString));

            //Clear the io object once more, damage the character and access the hospital from the idle state
            io.Reset();
            io.inputs.Add(StateInputCommand.LEAVE.command);
            io.inputs.Add(StateInputCommand.HOSPITAL.command);
            io.inputs.Add(StateInputCommand.YES.command);

            currentCharacter.CurrentHP = 15;

            //Throw the inputs into the realm
            FeedRealmWithInput(realm, io);

            Assert.AreEqual(typeof(IdleState), realm.GetCharactersCurrentState(currentCharacter.ID).GetType());
            Assert.AreEqual(CHARACTER_TESTER_MAXHP, currentCharacter.CurrentHP);
            Assert.AreEqual(startingGold - healingCost * 2, currentCharacter.Gold);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.STATE_HOSPITAL_TREATMENT, CHARACTER_TESTER_NAME)));
        }
    }
}
