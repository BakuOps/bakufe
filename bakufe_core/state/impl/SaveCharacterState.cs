﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl
{
    /// <summary>
    /// Confirmation stated that prompts the player to save the current character's data.
    /// Does so in case of a positive reply
    /// Does nothing in case of a negative reply
    /// </summary>
    public class SaveCharacterState : ConfirmationState
    {
        public SaveCharacterState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_SAVE_NAME)
        {
            Description = String.Format(IOPatterns.STATE_SAVE_DESCRIPTION, character.Name);
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            io.PushOutput(IOPatterns.STATE_SAVE_DECLINED);
            IdleState idleState = new IdleState(realm, character);
            idleState.ChangeToThisCharacterState(io);
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            bool result = realm.PersistCharacter(character.ID);
            if (result)
            {
                io.PushOutput(String.Format(IOPatterns.STATE_SAVE_POSITIVE_RESULT, character.Name));
            }
            else
            {
                io.PushOutput(String.Format(IOPatterns.STATE_SAVE_NEGATIVE_RESULT, character.Name));
            }
            IdleState idleState = new IdleState(realm, character);
            idleState.ChangeToThisCharacterState(io);
        }
    }
}
