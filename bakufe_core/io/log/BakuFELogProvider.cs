﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io.log
{
    public class BakuFELogProvider
    {
        private static readonly string LOG_PATH = "logs/bakufe_.log";

        public static ILogger Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                //.WriteTo.Console()
                .WriteTo.File(LOG_PATH, rollingInterval: RollingInterval.Day)
                .CreateLogger();

        private BakuFELogProvider() { }
    }
}
