﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_core.persistence.character;
using bakufe_test.entities.util;
using bakufe_test.states;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.entities
{
    [TestFixture]
    public class BattleTest
    {
        private static readonly string BATTLE_TESTER_ID = "BattleTester";
        private static readonly string BATTLE_TESTER_VANTAGE_ID = "BattleTesterVantage";

        private static readonly int BATTLE_TESTER_START_HP = 20;

        [Test]
        public void TestExecuteAttackAction_NormalAttack()
        {
            int expectedDamage = 8;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(1);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteAttackAction(io, firstCharacter, secondCharacter);

            Assert.IsTrue(firstCharacter.CurrentHP == BATTLE_TESTER_START_HP);
            Assert.IsTrue(secondCharacter.CurrentHP == BATTLE_TESTER_START_HP - expectedDamage);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_DAMAGE, firstCharacter.Name, secondCharacter.Name, expectedDamage)));
        }

        [Test]
        public void TestExecuteAttackAction_CriticalAttack()
        {
            int expectedDamage = 8 * 3;
            int hpExpanded = 25;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            secondCharacter.Attributes[(int)bakufe_core.entities.enums.Attribute.HP] = hpExpanded;
            secondCharacter.CurrentHP = hpExpanded;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(0);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteAttackAction(io, firstCharacter, secondCharacter);

            Assert.IsTrue(firstCharacter.CurrentHP == BATTLE_TESTER_START_HP);
            Assert.IsTrue(secondCharacter.CurrentHP == hpExpanded - expectedDamage);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_DAMAGE, firstCharacter.Name, secondCharacter.Name, expectedDamage)));
            Assert.IsTrue(io.DoesIOObjectContainString(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT));
        }

        [Test]
        public void TestExecuteBattle_FullBattle()
        {
            string firstCharacterIdName = "FirstCharacter";
            string secondCharacterIdName = "SecondCharacter";

            int damagePerAttack = 8;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            firstCharacter.ID = firstCharacterIdName;
            firstCharacter.Name = firstCharacterIdName;

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            secondCharacter.ID = secondCharacterIdName;
            secondCharacter.Name = secondCharacterIdName;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(1);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteBattle(io);

            //Check if the battle is won by the correct character
            Assert.AreEqual(firstCharacterIdName, actual: battle.GetWinningCharacter().ID);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, secondCharacterIdName, firstCharacterIdName)));
            //Check remaining HP
            Assert.IsTrue(firstCharacter.CurrentHP == BATTLE_TESTER_START_HP - damagePerAttack * 2);
            Assert.IsTrue(secondCharacter.CurrentHP == 0);
            //Check combat rounds:  Round 1-3 were played out, 4-5 did not happen
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 1)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 2)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 3)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 4)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 5)));
            //Check: No crits, no misses
            Assert.IsFalse(io.DoesIOObjectContainString(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, secondCharacterIdName)));
        }

        [Test]
        public void TestExecuteAttackAction_VampircHealing()
        {
            int expectedDamage = 4;
            int expectedHealing = 2;
            int startingHP = 18;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            firstCharacter.EquippedWeaponSlot = 1;
            firstCharacter.CurrentHP = startingHP;

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            secondCharacter.CurrentHP = startingHP;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(50);
            riggedRandom.SequenceValues.Add(50);
            riggedRandom.SequenceValues.Add(99);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteAttackAction(io, firstCharacter, secondCharacter);

            Assert.AreEqual(WeaponKey.NOSFERATU, InventoryUtil.GetEquippedWeapon(firstCharacter).WeaponKey);
            Assert.AreEqual(startingHP + expectedHealing, firstCharacter.CurrentHP);
            Assert.AreEqual(startingHP - expectedDamage, secondCharacter.CurrentHP);
            Assert.IsTrue(io.DoesIOObjectContainString(string.Format(IOPatterns.BATTLE_SKILL_VAMPIRIC_HEALING, firstCharacter.Name, expectedHealing)));
        }

        [Test]
        public void TestExecuteBattle_FullBattle_VantageTrigger()
        {
            string firstCharacterIdName = "FirstCharacter";
            string secondCharacterIdName = "SecondCharacter";

            int damagePerAttack = 8;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_VANTAGE_ID);
            firstCharacter.ID = firstCharacterIdName;
            firstCharacter.Name = firstCharacterIdName;

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_VANTAGE_ID);
            secondCharacter.ID = secondCharacterIdName;
            secondCharacter.Name = secondCharacterIdName;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(1);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteBattle(io);

            //Check if the battle is won by the correct character
            Assert.AreEqual(secondCharacterIdName, actual: battle.GetWinningCharacter().ID);
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, firstCharacterIdName, secondCharacterIdName)));
            //Check remaining HP
            Assert.IsTrue(secondCharacter.CurrentHP == BATTLE_TESTER_START_HP - damagePerAttack * 2);
            Assert.IsTrue(firstCharacter.CurrentHP == 0);
            //Check for Vantage trigger
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_SKILL_ACTIVATION, secondCharacter, SkillCollection.SkillDictionary[SkillKey.VANTAGE].Name)));
            //Check combat rounds:  Round 1-3 were played out, 4-5 did not happen
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 1)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 2)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 3)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 4)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 5)));
            //Check: No crits, no misses
            Assert.IsFalse(io.DoesIOObjectContainString(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, secondCharacterIdName)));
        }

        [Test]
        public void TestExecuteBattle_FullBattle_Timeout()
        {
            string firstCharacterIdName = "FirstCharacter";
            string secondCharacterIdName = "SecondCharacter";

            int firstCharacterHPExpanded = 45;
            int secondCharacterHPExpanded = 50;

            double firstCharacterHPPercenRemaining = 11.11;
            double secondCharacterHPPercenRemaining = 20;

            int damagePerAttack = 8;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            firstCharacter.ID = firstCharacterIdName;
            firstCharacter.Name = firstCharacterIdName;
            firstCharacter.Attributes[(int)bakufe_core.entities.enums.Attribute.HP] = firstCharacterHPExpanded;
            firstCharacter.CurrentHP = firstCharacterHPExpanded;

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            secondCharacter.ID = secondCharacterIdName;
            secondCharacter.Name = secondCharacterIdName;
            secondCharacter.Attributes[(int)bakufe_core.entities.enums.Attribute.HP] = secondCharacterHPExpanded;
            secondCharacter.CurrentHP = secondCharacterHPExpanded;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(1);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteBattle(io);

            //Check if the battle is won by the correct character
            Assert.AreEqual(secondCharacterIdName, actual: battle.GetWinningCharacter().ID);
            //Check that no character was defeated
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, secondCharacterIdName, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, firstCharacterIdName, firstCharacterIdName)));
            //Check for timeout message
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_TIMEOUT, secondCharacterHPPercenRemaining, secondCharacterIdName, firstCharacterIdName, firstCharacterHPPercenRemaining)));
            //Check remaining HP
            Assert.IsTrue(firstCharacter.CurrentHP == firstCharacterHPExpanded - damagePerAttack * 5);
            Assert.IsTrue(secondCharacter.CurrentHP == secondCharacterHPExpanded - damagePerAttack * 5);
            //Check combat rounds:  Round 1-3 were played out, 4-5 did not happen
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 1)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 2)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 3)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 4)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 5)));
            //Check: No crits, no misses
            Assert.IsFalse(io.DoesIOObjectContainString(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, secondCharacterIdName)));
        }

        [Test]
        public void TestExecuteBattle_FullBattle_Timeout_TrueTie()
        {
            string firstCharacterIdName = "FirstCharacter";
            string secondCharacterIdName = "SecondCharacter";

            int characterHPExpanded = 50;

            double characterHPPercenRemaining = 20;

            int damagePerAttack = 8;

            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character firstCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            firstCharacter.ID = firstCharacterIdName;
            firstCharacter.Name = firstCharacterIdName;
            firstCharacter.Attributes[(int)bakufe_core.entities.enums.Attribute.HP] = characterHPExpanded;
            firstCharacter.CurrentHP = characterHPExpanded;

            Character secondCharacter = persistenceTextfile.LoadCharacter(BATTLE_TESTER_ID);
            secondCharacter.ID = secondCharacterIdName;
            secondCharacter.Name = secondCharacterIdName;
            secondCharacter.Attributes[(int)bakufe_core.entities.enums.Attribute.HP] = characterHPExpanded;
            secondCharacter.CurrentHP = characterHPExpanded;

            PredictableBakuFERandom riggedRandom = new PredictableBakuFERandom();
            riggedRandom.SequenceValues.Add(95);
            riggedRandom.SequenceValues.Add(85);
            riggedRandom.SequenceValues.Add(1);

            StateTestInputOutput io = new StateTestInputOutput();

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, riggedRandom);
            battle.ExecuteBattle(io);

            //Check if the battle is won by the correct character
            Assert.AreEqual(firstCharacterIdName, battle.GetWinningCharacter().ID);
            //Check that no character was defeated
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, secondCharacterIdName, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, firstCharacterIdName, firstCharacterIdName)));
            //Check for timeout message
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_TIMEOUT_TRUE_TIE, characterHPPercenRemaining, firstCharacterIdName)));
            //Check remaining HP
            Assert.IsTrue(firstCharacter.CurrentHP == characterHPExpanded - damagePerAttack * 5);
            Assert.IsTrue(secondCharacter.CurrentHP == characterHPExpanded - damagePerAttack * 5);
            //Check combat rounds:  Round 1-3 were played out, 4-5 did not happen
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 1)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 2)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 3)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 4)));
            Assert.IsTrue(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ROUND_START, 5)));
            //Check: No crits, no misses
            Assert.IsFalse(io.DoesIOObjectContainString(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, firstCharacterIdName)));
            Assert.IsFalse(io.DoesIOObjectContainString(String.Format(IOPatterns.BATTLE_ATTACK_MISS, secondCharacterIdName)));
        }
    }
}
