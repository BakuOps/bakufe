﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io.provider
{
    public interface IInputOutputProvider
    {
        public IInputOutput GetStandardInputOutput();
    }
}
