﻿using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.util
{
    public class ExperienceUtil
    {
        public static readonly int NO_VALID_LEVEL_OUTPUT = -1;
        public static readonly int EXP_MAXED_OUT = -2;
        public static readonly int MIN_LEVEL = 1;
        public static readonly int MAX_LEVEL = 50;

        /// <summary>
        /// Returns the requiered amount of EXP needed to advance from the current level to the next level.
        /// </summary>
        /// <param name="currentLevel">The character's current level</param>
        /// <returns>the requiered amount of EXP needed to advance from the current level to the next level</returns>
        public static int GetXPRequiredForNextLevelUp(int currentLevel)
        {
            if(currentLevel < MIN_LEVEL)
            {
                return NO_VALID_LEVEL_OUTPUT;
            }
            else if(currentLevel >= MAX_LEVEL)
            {
                return EXP_MAXED_OUT;
            }

            double level1To21Multiplier = 1.1;
            double level21To50Multiplier = 1.05;
            double BaseExpRequired = 100;

            int post22Levels = 0;
            int pre21Levels;
            if (currentLevel > 21)
            {
                pre21Levels = 21;
                post22Levels = currentLevel - 21;
            }
            else
            {
                pre21Levels = currentLevel;
            }

            double requiredExp = BaseExpRequired * Math.Pow(level1To21Multiplier, pre21Levels);
            if(post22Levels > 0)
            {
                requiredExp *= Math.Pow(level21To50Multiplier, post22Levels);
            }

            return (int)requiredExp;
        }

        /// <summary>
        /// Returns the amount of experience needed to master the given class.
        /// </summary>
        /// <param name="cClass">The given class</param>
        /// <returns>the amount of experience needed to master the given class</returns>
        public static int GetExperienceNeedToMasterClass(CharacterClass cClass)
        {
            return cClass.Level switch
            {
                CharacterClassLevel.BEGINNER => 60,
                CharacterClassLevel.INTERMEDIATE => 100,
                CharacterClassLevel.ADVANCED => 150,
                CharacterClassLevel.MASTER => 200,
                _ => 200
            };
        }

        /// <summary>
        /// Returns the amount of experiennce needed to advance to the next weapon rank
        /// Taken from here: https://fe3h.com/skills
        /// and modified to adapt it to the game for now
        /// </summary>
        /// <param name="rank">The rank in question</param>
        /// <returns>the amount of experiennce needed to advance to the next weapon rank</returns>
        public static int GetExperienceToIncreaseWeaponRank(WeaponRank rank)
        {
            return rank switch
            {
                WeaponRank.E => 40,
                WeaponRank.D => 60,
                WeaponRank.C => 80,
                WeaponRank.B => 120,
                WeaponRank.A => 160,
                WeaponRank.S => 200,
                _ => 200
            };
        }

        public static WeaponRank GetFollowUpWeaponRank(WeaponRank rank)
        {
            return rank switch
            {
                WeaponRank.E => WeaponRank.D,
                WeaponRank.D => WeaponRank.C,
                WeaponRank.C => WeaponRank.B,
                WeaponRank.B => WeaponRank.A,
                WeaponRank.A => WeaponRank.S,
                WeaponRank.S => WeaponRank.S,
                _ => WeaponRank.E
            };
        }

        private ExperienceUtil()
        {

        }
    }
}
