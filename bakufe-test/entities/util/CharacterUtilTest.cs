﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_test.entities.util
{
    [TestFixture]
    public class CharacterUtilTest
    {
        [Test]
        public void CalculateAttackSpeed_TestWithDiefferentWeapons()
        {
            int[] beginnerBaseAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = beginnerBaseAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE)));
            character.WeaponRanks[(int)WeaponType.SWORD] = new WeaponHandling(WeaponType.SWORD, WeaponRank.D);

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon steelSword = WeaponCollection.GetWeaponByKey(WeaponKey.STEEL_SWORD);

            character.Inventory[0] = ironSword;
            character.Inventory[1] = steelSword;

            //Equip the iron sword
            InventoryUtil.EquipOrUnequipItem(character, 0);

            // Expected Attack speed
            //      = SPD (6) - ( Weapon Weight (5) + Accessory Weight (0) - (SPD (5)/ 5))
            //      = 6 - (5 + 0 - 1) = 2
            Assert.AreEqual(2, CharacterUtil.CalculateAttackSpeed(character));

            //Equip the steel sword
            InventoryUtil.EquipOrUnequipItem(character, 1);

            // Expected Attack speed
            //      = SPD (6) - ( Weapon Weight (10) + Accessory Weight (0) - (SPD (5)/ 5))
            //      = 6 - (10 + 0 - 1) = -3
            Assert.AreEqual(-3, CharacterUtil.CalculateAttackSpeed(character));
        }

        [Test]
        public void CalculateHitValue_TestCalculationWeaponAccessoryMagicAndPhysical()
        {
            int[] beginnerBaseAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = beginnerBaseAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            Accessory accuracyRing = AccessoryCollection.GetAccessoryByKey(AccessoryKey.ACCURACY_RING);

            character.Inventory[0] = ironSword;
            character.Inventory[1] = fireTome;
            character.Inventory[2] = accuracyRing;

            //Equip the sword
            InventoryUtil.EquipOrUnequipItem(character, 0);

            // Expected Hitvalue = Iron Sword Hit (90) + Accessory Hit (0) + Dex (6) = 96
            Assert.AreEqual(96, CharacterUtil.CalculateHitValue(character));

            //Equip Accuracy Ring
            InventoryUtil.EquipOrUnequipItem(character, 2);

            // Expected Hitvalue = Iron Sword Hit (90) + Accessory Hit (10) + Dex (6) = 106
            Assert.AreEqual(106, CharacterUtil.CalculateHitValue(character));

            //Switch over to the fire tome
            InventoryUtil.EquipOrUnequipItem(character, 2);

            // Expected Hitvalue = Iron Sword Hit (90) + Accessory Hit (10) + (Dex (6) + Lck (4)) / 2 = 105
            Assert.AreEqual(105, CharacterUtil.CalculateHitValue(character));
        }

        [Test]
        public void CalculateAvoidValue_TestPhysicalAndMagicalAvoid()
        {
            int[] beginnerBaseAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = beginnerBaseAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            Accessory accuracyRing = AccessoryCollection.GetAccessoryByKey(AccessoryKey.ACCURACY_RING);

            character.Inventory[0] = ironSword;

            //Equip the sword
            InventoryUtil.EquipOrUnequipItem(character, 0);

            //Expected physical avoid value = Attack speed (2) + Accessory bonus (0) = 2
            Assert.AreEqual(2, CharacterUtil.CalculateAvoidValue(character, WeaponDamageType.PHYSICAL));

            //Expected magical avoid value = (SPD (6) + LCK (4)) / 2 + Accessory Bonus (0) = 5
            Assert.AreEqual(5, CharacterUtil.CalculateAvoidValue(character, WeaponDamageType.MAGICAL));
        }

        [Test]
        public void CalculateAttackValue_TestPhysicalAndMagicalDamage()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);

            character.Inventory[0] = ironSword;
            character.Inventory[1] = fireTome;

            //Equip the sword
            InventoryUtil.EquipOrUnequipItem(character, 0);

            //Expected attack value = Weapon might (5) + STR (4) = 9
            Assert.AreEqual(9, CharacterUtil.CalculateAttackValue(character, Movementtype.INFANTRY));

            //Equip the fire tome
            InventoryUtil.EquipOrUnequipItem(character, 1);

            //Expected attack value = Weapon might (5) + MAG (8) = 13
            Assert.AreEqual(13, CharacterUtil.CalculateAttackValue(character, Movementtype.INFANTRY));
        }

        [Test]
        public void CalculateAttackValue_TestUnarmedCombat()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            //The character has no weapon equipped -> They use their fists
            //Expected attack value = Weapon might (0) + STR (4) = 4
            Assert.AreEqual(4, CharacterUtil.CalculateAttackValue(character, Movementtype.INFANTRY));
        }

        [Test]
        public void CalculateDefenseValue_TestDefencesAgainstPhysicalAndMagicalDamage()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            Weapon runiceBlade = WeaponCollection.GetWeaponByKey(WeaponKey.RUNIC_BLADE);

            //Expected defense value = DEF (3)
            Assert.AreEqual(3, CharacterUtil.CalculateDefenseValue(character, ironSword));

            //Expected defense value = RES (4 + 1 Class bonus)
            Assert.AreEqual(5, CharacterUtil.CalculateDefenseValue(character, fireTome));

            //Expected value = the lower of defense and ressilliance = DEF (3)
            Assert.AreEqual(3, CharacterUtil.CalculateDefenseValue(character, runiceBlade));
        }

        [Test]
        public void CalculateCritValue_TestCalculation()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            Weapon rapier = WeaponCollection.GetWeaponByKey(WeaponKey.RAPIER_PLUS);

            character.Inventory[0] = rapier;

            //Equip the rapier
            InventoryUtil.EquipOrUnequipItem(character, 0);

            //Expected crit value = Weapon (10) + (DEX (6) + LCK (4)) / 2 = 15
            Assert.AreEqual(25, CharacterUtil.CalculateCritValue(character));

            //Equip the fire tome
            InventoryUtil.EquipOrUnequipItem(character, 0);

            //Expected crit value = Weapon (0) + (DEX (6) + LCK (4)) / 2 = 5
            Assert.AreEqual(5, CharacterUtil.CalculateCritValue(character));
        }

        [Test]
        public void CalculateCritAvoidValue_TestCalculation()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            //Expected crit avoid value = LCK = 4
            Assert.AreEqual(4, CharacterUtil.CalculateCritAvoidValue(character));
        }
    }
}
