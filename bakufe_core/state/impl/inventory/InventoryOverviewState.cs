﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.inventory
{
    /// <summary>
    /// Lists of the items in the characters inventory and allows access to the detail views
    /// </summary>
    public class InventoryOverviewState : NumericInputState
    {
        /// <summary>
        /// The next free inventory slot
        /// </summary>
        private readonly int nextFreeInventorySlot;

        public InventoryOverviewState(BakuFERealm realm, Character character)
            : base(realm, character, string.Format(IOPatterns.STATE_INVENTORY_NAME, character.Name))
        {
            //Sort characters inventory to prevent empty spots
            InventoryUtil.SortInventory(character);

            nextFreeInventorySlot = InventoryUtil.GetNextFreeInventorySlot(character);

            MinInputValue = 0;
            //One extra cause we need an option to get back
            MaxInputValue = InventoryUtil.MAX_INVENTORY_SLOTS;

            String inventoryString = InventoryUtil.GetInventoryString(character);
            //Add the text for the close option
            inventoryString += string.Format(IOPatterns.STATE_INVENTORY_CLOSE_OPTION, InventoryUtil.MAX_INVENTORY_SLOTS);

            Description = string.Format(IOPatterns.STATE_INVENTORY_DESCRIPTION, character.Gold, inventoryString);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if(InputValue == MaxInputValue)
            {
                //Back option is called -> Back to the idle state
                io.PushOutput(IOPatterns.STATE_INVENTORY_CLOSED);
                IdleState idleState = new IdleState(realm, character);
                idleState.ChangeToThisCharacterState(io);
            } 
            else if(InputValue < nextFreeInventorySlot)
            {
                //Change into the item detail state
                InventoryDetailState detailState = new InventoryDetailState(realm, character, InputValue);
                detailState.ChangeToThisCharacterState(io);
            }
            else
            {
                //Picked an empty slot => Push the output
                io.PushOutput(string.Format(IOPatterns.STATE_INVENTORY_EMPTY_SLOT, InputValue));
            }
        }
    }
}
