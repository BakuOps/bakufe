﻿using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_core.state.impl
{
    public class CharacterDetailState : CharacterStateBase
    {
        public CharacterDetailState(BakuFERealm realm, Character character)
            : base(realm, character, string.Format(IOPatterns.STATE_CHARACTER_DETAIL_NAME, character.Name))
        {
            string attributeString = "";

            // Attribute string representation
            Attribute[] attributes = (Attribute[])Enum.GetValues(typeof(Attribute));
            for (int i = 0; i < attributes.Length; i++)
            {
                Attribute attribute = attributes[i];
                attributeString += string.Format(IOPatterns.STATE_CHARACTER_DETAIL_ATTRIBUTE_PATTERN, Enum.GetName(typeof(Attribute), attribute), character.GetAttributeValue(attribute));
            }

            AcquiredCharacterClass currentAcquiredCharacterClass = character.GetCurrentAcquiredClass();
            string otherAcquiredClasses = "";

            // Non-equipped character classes
            List<AcquiredCharacterClass> nonEquippedAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            foreach(AcquiredCharacterClass acc in character.AcquieredCharacterClasses)
            {
                if (!acc.Equals(currentAcquiredCharacterClass))
                {
                    nonEquippedAcquiredCharacterClasses.Add(acc);
                }
            }
            for(int i = 0; i < nonEquippedAcquiredCharacterClasses.Count; i++)
            {
                AcquiredCharacterClass acc = nonEquippedAcquiredCharacterClasses[i];
                otherAcquiredClasses += string.Format(IOPatterns.CHARACTER_ACQUIRED_CLASS_PATTERN, acc.Class.Name, acc.GetCurrentClassExperienceRepresentation());
                if(i < nonEquippedAcquiredCharacterClasses.Count - 1)
                {
                    otherAcquiredClasses += ", ";
                }
            }

            // Weaponrank string representation
            string weaponRankString = "";
            WeaponType[] weaponTypes = (WeaponType[])Enum.GetValues(typeof(WeaponType));
            for (int i = 0; i < weaponTypes.Length; i++)
            {
                WeaponType type = weaponTypes[i];
                WeaponHandling rank = character.WeaponRanks[(int)type];
                weaponRankString += string.Format(IOPatterns.STATE_CHARACTER_DETAIL_WEAPON_RANK_PATTERN, WeaponUtil.GetReadableNameForWeaponType(type), Enum.GetName(typeof(WeaponRank), rank.WeaponRank), rank.CurrentWeaponExperience, ExperienceUtil.GetExperienceToIncreaseWeaponRank(rank.WeaponRank));
            }


            Description = string.Format(
                IOPatterns.STATE_CHARACTER_DETAIL_DESCRIPTION, 
                character.Name, 
                character.Level, 
                character.CurrentEXP, 
                ExperienceUtil.GetXPRequiredForNextLevelUp(character.Level), 
                attributeString,
                character.GetCurrentClass().Name,
                currentAcquiredCharacterClass.GetCurrentClassExperienceRepresentation(),
                otherAcquiredClasses,
                weaponRankString
            );
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            base.ChangeToThisCharacterState(io);

            // Paste the description and fall back to the idle state
            IdleState idleState = new IdleState(realm, character);
            idleState.ChangeToThisCharacterState(io);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            // Nothing to do here since this state changes straight back into the idle state
        }
    }
}
