﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.persistence.character;
using System;
using System.Collections.Generic;

namespace bakufe_test
{
    public class TestCharacterSavefileGenerator
    {
        public static void GenerateTestCharacterSavefiles()
        {
            Console.WriteLine();
            Console.WriteLine("Generating Test characters...");

            CharacterPersistenceTextfile characterPersistence = new CharacterPersistenceTextfile("testcharacters" ,true);

            //----- BattleTester -----
            string battleTesterName = "BattleTester";
            string battleTesterId = "BattleTester";
            int battleTesterLevel = 1;
            int battleTesterEquippedWeaponSlot = 0;
            int battleTesterEquippedAccessorySlot = -1;
            int battleTesterCurrentHP = 20;
            int battleTesterCurrentExperience = 69;
            int[] battleTesterAttributes = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
            SkillKey[] battleTesterEquippedSkills = new SkillKey[] { SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
            int battleTesterCurrentClassIndex = 0;
            int battleTesterGold = 1000;

            Weapon battleTesterIronSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            Weapon battleTesterNosferatuTome = WeaponCollection.GetWeaponByKey(WeaponKey.NOSFERATU);
            battleTesterIronSword.RemainingUses = 25;
            ItemBase[] battleTesterInventory = new ItemBase[] { battleTesterIronSword, battleTesterNosferatuTome, null, null, null, null, null, null };

            List<SkillKey> battleTesterLearnedSkills = new List<SkillKey>();

            List<AcquiredCharacterClass> battleTesterAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            battleTesterAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false));

            WeaponHandling[] battleTesterWeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            battleTesterWeaponRanks[0] = new WeaponHandling(WeaponType.SWORD);
            battleTesterWeaponRanks[1] = new WeaponHandling(WeaponType.LANCE);
            battleTesterWeaponRanks[2] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);
            battleTesterWeaponRanks[3] = new WeaponHandling(WeaponType.BOW);
            battleTesterWeaponRanks[4] = new WeaponHandling(WeaponType.GAUNTLET);
            battleTesterWeaponRanks[5] = new WeaponHandling(WeaponType.ANIMA);
            battleTesterWeaponRanks[6] = new WeaponHandling(WeaponType.DARK);
            battleTesterWeaponRanks[7] = new WeaponHandling(WeaponType.LIGHT);

            Character battleTester = new Character(
                battleTesterName,
                battleTesterId,
                battleTesterLevel,
                battleTesterAttributes,
                battleTesterCurrentClassIndex,
                battleTesterWeaponRanks,
                battleTesterEquippedSkills,
                battleTesterAcquiredCharacterClasses,
                battleTesterCurrentHP,
                battleTesterCurrentExperience,
                battleTesterEquippedWeaponSlot,
                battleTesterEquippedAccessorySlot,
                battleTesterGold
            );
            battleTester.LearnedSkills.AddRange(battleTesterLearnedSkills);
            battleTester.Inventory = battleTesterInventory;

            //----- BattleTesterVantage -----
            string battleTesterVantageId = "BattleTesterVantage";
            SkillKey[] battleTesterVantageEquippedSkills = new SkillKey[] { SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };

            Character battleTesterVantage = new Character(
                battleTesterName,
                battleTesterVantageId,
                battleTesterLevel,
                battleTesterAttributes,
                battleTesterCurrentClassIndex,
                battleTesterWeaponRanks,
                battleTesterVantageEquippedSkills,
                battleTesterAcquiredCharacterClasses,
                battleTesterCurrentHP,
                battleTesterCurrentExperience,
                battleTesterEquippedWeaponSlot,
                battleTesterEquippedAccessorySlot,
                battleTesterGold
            );
            battleTesterVantage.LearnedSkills.AddRange(battleTesterLearnedSkills);
            battleTesterVantage.Inventory = battleTesterInventory;

            //----- Experience -----
            string experienceName = "ExperienceTester";
            string experienceId = "Experience";
            int experienceLevel = 1;
            int experienceEquippedWeaponSlot = 0;
            int experienceEquippedAccessorySlot = -1;
            int experienceCurrentHP = 15;
            int experienceCurrentExperience = 69;
            int[] experienceAttributes = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
            SkillKey[] experienceEquippedSkills = new SkillKey[] { SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
            int experienceCurrentClassIndex = 0;
            int experienceGold = 123;

            Weapon experienceIronSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            experienceIronSword.RemainingUses = 27;
            ItemBase[] experienceInventory = new ItemBase[] { experienceIronSword, null, null, null, null, null, null, null };

            List<SkillKey> experienceLearnedSkills = new List<SkillKey>();
            experienceLearnedSkills.Add(SkillKey.MAG_PLUS);
            experienceLearnedSkills.Add(SkillKey.STR_PLUS);

            List<AcquiredCharacterClass> experienceAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            experienceAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false));

            WeaponHandling[] experienceWeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            experienceWeaponRanks[0] = new WeaponHandling(WeaponType.SWORD);
            experienceWeaponRanks[1] = new WeaponHandling(WeaponType.LANCE);
            experienceWeaponRanks[2] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);
            experienceWeaponRanks[3] = new WeaponHandling(WeaponType.BOW);
            experienceWeaponRanks[4] = new WeaponHandling(WeaponType.GAUNTLET);
            experienceWeaponRanks[5] = new WeaponHandling(WeaponType.ANIMA);
            experienceWeaponRanks[6] = new WeaponHandling(WeaponType.DARK);
            experienceWeaponRanks[7] = new WeaponHandling(WeaponType.LIGHT);

            Character experience = new Character(
                experienceName,
                experienceId,
                experienceLevel,
                experienceAttributes,
                experienceCurrentClassIndex,
                experienceWeaponRanks,
                experienceEquippedSkills,
                experienceAcquiredCharacterClasses,
                experienceCurrentHP,
                experienceCurrentExperience,
                experienceEquippedWeaponSlot,
                experienceEquippedAccessorySlot,
                experienceGold
            );
            experience.LearnedSkills.AddRange(experienceLearnedSkills);
            experience.Inventory = experienceInventory;

            //----- Inventory -----
            string inventoryName = "InventoryTester";
            string inventoryId = "Inventory";
            int inventoryLevel = 1;
            int inventoryEquippedWeaponSlot = 0;
            int inventoryEquippedAccessorySlot = -1;
            int inventoryCurrentHP = 15;
            int inventoryCurrentExperience = 69;
            int[] inventoryAttributes = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
            SkillKey[] inventoryEquippedSkills = new SkillKey[] { SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
            int inventoryCurrentClassIndex = 0;
            int inventoryGold = 123;

            Weapon inventoryIronSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            inventoryIronSword.RemainingUses = 27;
            GenericItem masterSeals = GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL);
            masterSeals.CurrentStackSize = 3;
            ItemBase[] inventoryInventory = new ItemBase[] { inventoryIronSword, masterSeals, AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING), null, null, null, null, null };

            List<SkillKey> inventoryLearnedSkills = new List<SkillKey>();
            inventoryLearnedSkills.Add(SkillKey.MAG_PLUS);
            inventoryLearnedSkills.Add(SkillKey.STR_PLUS);

            List<AcquiredCharacterClass> inventoryAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            inventoryAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false));

            WeaponHandling[] inventoryWeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            inventoryWeaponRanks[0] = new WeaponHandling(WeaponType.SWORD);
            inventoryWeaponRanks[1] = new WeaponHandling(WeaponType.LANCE);
            inventoryWeaponRanks[2] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);
            inventoryWeaponRanks[3] = new WeaponHandling(WeaponType.BOW);
            inventoryWeaponRanks[4] = new WeaponHandling(WeaponType.GAUNTLET);
            inventoryWeaponRanks[5] = new WeaponHandling(WeaponType.ANIMA);
            inventoryWeaponRanks[6] = new WeaponHandling(WeaponType.DARK);
            inventoryWeaponRanks[7] = new WeaponHandling(WeaponType.LIGHT);

            Character inventory = new Character(
                inventoryName,
                inventoryId,
                inventoryLevel,
                inventoryAttributes,
                inventoryCurrentClassIndex,
                inventoryWeaponRanks,
                inventoryEquippedSkills,
                inventoryAcquiredCharacterClasses,
                inventoryCurrentHP,
                inventoryCurrentExperience,
                inventoryEquippedWeaponSlot,
                inventoryEquippedAccessorySlot,
                inventoryGold
            );
            inventory.LearnedSkills.AddRange(inventoryLearnedSkills);
            inventory.Inventory = inventoryInventory;

            //----- Character Class -----
            string className = "ClassTester";
            string classId = "Class";
            int classLevel = 12;
            int classEquippedWeaponSlot = 0;
            int classEquippedAccessorySlot = -1;
            int classCurrentHP = 15;
            int classCurrentExperience = 69;
            int[] classAttributes = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
            SkillKey[] classEquippedSkills = new SkillKey[] { SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
            int classCurrentClassIndex = 0;
            int classGold = 123;

            Weapon classIronSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            classIronSword.RemainingUses = 27;
            GenericItem classIntermediateSeals = GenericItemCollection.GetGenericItemByKey(GenericItemKey.INTERMEDIATE_SEAL);
            classIntermediateSeals.CurrentStackSize = 1;
            GenericItem classBeginnerSeals = GenericItemCollection.GetGenericItemByKey(GenericItemKey.BEGINNER_SEAL);
            classBeginnerSeals.CurrentStackSize = 1;
            ItemBase[] classInventory = new ItemBase[] { inventoryIronSword, classIntermediateSeals, AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING), classBeginnerSeals, null, null, null, null };

            List<SkillKey> classLearnedSkills = new List<SkillKey>();
            classLearnedSkills.Add(SkillKey.MAG_PLUS);
            classLearnedSkills.Add(SkillKey.STR_PLUS);

            List<AcquiredCharacterClass> classAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            classAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false));
            classAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER), 60, true));

            WeaponHandling[] classWeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            classWeaponRanks[0] = new WeaponHandling(WeaponType.SWORD);
            classWeaponRanks[1] = new WeaponHandling(WeaponType.LANCE);
            classWeaponRanks[2] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);
            classWeaponRanks[3] = new WeaponHandling(WeaponType.BOW);
            classWeaponRanks[4] = new WeaponHandling(WeaponType.GAUNTLET, WeaponRank.D);
            classWeaponRanks[5] = new WeaponHandling(WeaponType.ANIMA);
            classWeaponRanks[6] = new WeaponHandling(WeaponType.DARK);
            classWeaponRanks[7] = new WeaponHandling(WeaponType.LIGHT);

            Character classTester = new Character(
                className,
                classId,
                classLevel,
                classAttributes,
                classCurrentClassIndex,
                classWeaponRanks,
                classEquippedSkills,
                classAcquiredCharacterClasses,
                classCurrentHP,
                classCurrentExperience,
                classEquippedWeaponSlot,
                classEquippedAccessorySlot,
                classGold
            );
            classTester.LearnedSkills.AddRange(classLearnedSkills);
            classTester.Inventory = classInventory;

            //----- Tester -----
            string testerName = "Tester";
            string testerId = "TesterID";
            int testerLevel = 3;
            int testerEquippedWeaponSlot = 0;
            int testerEquippedAccessorySlot = -1;
            int testerCurrentHP = 15;
            int testerCurrentExperience = 69;
            int[] testerAttributes = new int[] { 20, 8, 3, 6, 6, 4, 5, 2 };
            SkillKey[] testerEquippedSkills = new SkillKey[] { SkillKey.VANTAGE, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY, SkillKey.DUMMY };
            int testerCurrentClassIndex = 0;
            int testerGold = 123;

            Weapon testerIronSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            testerIronSword.RemainingUses = 27;
            ItemBase[] testerInventory = new ItemBase[] { testerIronSword, null, null, AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING), null, null, null, null };

            List<SkillKey> testerLearnedSkills = new List<SkillKey>();
            testerLearnedSkills.Add(SkillKey.MAG_PLUS);
            testerLearnedSkills.Add(SkillKey.STR_PLUS);

            List<AcquiredCharacterClass> testerAcquiredCharacterClasses = new List<AcquiredCharacterClass>();
            testerAcquiredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER), 45, false));

            WeaponHandling[] testerWeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            testerWeaponRanks[0] = new WeaponHandling(WeaponType.SWORD);
            testerWeaponRanks[1] = new WeaponHandling(WeaponType.LANCE);
            testerWeaponRanks[2] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);
            testerWeaponRanks[3] = new WeaponHandling(WeaponType.BOW);
            testerWeaponRanks[4] = new WeaponHandling(WeaponType.GAUNTLET);
            testerWeaponRanks[5] = new WeaponHandling(WeaponType.ANIMA);
            testerWeaponRanks[6] = new WeaponHandling(WeaponType.DARK);
            testerWeaponRanks[7] = new WeaponHandling(WeaponType.LIGHT);

            Character tester = new Character(
                testerName,
                testerId,
                testerLevel,
                testerAttributes,
                testerCurrentClassIndex,
                testerWeaponRanks,
                testerEquippedSkills,
                testerAcquiredCharacterClasses,
                testerCurrentHP,
                testerCurrentExperience,
                testerEquippedWeaponSlot,
                testerEquippedAccessorySlot,
                testerGold
            );
            tester.LearnedSkills.AddRange(testerLearnedSkills);
            tester.Inventory = testerInventory;

            //Persist characters
            Console.WriteLine("Persisting: " + battleTester.ID + "...");
            characterPersistence.PersistCharacter(battleTester.ID, battleTester);
            Console.WriteLine("Persisting: " + battleTesterVantage.ID + "...");
            characterPersistence.PersistCharacter(battleTesterVantage.ID, battleTesterVantage);
            Console.WriteLine("Persisting: " + experience.ID + "...");
            characterPersistence.PersistCharacter(experience.ID, experience);
            Console.WriteLine("Persisting: " + inventory.ID + "...");
            characterPersistence.PersistCharacter(inventory.ID, inventory);
            Console.WriteLine("Persisting: " + classTester.ID + "...");
            characterPersistence.PersistCharacter(classTester.ID, classTester);
            Console.WriteLine("Persisting: " + tester.ID + "...");
            characterPersistence.PersistCharacter(tester.ID, tester);
            Console.WriteLine();
        }
    }
}
