﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.church
{
    /// <summary>
    /// Confirmation state where the character's wounds can be treated 
    /// </summary>
    public class HospitalState : ConfirmationState
    {
        private static readonly int MISSING_HP_TO_GOLD_FACTOR = -5;

        private bool needHealing;
        private int price;
        private CharacterStateBase previousState;

        public HospitalState(BakuFERealm realm, Character character, CharacterStateBase pState)
            : base(realm, character, IOPatterns.STATE_HOSPITAL_NAME)
        {
            previousState = pState;
            needHealing = character.CurrentHP != character.GetAttributeValue(entities.enums.Attribute.HP);
            price = (character.CurrentHP - character.GetAttributeValue(entities.enums.Attribute.HP)) * MISSING_HP_TO_GOLD_FACTOR;

            Description = string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION, character.Name);
            if (needHealing)
            {
                Description += string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION_NEED_HEALING, character.Name, price);
                if (price > character.Gold)
                {
                    price = character.Gold;
                    Description += string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION_NEED_HEALING_NOT_ENOUGH_FUNDS, price);
                }
            }
            else
            {
                Description += string.Format(IOPatterns.STATE_HOSPITAL_DESCRIPTION_FULL_HEALTH, character.Name);
            }
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (!needHealing)
            {
                io.PushOutput(Description);
                GetFollowupState().ChangeToThisCharacterState(io);
            }
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        private CharacterStateBase GetFollowupState()
        {
            CharacterStateBase followupState;
            if (previousState.Name == IOPatterns.STATE_CHURCH_NAME)
            {
                followupState = new ChurchState(realm, character);
            }
            else
            {
                followupState = new IdleState(realm, character);
            }
            return followupState;
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            io.PushOutput(string.Format(IOPatterns.STATE_HOSPITAL_TREATMENT_DECLINED, character.Name));
            ChangeCharacterToState(io, GetFollowupState());
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            io.PushOutput(string.Format(IOPatterns.STATE_HOSPITAL_TREATMENT, character.Name));

            character.Gold -= price;
            character.CurrentHP = character.GetAttributeValue(entities.enums.Attribute.HP);
            realm.AutosaveCharacterIfActivated(character);
            
            ChangeCharacterToState(io, GetFollowupState());
        }
    }
}
