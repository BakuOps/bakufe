﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state
{
    /// <summary>
    /// Template for a CharacterState that handles a numeric (interger) value input instead of a string command
    /// </summary>
    public abstract class NumericInputState : CharacterStateBase
    {
        /// <summary>
        /// Default value in case none is set yet
        /// </summary>
        protected static readonly int NO_VALUE_SET_YET = -1000;

        /// <summary>
        /// Stores the last input value
        /// </summary>
        protected int InputValue = NO_VALUE_SET_YET;

        /// <summary>
        /// The minimum accepted input value in case it is set ( != NO_VALUE_SET_YET)
        /// </summary>
        protected int MinInputValue = NO_VALUE_SET_YET;
        /// <summary>
        /// The maximum accepted input value in case it is set ( != NO_VALUE_SET_YET)
        /// </summary>
        protected int MaxInputValue = NO_VALUE_SET_YET;

        public NumericInputState(BakuFERealm realm, Character character, string name)
            : base(realm, character, name)
        {
        }

        public NumericInputState(BakuFERealm realm, Character character, string name, string description)
            : base(realm, character, name, description)
        {
        }


        public override void HandleInput(IInputOutput io)
        {
            //Clear the input command to prevent the last command from messing up the current cycle
            InputValue = NO_VALUE_SET_YET;

            string input = io.GetInput().ToLower();

            //Check if the state is canceled
            if (input == StateInputCommand.CANCEL.ToString())
            {
                isCanceled = true;
                io.PushOutput(STATE_EXITED);
                ClearCurrentContext(io);
            }
            else
            {
                //Extract the command from the user input
                //First check if the input is allowed
                if (!IsInputAllowed(input))
                {
                    //if not, push an error message to the output
                    io.PushOutput(String.Format(IOPatterns.STATE_INPUT_ILLEGAL, input, GetInputOptions()));
                }
                else
                {
                    //If the input is allowed extract the numeric value
                    InputValue = int.Parse(input);
                    
                    //If the minimumValue is set, validate the input
                    if(MinInputValue != NO_VALUE_SET_YET && InputValue < MinInputValue)
                    {
                        io.PushOutput(String.Format(IOPatterns.STATE_INPUT_TO_LOW, InputValue, MinInputValue));
                        return;
                    }

                    //If the maximum is set, validate the input
                    if (MaxInputValue != NO_VALUE_SET_YET && MaxInputValue < InputValue)
                    {
                        io.PushOutput(String.Format(IOPatterns.STATE_INPUT_TO_HIGH, InputValue, MaxInputValue));
                        return;
                    }

                    //Fianlly handle the input
                    HandleInputValidCommand(io);
                }
            }
        }

        /// <summary>
        /// Checks if the input is valid.
        /// In this iteratio this method checks if that input can be converted to a proper integer value
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected override bool IsInputAllowed(string input)
        {
            try
            {
                int potentialInput = int.Parse(input);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
