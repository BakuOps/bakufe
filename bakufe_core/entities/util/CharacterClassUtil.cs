﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.util
{
    public class CharacterClassUtil
    {
        public static readonly int LEVEL_REQUIRED_FOR_BEGINNER_CLASS = 0;
        public static readonly int LEVEL_REQUIRED_FOR_INTERMEDIATE_CLASS = 10;
        public static readonly int LEVEL_REQUIRED_FOR_ADVANCED_CLASS = 20;
        public static readonly int LEVEL_REQUIRED_FOR_MASTER_CLASS = 30;

        public static int GetRequieredExperienceToMaster(CharacterClassLevel level)
        {
            int result = 0;
            switch (level)
            {
                case CharacterClassLevel.BEGINNER:
                    result = 50;
                    break;
                case CharacterClassLevel.INTERMEDIATE:
                    result = 100;
                    break;
                case CharacterClassLevel.ADVANCED:
                    result = 150;
                    break;
                case CharacterClassLevel.MASTER:
                    result = 200;
                    break;
            }
            return result;
        }

        public static int GetRequiredLevelToChangeToClass(CharacterClassLevel level)
        {
            int result = 0;
            switch (level)
            {
                case CharacterClassLevel.BEGINNER:
                    result = LEVEL_REQUIRED_FOR_BEGINNER_CLASS;
                    break;
                case CharacterClassLevel.INTERMEDIATE:
                    result = LEVEL_REQUIRED_FOR_INTERMEDIATE_CLASS;
                    break;
                case CharacterClassLevel.ADVANCED:
                    result = LEVEL_REQUIRED_FOR_ADVANCED_CLASS;
                    break;
                case CharacterClassLevel.MASTER:
                    result = LEVEL_REQUIRED_FOR_MASTER_CLASS;
                    break;
                default:
                    break;
            }
            return result;
        }

        public static List<CharacterClassLevel> GetCharacterClassLevelsAvailableAtLevel(int characterLevel)
        {
            List<CharacterClassLevel> result = new List<CharacterClassLevel>();

            result.Add(CharacterClassLevel.BEGINNER);
            if(characterLevel >= LEVEL_REQUIRED_FOR_INTERMEDIATE_CLASS)
            {
                result.Add(CharacterClassLevel.INTERMEDIATE);
            }
            if (characterLevel >= LEVEL_REQUIRED_FOR_ADVANCED_CLASS)
            {
                result.Add(CharacterClassLevel.ADVANCED);
            }
            if (characterLevel >= LEVEL_REQUIRED_FOR_MASTER_CLASS)
            {
                result.Add(CharacterClassLevel.MASTER);
            }

            return result;
        }

        /// <summary>
        /// Calculates the success chance of the given character to succeed in aquiring this class
        /// </summary>
        /// <param name="character">The character in question</param>
        /// <param name="examClass">The class that character attempts to aquire</param>
        /// <returns></returns>
        public static int GetClassExamSuccessChance(Character character, CharacterClass examClass)
        {
            int luckAdditor = (int)Math.Ceiling((double)character.GetAttributeValue(enums.Attribute.LCK) / 2.0);
            int requirementSubtractor = 0;
            int requirementFactor = GetRequirementFactorForClass(examClass);
            foreach (ClassPromotionRequirement requirement in examClass.ClassPromotionRequirements)
            {
                WeaponRank charactersCurrentRank = character.WeaponRanks[(int)requirement.WeaponType].WeaponRank;
                WeaponRank requiredRank = requirement.WeaponRank;
                int difference = requiredRank - charactersCurrentRank;
                if (difference > 0)
                {
                    requirementSubtractor += difference * requirementFactor;
                }
            }

            return 100 - requirementSubtractor + luckAdditor;
        }

        /// <summary>
        /// Returns the requirement factor for the given class
        /// </summary>
        /// <param name="cClass">The class in question</param>
        /// <returns>the requirement factor for the given class</returns>
        private static int GetRequirementFactorForClass(CharacterClass cClass)
        {
            switch (cClass.Level)
            {
                case CharacterClassLevel.BEGINNER:
                    return 40;
                case CharacterClassLevel.INTERMEDIATE:
                    return 40;
                case CharacterClassLevel.ADVANCED:
                    if(cClass.ClassPromotionRequirements.Count > 1)
                    {
                        return 40;
                    }
                    return 32;
                case CharacterClassLevel.MASTER:
                    return 20;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Returns the list of all CharacterClasses the given character can attempt to acquire 
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns>the list of all CharacterClasses the given character can attempt to acquire </returns>
        public static List<CharacterClass> GetClassAvailableToBeAquired(Character character)
        {
            List<CharacterClass> result = new List<CharacterClass>();

            //Get all the classes that are available to the character...
            List<CharacterClassLevel> availableCharacterClassLevels = GetCharacterClassLevelsAvailableAtLevel(character.Level);
            result.AddRange(CharacterClassCollection.GetCharacterClassesForLevel(availableCharacterClassLevels));

            //...and remove the ones that already got aquired
            foreach(AcquiredCharacterClass aClass in character.AcquieredCharacterClasses)
            {
                result.Remove(aClass.Class);
            }
            result.Remove(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BASE));

            return result;
        }

        /// <summary>
        /// Returns the required seal to attempt a class promotion
        /// </summary>
        /// <param name="cClass">The class in question</param>
        /// <returns>the required seal to attempt a class promotion</returns>
        public static GenericItem GetRequiredSealForClass(CharacterClass cClass)
        {
            switch (cClass.Level)
            {
                case CharacterClassLevel.BEGINNER:
                    return GenericItemCollection.GetGenericItemByKey(GenericItemKey.BEGINNER_SEAL);
                case CharacterClassLevel.INTERMEDIATE:
                    return GenericItemCollection.GetGenericItemByKey(GenericItemKey.INTERMEDIATE_SEAL);
                case CharacterClassLevel.ADVANCED:
                    return GenericItemCollection.GetGenericItemByKey(GenericItemKey.ADVANCED_SEAL);
                case CharacterClassLevel.MASTER:
                    return GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL);
                default:
                    return GenericItemCollection.GetGenericItemByKey(GenericItemKey.MASTER_SEAL);
            }
        }
    }
}
