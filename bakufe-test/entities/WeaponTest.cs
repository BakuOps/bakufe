﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.entities
{
    [TestFixture]
    public class WeaponTest
    {
        [Test]
        public void WeaponUtil_CanCharacterEquipWeapon()
        {
            // Empty character => All weapon ranks = E
            Character character = new Character();
            //Requires weapon rank E
            Weapon ironSword = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            //Requires weapon rank D => Not equipable
            Weapon steelSword = WeaponCollection.GetWeaponByKey(WeaponKey.STEEL_SWORD);

            Assert.IsTrue(WeaponUtil.CanCharacterEquipWeapon(character, ironSword));
            Assert.IsFalse(WeaponUtil.CanCharacterEquipWeapon(character, steelSword));
        }

        [Test]
        public void WeaponUtil_DoesWeaponHaveModification()
        {
            Weapon fireTome = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            Weapon runiceBlade = WeaponCollection.GetWeaponByKey(WeaponKey.RUNIC_BLADE);

            Assert.IsFalse(WeaponUtil.DoesWeaponHaveModification(fireTome, WeaponModification.ADAPTIVE));
            Assert.IsTrue(WeaponUtil.DoesWeaponHaveModification(runiceBlade, WeaponModification.ADAPTIVE));
        }

        [Test]
        public void WeaponUtil_IsWeaponEffectiveAgainstMovementtype()
        {
            Weapon rapier = WeaponCollection.GetWeaponByKey(WeaponKey.RAPIER);

            Assert.IsFalse(WeaponUtil.IsWeaponEffectiveAgainstMovementtype(rapier, Movementtype.INFANTRY));
            Assert.IsTrue(WeaponUtil.IsWeaponEffectiveAgainstMovementtype(rapier, Movementtype.CAVALRY));
            Assert.IsFalse(WeaponUtil.IsWeaponEffectiveAgainstMovementtype(rapier, Movementtype.FLIER));
            Assert.IsTrue(WeaponUtil.IsWeaponEffectiveAgainstMovementtype(rapier, Movementtype.ARMORED));
        }
    }
}
