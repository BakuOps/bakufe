﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public enum WeaponRank
    {
        E = 0,
        D = 1,
        C = 2,
        B = 3,
        A = 4,
        S = 5
    }
}
