﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.persistence.character;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using SkillKey = bakufe_core.datasources.keys.SkillKey;

namespace bakufe_test.entities.util
{
    [TestFixture]
    public class SkillUtilTest
    {
        private static readonly string TESTER_ID = "TesterID";

        [Test]
        public void DoesCharacterHaveSkillEquipped_TestClassSkills()
        {
            CharacterPersistenceTextfile persistenceTextfile = new CharacterPersistenceTextfile(false);

            Character character = persistenceTextfile.LoadCharacter(TESTER_ID);

            //Character has only Vantage equipped so that one will be found
            Assert.IsTrue(SkillUtil.DoesCharacterHaveSkillEquipped(character, SkillKey.VANTAGE));
            Assert.IsFalse(SkillUtil.DoesCharacterHaveSkillEquipped(character, SkillKey.UNARMED_COMBAT_PLUS));

            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.BRAWLER)));
            character.CurrentClassIndex = 1;

            //Now that the character had changed to the Brawler class, they get the unarmed skill provided via the class
            Assert.IsTrue(SkillUtil.DoesCharacterHaveSkillEquipped(character, SkillKey.VANTAGE));
            Assert.IsTrue(SkillUtil.DoesCharacterHaveSkillEquipped(character, SkillKey.UNARMED_COMBAT_PLUS));
        }
    }
}
