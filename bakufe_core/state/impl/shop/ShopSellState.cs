﻿using bakufe_core.datasources;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.shop
{
    /// <summary>
    /// This state lists the wepaons the character can sell to the shop, if there are any
    /// </summary>
    public class ShopSellState : NumericInputState
    {
        readonly List<Weapon> sellableWeapons = new List<Weapon>();

        public ShopSellState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_SHOP_SELL_NAME)
        {
            sellableWeapons = InventoryUtil.GetWeaponsThatCanBeSold(character);
            if (sellableWeapons.Count == 0)
            {
                Description = IOPatterns.STATE_SHOP_SELL_NOTHING_SELLABLE;
            }
            else
            {
                //Build shop
                string shopInventory = IOPatterns.STATE_SHOP_SELL_SHOPINVENTORY_HEADER;
                MinInputValue = 0;
                MaxInputValue = sellableWeapons.Count;
                shopInventory += BuildShopInventoryString();
                shopInventory += IOPatterns.STATE_SHOP_BUY_SHOPINVENTORY_HINT;
                Description = shopInventory;
            }
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (sellableWeapons.Count == 0)
            {
                io.PushOutput(IOPatterns.STATE_SHOP_SELL_NOTHING_SELLABLE);
                ChangeCharacterToState(io, new ShopState(realm, character, this));
            }
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if(InputValue == 0)
            {
                io.PushOutput(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DECLINE);
                ShopState shopState = new ShopState(realm, character, this);
                ChangeCharacterToState(io, shopState);
            }
            else
            {
                Weapon weapon = sellableWeapons[InputValue - 1];

                ShopSellConfirmationState shopSellConfirmationState = new ShopSellConfirmationState(realm, character, weapon, this);
                ChangeCharacterToState(io, shopSellConfirmationState);
            }
        }

        private string BuildShopInventoryString()
        {
            string shopInventory = "";
            Weapon weapon;
            string numberString;
            for (int i = 0; i < sellableWeapons.Count; i++)
            {
                weapon = sellableWeapons[i];
                numberString = " " + (i + 1);
                shopInventory += string.Format(IOPatterns.STATE_SHOP_BUY_SHOPINVENTORY_LINE, numberString, weapon.Name, weapon.GetSellValue());
            }

            return shopInventory;
        }
    }
}
