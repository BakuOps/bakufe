﻿using bakufe_core.io.log;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io
{
    class CommandlineInputOutput : IInputOutput
    {
        private readonly ILogger logger = BakuFELogProvider.Logger;

        public String Input { get; set; }

        public string GetInput()
        {
            return Input;
        }

        public void PushEmptyLine()
        {
            Console.WriteLine("");
        }

        public void PushOutput(string output)
        {
            logger.Information("OUTPUT: " + output);
            Console.WriteLine(output);
        }

        public void SetInput(string input)
        {
            logger.Information("INPUT: " + input);
            Input = input;
        }
    }
}
