﻿using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// Represents the ability of a character to wield weapons of a given type.
    /// This object tracks the type of weapons, the rank with those as well as the current amount
    /// of experience collected with this type of weapons.
    /// Also, this object contains the logic for exp increases.
    /// </summary>
    public class WeaponHandling
    {
        /// <summary>
        /// The type of weapon the Handling object is referring to
        /// </summary>
        public WeaponType WeaponType { get; private set; }

        /// <summary>
        /// The weapon rank currently associated with the type of weapon type
        /// </summary>
        public WeaponRank WeaponRank { get; private set; }

        /// <summary>
        /// The current amount of weapon exp acquired for the current weapon rank
        /// </summary>
        public int CurrentWeaponExperience { get; private set; }

        public WeaponHandling(WeaponType weaponType)
        {
            WeaponType = weaponType;
            WeaponRank = WeaponRank.E;
            CurrentWeaponExperience = 0;
        }

        public WeaponHandling(WeaponType weaponType, WeaponRank weaponRank)
        {
            WeaponType = weaponType;
            WeaponRank = weaponRank;
            CurrentWeaponExperience = 0;
        }

        public WeaponHandling(WeaponType weaponType, WeaponRank weaponRank, int currentWeaponExperience)
        {
            WeaponType = weaponType;
            WeaponRank = weaponRank;
            CurrentWeaponExperience = currentWeaponExperience;
        }

        /// <summary>
        /// Adds the given amount of experience to the current experience and increases the weapon rank in case the treshold is reached.
        /// </summary>
        /// <param name="amountToAdd">the given amount of experience</param>
        public void AddExperience(int amountToAdd)
        {
            if(WeaponRank != WeaponRank.S)
            {
                int amountRequired = ExperienceUtil.GetExperienceToIncreaseWeaponRank(WeaponRank);
                CurrentWeaponExperience += amountToAdd;
                if(CurrentWeaponExperience >= amountRequired)
                {
                    CurrentWeaponExperience -= amountRequired;
                    WeaponRank = ExperienceUtil.GetFollowUpWeaponRank(WeaponRank);
                }
            }
        }

        

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is WeaponHandling handling &&
                   WeaponType == handling.WeaponType &&
                   WeaponRank == handling.WeaponRank &&
                   CurrentWeaponExperience == handling.CurrentWeaponExperience;
        }
    }
}
