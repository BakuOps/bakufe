﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_core.persistence.character;
using bakufe_core.state;
using bakufe_test.states;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test
{

    public abstract class TestWithARealm
    {
        protected static readonly string CHARACTER_TESTER_ID = "TesterID";
        protected static readonly string CHARACTER_TESTER_NAME = "Tester";
        protected static readonly int CHARACTER_TESTER_MAXHP = 20;

        protected Character currentCharacter;

        protected BakuFERealm CreateEmptyRealmAndLoadCharacter(string characterId)
        {
            return CreateEmptyRealmAndLoadCharacter(characterId, new BakuFERandom());
        }

        protected BakuFERealm CreateEmptyRealmAndLoadCharacter(string characterId, BakuFERandom bakuFERandom)
        {
            BakuFERealm realm = new BakuFERealm(new StateTestInputOutputProvider(), new CharacterPersistenceTextfile(true), bakuFERandom, false);
            currentCharacter = realm.GetCharacter(characterId);
            Assert.IsTrue(currentCharacter != null);
            return realm;
        }

        protected void FeedRealmWithInput(BakuFERealm realm, StateTestInputOutput io)
        {
            CharacterStateBase state;
            for (int i = 0; i < io.inputs.Count; i++)
            {
                state = realm.GetCharactersCurrentState(currentCharacter.ID);
                Assert.IsTrue(state != null);
                state.HandleInput(io);
            }
        }
    }
}
