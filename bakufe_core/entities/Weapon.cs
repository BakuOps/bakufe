using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    public class Weapon : ItemBase
    {
        public WeaponKey WeaponKey { get; set; }
        public WeaponType Type {get; set;}
        public WeaponRank Rank {get; set;}
        public WeaponDamageType DamageType {get; set;}
        public int[] WeaponAttirbutes { get; set; }
        public WeaponModification[] WeaponModifications { get; set; }
        public int RemainingUses { get; set; }

        #region Constructors/Shallow Copy

        /// <summary>
        /// Base constructor only needed for serialization and testing purposes, don't use this anywhere else!!!
        /// </summary>
        public Weapon() : base()
        {
            WeaponAttirbutes = new int[Enum.GetNames(typeof(WeaponAttribute)).Length];
        }

        public Weapon(string name, string description, WeaponKey weaponKey, WeaponType type, WeaponRank rank, WeaponDamageType damageType, int[] weaponAttirbutes, WeaponModification[] weaponModifications, int price) 
            : base(weaponKey.ToString(), name, description, 1, price)
        {
            WeaponKey = weaponKey;
            Type = type;
            Rank = rank;
            DamageType = damageType;
            WeaponAttirbutes = weaponAttirbutes;
            WeaponModifications = weaponModifications;
            RemainingUses = WeaponAttirbutes[(int)WeaponAttribute.MAX_USES];
        }

        public Weapon ShallowCopy()
        {
            return (Weapon)MemberwiseClone();
        }

        #endregion

        public override int GetSellValue()
        {
            int result = Price / 4;
            result += (int)((double)RemainingUses / (double)WeaponAttirbutes[(int)WeaponAttribute.MAX_USES]) * (Price / 4);
            return result;
        }

        public override string GetFullItemDescription()
        {
            String statsString = "";

            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Uses", RemainingUses + "/" + WeaponAttirbutes[(int)WeaponAttribute.MAX_USES]);
            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Might", WeaponAttirbutes[(int)WeaponAttribute.MIGHT]);
            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Hit", WeaponAttirbutes[(int)WeaponAttribute.HIT]);
            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Crit", WeaponAttirbutes[(int)WeaponAttribute.CRIT]);
            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Range", WeaponAttirbutes[(int)WeaponAttribute.RANGE_MIN] + "-" + WeaponAttirbutes[(int)WeaponAttribute.RANGE_MAX]);
            statsString += string.Format(IOPatterns.ITEM_WEAPON_ATTRIBUTE_PATTERN, "Weight", WeaponAttirbutes[(int)WeaponAttribute.WEIGHT]);

            return string.Format(IOPatterns.ITEM_WEAPON_FULL_DESCRIPTION, statsString, Description);
        }

        public override bool Equals(object obj)
        {
            return obj is Weapon weapon &&
                   WeaponKey == weapon.WeaponKey &&
                   RemainingUses == weapon.RemainingUses;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(WeaponKey, RemainingUses);
        }
    }
}
