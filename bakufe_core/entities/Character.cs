﻿using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_core.entities
{
    public class Character
    {
        #region Attributes

        public string Name { get; set; }

        public string ID { get; set; }

        public int Level { get; set; }

        public int[] Attributes { get; set; }

        public int CurrentClassIndex { get; set; }

        public WeaponHandling[] WeaponRanks { get; set; }

        public ItemBase[] Inventory { get; set; }

        public SkillKey[] EquippedSkills { get; set; }

        public readonly List<SkillKey> LearnedSkills = new List<SkillKey>();

        public readonly List<AcquiredCharacterClass> AcquieredCharacterClasses = new List<AcquiredCharacterClass>();

        private int currentHP;
        private int currentEXP;
        private int gold;

        public int CurrentHP {
            get { return currentHP; }
            set {
                currentHP = value;
                int maxHP = Attributes[(int)Attribute.HP];
                if (currentHP < 0)
                {
                    currentHP = 0;
                }
                else if (currentHP > maxHP)
                {
                    currentHP = GetAttributeValue(Attribute.HP);
                }
            } 
        }

        public int CurrentEXP
        {
            get { return currentEXP; }
            set
            {
                currentEXP = value;
            }
        }

        public int Gold
        {
            get { return gold; }
            set
            {
                gold = value;
                if(gold < 0)
                {
                    gold = 0;
                }
            }
        }

        public int EquippedWeaponSlot { get; set; }

        public int EquippedAccessorySlot { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Base constructor only needed for serialization and testing purposes, don't use this anywhere else!!!
        /// </summary>
        public Character()
        {
            InitMembers();
        }

        public Character(string name, string iD, int level, int[] attributes, int cci, WeaponHandling[] weaponRanks, SkillKey[] equippedSkills, List<AcquiredCharacterClass> acquiredCharacterClasses, int cHP, int cEXP, int equippedWeaponSlot, int equippedAccessorySlot, int cgold)
        {
            Name = name;
            ID = iD;
            Level = level;
            Attributes = attributes;
            CurrentClassIndex = cci;
            WeaponRanks = weaponRanks;
            EquippedSkills = equippedSkills;
            AcquieredCharacterClasses = acquiredCharacterClasses;
            CurrentHP = cHP;
            CurrentEXP = cEXP;
            EquippedWeaponSlot = equippedWeaponSlot;
            EquippedAccessorySlot = equippedAccessorySlot;
            Inventory = new ItemBase[InventoryUtil.MAX_INVENTORY_SLOTS];
            Gold = cgold;
        }

        private void InitMembers()
        {
            Attributes = new int[Enum.GetNames(typeof(Attribute)).Length];
            WeaponRanks = new WeaponHandling[Enum.GetNames(typeof(WeaponType)).Length];
            Inventory = new ItemBase[InventoryUtil.MAX_INVENTORY_SLOTS];
            EquippedSkills = new SkillKey[SkillUtil.MAX_EQUIPPED_SKILLS];

            foreach(WeaponType wpt in Enum.GetValues(typeof(WeaponRank)))
            {
                WeaponRanks[(int)wpt] = new WeaponHandling(wpt);
            }

            EquippedWeaponSlot = InventoryUtil.NO_SLOT_AVAILABLE;
            EquippedAccessorySlot = InventoryUtil.NO_SLOT_AVAILABLE;
        }

        #endregion

        #region Stat/Class-Getters

        /// <summary>
        /// Returns the value of the attribute fitting to the key, taking class base stats as well as class attribute boni into account
        /// </summary>
        /// <param name="key">The wanted attributes key</param>
        /// <returns>the value of the attribute fitting to the key</returns>
        private int GetBaseAttribute(enums.Attribute key)
        {
            int characterAttribute = Attributes[(int)key];
            int classAttribute = AcquieredCharacterClasses[CurrentClassIndex].Class.BaseAttributes[(int)key];
            if(classAttribute > characterAttribute)
            {
                return classAttribute;
            }
            return characterAttribute;
        }

        /// <summary>
        /// Returns the higher of the characters stat between the character's attributes and the class base attributes
        /// </summary>
        /// <param name="key">The wanted attributes key</param>
        /// <returns>the higher of the characters stat</returns>
        public int GetAttributeValue(enums.Attribute key)
        {
            int accessoryStatBoost = 0;
            if(EquippedAccessorySlot >= 0)
            {
                Accessory accessory = (Accessory)Inventory[EquippedAccessorySlot];
                accessoryStatBoost = accessory.AttributeBoni[(int)key];
            }

            int skillStatBonus = 0;
            SkillKey fittingBonusSkill = SkillUtil.GetFittingSkillForAttributeBonus(key);
            if(fittingBonusSkill != SkillKey.DUMMY && SkillUtil.DoesCharacterHaveSkillEquipped(this, fittingBonusSkill))
            {
                skillStatBonus = SkillUtil.GetAttributeBonusBySkillKey(fittingBonusSkill);
            }

            return GetBaseAttribute(key) + AcquieredCharacterClasses[CurrentClassIndex].Class.AttributeBonuses[(int)key] + accessoryStatBoost + skillStatBonus;
        }

        public AcquiredCharacterClass GetCurrentAcquiredClass()
        {
            return AcquieredCharacterClasses[CurrentClassIndex];
        }

        public CharacterClass GetCurrentClass()
        {
            return GetCurrentAcquiredClass().Class;
        }

        #endregion

        #region Serialization

        public virtual string GetJSONString()
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            return JsonSerializer.Serialize(this, options);
        }

        #endregion

    }
}
