﻿using bakufe_core.io;
using bakufe_core.io.provider;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.states
{
    public class StateTestInputOutputProvider : IInputOutputProvider
    {
        public IInputOutput GetStandardInputOutput()
        {
            return new StateTestInputOutput();
        }
    }
}
