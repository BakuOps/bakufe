﻿using bakufe_core.datasources.keys;
using bakufe_core.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.datasources
{
    public class AccessoryCollection
    {
        private static int GENERIC_RING_PRICE = 500;

        private static readonly Dictionary<AccessoryKey, Accessory> AccessoryDictionary = GetAccessoryDictionary();

        public static Accessory GetAccessoryByKey(AccessoryKey key)
        {
            if (!AccessoryDictionary.ContainsKey(key))
            {
                return null;
            }
            else
            {
                return AccessoryDictionary[key].ShallowCopy();
            }
        }

        private static Dictionary<AccessoryKey, Accessory> GetAccessoryDictionary() => new Dictionary<AccessoryKey, Accessory>()
        {
            //Rings
            {AccessoryKey.ACCURACY_RING, new Accessory(
                "Accuracy Ring",
                "A ring that increases hit rate of attacks.",
                GENERIC_RING_PRICE,
                AccessoryKey.ACCURACY_RING,
                new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{ 0, 0, 10, 0, 0, 0, 0}
                ) 
            },
            {AccessoryKey.CRITICAL_RING, new Accessory(
                "Critical Ring",
                "A ring that increases critical-hit rate.",
                GENERIC_RING_PRICE,
                AccessoryKey.CRITICAL_RING,
                new int[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{ 0, 0, 0, 5, 0, 0, 0}
                )
            },
            {AccessoryKey.SPEED_RING, new Accessory(
                "Speed Ring",
                "A ring that increases speed.",
                GENERIC_RING_PRICE,
                AccessoryKey.SPEED_RING,
                new int[]{ 0, 0, 0, 0, 2, 0, 0, 0, 0},
                new int[]{ 0, 0, 0, 0, 0, 0, 0}
                )
            },
        };

        private AccessoryCollection() { }
    }
}
