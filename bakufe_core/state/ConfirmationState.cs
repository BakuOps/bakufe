﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state
{
    /// <summary>
    /// Abstract implementation of a confirmation state to standartize yes/no querys towards the player
    /// </summary>
    public abstract class ConfirmationState : CharacterStateBase
    {
        public ConfirmationState(BakuFERealm realm, Character character, string name)
            : base(realm, character, name)
        {
            AddComfirmationInputs();
        }

        public ConfirmationState(BakuFERealm realm, Character character, string name, string description) 
            : base(realm, character, name, description)
        {
            AddComfirmationInputs();
        }

        /// <summary>
        /// Adds the confirmation inputs to the AllowedInputs
        /// </summary>
        private void AddComfirmationInputs()
        {
            AllowedInput.Add(StateInputCommand.YES.command);
            AllowedInput.Add(StateInputCommand.Y.command);
            AllowedInput.Add(StateInputCommand.NO.command);
            AllowedInput.Add(StateInputCommand.N.command);
        }

        /// <summary>
        /// Handles the input and calls the right methods in case of a correct input.
        /// Inputing "Y(es)" calls HandlePositiveReply
        /// Inputing "N(o)" calls HandleNegativeReply
        /// </summary>
        /// <param name="io">The io obeject</param>
        public override void HandleInputValidCommand(IInputOutput io)
        {
            if(inputCommand.key == StateInputCommand.YES.key || inputCommand.key == StateInputCommand.Y.key)
            {
                HandlePositiveReply(io);
            }
            else
            {
                HandleNegativeReply(io);
            }
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            Description += IOPatterns.STATE_CONFIRM_YESNO_SUFFIX;
            base.ChangeToThisCharacterState(io);
        }

        /// <summary>
        /// Is called in case the user inputs a positive reply.
        /// Implement the things that the user agreed to here
        /// </summary>
        /// <param name="io">The io obeject</param>
        protected abstract void HandlePositiveReply(IInputOutput io);

        /// <summary>
        /// Is called in case the user inputs a negative reply.
        /// This usually means stepping back to the previos state or shifting into the follow up state without executing certain options
        /// </summary>
        /// <param name="io">The io obeject</param>
        protected abstract void HandleNegativeReply(IInputOutput io);
    }
}
