﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.util;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;
using Movementtype = bakufe_core.entities.enums.Movementtype;

namespace bakufe_test.entities
{
    [TestFixture]
    public class CharacterTest
    {

        [Test]
        public void CharacterTest_TestAttributePlusSkill()
        {
            int[] monkAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = monkAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.MONK)));

            //Nothing equipped yet => 8 MAG = 8 from MONK
            Assert.AreEqual(8, character.GetAttributeValue(Attribute.MAG));

            //Equip the MAG+2 Skill
            character.EquippedSkills[0] = SkillKey.MAG_PLUS;

            //Nothing equipped yet => 10 MAG = 8 from MONK + 2 from MAG+2
            Assert.AreEqual(10, character.GetAttributeValue(Attribute.MAG));
        }

        [Test]
        public void CharacterTest_TestUnarmedUpgrade()
        {
            int[] fighterAttributes = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER).BaseAttributes;

            Character character = new Character
            {
                Name = "Byleth",
                Attributes = fighterAttributes
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.FIGHTER)));

            //No Weapon Equipped => Unarmed Combat => Attack value = STR (9) + Weapon might (0) = 9
            Assert.AreEqual(9, CharacterUtil.CalculateAttackValue(character, Movementtype.INFANTRY));

            //"Equip" the unarmed combat skill
            character.EquippedSkills[0] = SkillKey.UNARMED_COMBAT_PLUS;

            //No Weapon Equipped => Unarmed Combat (but enhanced through the skill) => Attack value = STR (9) + Weapon might (3) = 12
            Assert.AreEqual(12, CharacterUtil.CalculateAttackValue(character, Movementtype.INFANTRY));
        }
    }
}
