﻿using bakufe_core.datasources.keys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bakufe_core.entities.util
{
    /// <summary>
    /// Util class for all logic involving the skills
    /// </summary>
    public class SkillUtil
    {
        /// <summary>
        /// The maximum amount of 
        /// </summary>
        public static readonly int MAX_EQUIPPED_SKILLS = 5;

        /// <summary>
        /// Checks if the character got the skill in question equipped or added via his class
        /// </summary>
        /// <param name="character">The given character</param>
        /// <param name="skill">The given skill</param>
        /// <returns>if the character got the skill in question equipped</returns>
        public static bool DoesCharacterHaveSkillEquipped(Character character, SkillKey skill)
        {
            return character.EquippedSkills.Contains(skill) || character.GetCurrentClass().ClassSkills.Contains(skill);
        }

        /// <summary>
        /// Returns the fitting SkillKey for the given Attribute bonus
        /// </summary>
        /// <param name="attribute">The attribute the bonus is asked for</param>
        /// <returns>the fitting SkillKey for the given Attribute bonus</returns>
        public static SkillKey GetFittingSkillForAttributeBonus(enums.Attribute attribute)
        {
            return attribute switch
            {
                enums.Attribute.HP => SkillKey.HP_PLUS,
                enums.Attribute.STR => SkillKey.STR_PLUS,
                enums.Attribute.MAG => SkillKey.MAG_PLUS,
                enums.Attribute.DEX => SkillKey.DEX_PLUS,
                enums.Attribute.SPD => SkillKey.SPD_PLUS,
                enums.Attribute.DEF => SkillKey.DEF_PLUS,
                enums.Attribute.RES => SkillKey.RES_PLUS,
                enums.Attribute.LCK => SkillKey.DUMMY,
                _ => SkillKey.DUMMY,
            };
        }

        /// <summary>
        /// Returns the attribute bonus for the given SkillKey
        /// </summary>
        /// <param name="skillKey">The given SkillKey</param>
        /// <returns>the attribute bonus for the given SkillKey</returns>        
        public static int GetAttributeBonusBySkillKey(SkillKey skillKey)
        {
            return skillKey switch
            {
                SkillKey.HP_PLUS => 5,
                SkillKey.STR_PLUS => 2,
                SkillKey.MAG_PLUS => 2,
                SkillKey.DEX_PLUS => 4,
                SkillKey.SPD_PLUS => 2,
                SkillKey.DEF_PLUS => 2,
                SkillKey.RES_PLUS => 2,
                _ => 0,
            };
        }
    }
}
