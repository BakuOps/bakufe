﻿using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_test.states
{
    /// <summary>
    /// IO-Object for state tests
    /// </summary>
    public class StateTestInputOutput : IInputOutput
    {
        public readonly List<string> inputs = new List<string>();
        public readonly List<string> outputs = new List<string>();
        private int nextInput = 0;

        public StateTestInputOutput()
        {

        }

        public string GetInput()
        {
            string newInput = inputs[nextInput];

            if (nextInput < inputs.Count - 1)
            {
                nextInput++;
            }

            return newInput;
        }

        public void PushEmptyLine()
        {
            //Do nothing
        }

        public void PushOutput(string output)
        {
            outputs.Add(output);
        }

        public void SetInput(string input)
        {
            //Do nothing
        }

        public bool DoesIOObjectContainString(string expectedString)
        {
            foreach (string outputline in outputs)
            {
                if (!string.IsNullOrEmpty(outputline) && outputline.Equals(expectedString))
                {
                    return true;
                }
            }

            return false;
        }

        public void Reset()
        {
            inputs.Clear();
            outputs.Clear();
            nextInput = 0;
        }

    }
}
