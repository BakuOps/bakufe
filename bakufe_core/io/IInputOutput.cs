﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.io
{
    public interface IInputOutput
    {
        /// <summary>
        /// Sets the current Input
        /// </summary>
        /// <returns>the player's input</returns>
        public void SetInput(string input);

        /// <summary>
        /// Returns the player's input
        /// </summary>
        /// <returns>the player's input</returns>
        public string GetInput();

        /// <summary>
        /// Queues up a line of input, that will be able to be displayed once the output is committed
        /// </summary>
        /// <param name="output">The text that will be queued up to be displayed</param>
        public void PushOutput(string output);

        /// <summary>
        /// Pushes an empty line into the output, mostly used for spacing
        /// </summary>
        public void PushEmptyLine();
    }
}
