﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.church
{
    /// <summary>
    /// This state represents a small "fluff action" for now
    /// 
    /// Maybe there will be a chance to aquire a buff later down the line. For now, it does nothing
    /// </summary>
    public class PrayingState : CharacterStateBase
    {
        public PrayingState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_CHURCH_NAME)
        {
            int val = realm.FERandom.Next(0, 10);

            string descpritionTemplate;

            if (val == 9)
            {
                descpritionTemplate = IOPatterns.STATE_PRAY_DESCRIPTION_4;
            }
            else if (val == 8)
            {
                descpritionTemplate = IOPatterns.STATE_PRAY_DESCRIPTION_3;
            }
            else if (val >= 5)
            {
                descpritionTemplate = IOPatterns.STATE_PRAY_DESCRIPTION_2;
            }
            else
            {
                descpritionTemplate = IOPatterns.STATE_PRAY_DESCRIPTION_1;
            }

            Description = string.Format(descpritionTemplate, character.Name);
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            base.ChangeToThisCharacterState(io);

            ChurchState churchState = new ChurchState(realm, character);
            churchState.ChangeToThisCharacterState(io);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            //Nothing to do here since this state changes straight back into the church state
        }
    }
}
