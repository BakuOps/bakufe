﻿using bakufe_core.datasources;
using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;

namespace bakufe_core.state.impl.shop
{
    /// <summary>
    /// This state is just a confirmation whether or not the character want's to buy the previously picked item
    /// </summary>
    public class ShopBuyConfirmState : ConfirmationState
    {
        private readonly Weapon weapon;
        private readonly bool isWeaponToExpensive = false;
        private readonly ShopBuyState followUpState;

        public ShopBuyConfirmState(BakuFERealm realm, Character character, Weapon weap, ShopBuyState buyState)
            : base(realm, character, IOPatterns.STATE_SHOP_BUY_CONFIRMATION_NAME)
        {
            weapon = weap;
            isWeaponToExpensive = weapon.Price > character.Gold;
            followUpState = buyState;

            if (isWeaponToExpensive)
            {
                Description = string.Format(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DESCRIPTION_NOT_ENOUGH_MONEY);
            }
            else
            {
                Description = string.Format(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DESCRIPTION, weapon.Name, weapon.Price);
            }

            Description += IOPatterns.STATE_CONFIRM_YESNO_SUFFIX;
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            if (isWeaponToExpensive)
            {
                ShopBuyState shopBuyState = new ShopBuyState(realm, character);
                ChangeCharacterToState(io, shopBuyState);
            }
            else
            {
                base.ChangeToThisCharacterState(io);
            }
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            io.PushOutput(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_DECLINE);
            ChangeCharacterToState(io, followUpState);
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            io.PushOutput(string.Format(IOPatterns.STATE_SHOP_BUY_CONFIRMATION_PURCHASE, character.Name, weapon.Name, weapon.Price));
            character.Gold -= weapon.Price;
            character.Inventory[InventoryUtil.GetNextFreeInventorySlot(character)] = WeaponCollection.GetWeaponByKey(weapon.WeaponKey);
            InventoryUtil.SortInventory(character);
            realm.AutosaveCharacterIfActivated(character);
            ChangeCharacterToState(io, followUpState);
        }
    }
}
