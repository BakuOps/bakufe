﻿using bakufe_core.entities;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.inventory
{
    /// <summary>
    /// Gives further information about the previously selected item
    /// </summary>
    public class InventoryDetailState : CharacterStateBase
    {
        /// <summary>
        /// The selected inventory slot
        /// </summary>
        private readonly int inventorySlot;

        /// <summary>
        /// The item that is hold in that slot
        /// </summary>
        private readonly ItemBase item;

        public InventoryDetailState(BakuFERealm realm, Character character, int slot)
            : base(realm, character, "")
        {
            inventorySlot = slot;
            item = character.Inventory[inventorySlot];

            // Add the back command
            AllowedInput.Add(StateInputCommand.BACK.command);

            Name = String.Format(IOPatterns.STATE_INVENTORY_DETAIL_NAME, character.Name, inventorySlot, item.Name);

            // In case the item is a Weapon or Accessory add the equip or unequip item,
            // depending on whether or not the item is already equipped
            if(item.GetType() == typeof(Weapon))
            {
                if(inventorySlot != character.EquippedWeaponSlot)
                {
                    AllowedInput.Add(StateInputCommand.EQUIP.command);
                }
                else
                {
                    AllowedInput.Add(StateInputCommand.UNEQUIP.command);
                }
            }
            else if (item.GetType() == typeof(Accessory))
            {
                if (inventorySlot != character.EquippedAccessorySlot)
                {
                    AllowedInput.Add(StateInputCommand.EQUIP.command);
                }
                else
                {
                    AllowedInput.Add(StateInputCommand.UNEQUIP.command);
                }
            }

            Description = item.GetFullItemDescription();
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (inputCommand.key == StateInputCommand.BACK.key)
            {
                InventoryOverviewState inventoryOverviewState = new InventoryOverviewState(realm, character);
                inventoryOverviewState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.EQUIP.key || inputCommand.key == StateInputCommand.UNEQUIP.key)
            {
                // Equip or Unequip item
                InventoryUtil.EquipOrUnequipItem(character, inventorySlot);
                InventoryOverviewState inventoryOverviewState = new InventoryOverviewState(realm, character);
                inventoryOverviewState.ChangeToThisCharacterState(io);
            }
        }
    }
}
