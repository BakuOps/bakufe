﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.enums
{
    public sealed class WeaponModification
    {
        #region Enum values

        //---------------------------------------------------
        // General modifications
        //---------------------------------------------------
        public static readonly WeaponModification BRAVE = new WeaponModification("BRAVE", "brave");
        public static readonly WeaponModification ADAPTIVE = new WeaponModification("ADAPTIVE", "adaptive");
        public static readonly WeaponModification VAMPIRIC = new WeaponModification("VAMPIRIC", "vampiric");

        //---------------------------------------------------
        // Effective against modifications
        //---------------------------------------------------
        public static readonly WeaponModification EFFECTIVE_AGAINST_INFANTRY = new WeaponModification("EFFECTIVE_AGAINST_INFANTRY", "effective against infantry");
        public static readonly WeaponModification EFFECTIVE_AGAINST_CAVALRY = new WeaponModification("EFFECTIVE_AGAINST_CAVALRY", "effective against cavalry");
        public static readonly WeaponModification EFFECTIVE_AGAINST_FLIERS = new WeaponModification("EFFECTIVE_AGAINST_FLIERS", "effective against fliers");
        public static readonly WeaponModification EFFECTIVE_AGAINST_ARMORED = new WeaponModification("EFFECTIVE_AGAINST_ARMORED", "effective against armored");

        #endregion

        public readonly string key;
        public readonly string readableName;

        private WeaponModification(string key, string readableName)
        {
            this.key = key;
            this.readableName = readableName;
        }

        public override String ToString()
        {
            return readableName;
        }

        public static WeaponModification[] GetValues()
        {
            return new WeaponModification[] {BRAVE, ADAPTIVE, EFFECTIVE_AGAINST_INFANTRY, EFFECTIVE_AGAINST_CAVALRY, EFFECTIVE_AGAINST_FLIERS, EFFECTIVE_AGAINST_ARMORED };
        }
    }
}
