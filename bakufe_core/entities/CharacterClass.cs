﻿using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    /// <summary>
    /// Represents a Class, the character can change into.
    /// Those classes decide the character's movementtype, give a baseline of attributes as well as passive boni,
    /// some skills that are active while the characters is classed into the given class as well as some
    /// Boni and Mali on certain stat growth rates
    /// </summary>
    public class CharacterClass
    {
        /// <summary>
        /// The class's display name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The classKey associated with this class,
        /// this is used as an identifier for the class type
        /// </summary>
        public CharacterClassKey CharacterClassKey { get; set; }

        /// <summary>
        /// The "level" of the class, setting requirements that need to be fullfilled
        /// for the character to attempt to aquire this class
        /// </summary>
        public CharacterClassLevel Level { get; set; }

        /// <summary>
        /// The class's movementtype
        /// </summary>
        public Movementtype Movementtype { get; set; }

        /// <summary>
        /// The minimum set of attributes associated with this class.
        /// If one or several of the character's attributes are below those values,
        /// they will be substituted with those values
        /// </summary>
        public int[] BaseAttributes { get; set; }

        /// <summary>
        /// A set of flat attribute boni that will be grated to the characters in this current class
        /// </summary>
        public int[] AttributeBonuses { get; set; }

        /// <summary>
        /// A set of chance increase and decreases, that will boost or reduce the growthchance of certain stats
        /// </summary>
        public int[] GrowthRates { get; set; }

        /// <summary>
        /// List of passive skills that are granted to the character while they are in this class
        /// </summary>
        public readonly List<SkillKey> ClassSkills = new List<SkillKey>();

        /// <summary>
        /// List of reward skills that are awarded once the class is mastered
        /// </summary>
        public readonly List<SkillKey> MasterySkills = new List<SkillKey>();

        /// <summary>
        /// A list of requirements to promote into this class
        /// </summary>
        public readonly List<ClassPromotionRequirement> ClassPromotionRequirements = new List<ClassPromotionRequirement>();

        #region Constructors

        public CharacterClass(
            string name, 
            CharacterClassLevel level, 
            CharacterClassKey characterClassKey, 
            Movementtype movementtype, 
            int[] baseAttributes, int[] 
            attributeBonuses, 
            int[] growthRates, 
            SkillKey[] classSkills,
            SkillKey[] masterySkills,
            ClassPromotionRequirement[] classPromotionRequirements
        )
        {
            Name = name;
            Level = level;
            CharacterClassKey = characterClassKey;
            Movementtype = movementtype;
            BaseAttributes = baseAttributes;
            AttributeBonuses = attributeBonuses;
            GrowthRates = growthRates;

            ClassSkills.AddRange(classSkills);
            MasterySkills.AddRange(masterySkills);
            ClassPromotionRequirements.AddRange(classPromotionRequirements);
        }

        public CharacterClass ShallowCopy()
        {
            return (CharacterClass)MemberwiseClone();
        }

        public override bool Equals(object obj)
        {
            return obj is CharacterClass @class &&
                   CharacterClassKey == @class.CharacterClassKey;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CharacterClassKey);
        }

        #endregion
    }
}
