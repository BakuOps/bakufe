﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state
{
    public class StateInputCommand
    {
        #region Enum values
        // Cancel command
        public static StateInputCommand CANCEL = new StateInputCommand("CANCEL", "cancel");
        // Cancel command
        public static StateInputCommand AVAILABLE_INPUTS = new StateInputCommand("AVAILABLE_INPUTS", "?");

        // Confirmation commands
        public static StateInputCommand Y = new StateInputCommand("Y", "y");
        public static StateInputCommand YES = new StateInputCommand("YES", "yes");
        public static StateInputCommand N = new StateInputCommand("N", "n");
        public static StateInputCommand NO = new StateInputCommand("NO", "no");

        //General commands
        public static StateInputCommand BACK = new StateInputCommand("BACK", "back");
        public static StateInputCommand LEAVE = new StateInputCommand("LEAVE", "leave");
        public static StateInputCommand SAVE = new StateInputCommand("SAVE", "save");
        public static StateInputCommand CHANGE_CLASS = new StateInputCommand("CHANGE_CLASS", "changeclass");
        public static StateInputCommand PROMOTE = new StateInputCommand("PROMOTE", "promote");
        public static StateInputCommand INVENTORY = new StateInputCommand("INVENTORY", "inventory");
        public static StateInputCommand STATUS = new StateInputCommand("STATUS", "status");

        //State specific commands
        public static StateInputCommand CHURCH = new StateInputCommand("CHRUCH", "church");
        public static StateInputCommand PRAY = new StateInputCommand("PRAY", "pray");
        public static StateInputCommand DONATE = new StateInputCommand("DONATE", "donate");
        public static StateInputCommand HOSPITAL = new StateInputCommand("HOSPITAL", "hospital");

        public static StateInputCommand SHOP = new StateInputCommand("SHOP", "shop");
        public static StateInputCommand BUY = new StateInputCommand("BUY", "buy");
        public static StateInputCommand SELL = new StateInputCommand("SELL", "sell");

        public static StateInputCommand EQUIP = new StateInputCommand("EQUIP", "equip");
        public static StateInputCommand UNEQUIP = new StateInputCommand("UNEQUIP", "unequip");

        public static StateInputCommand ADVENTURE = new StateInputCommand("ADVENTURE", "adventure");

        #endregion

        #region Enum methods

        public static StateInputCommand[] GetValues()
        {
            return new StateInputCommand[]
            {
                CANCEL,

                Y,
                YES,
                N,
                NO,

                BACK,
                LEAVE,
                SAVE,
                CHANGE_CLASS,
                PROMOTE,
                INVENTORY,
                STATUS,

                CHURCH,
                PRAY,
                DONATE,
                HOSPITAL,

                SHOP,
                BUY,
                SELL,

                EQUIP,
                UNEQUIP,

                ADVENTURE
            };
        }

        public static StateInputCommand GetInputCommandByCommand(string command)
        {
            foreach(StateInputCommand ipc in GetValues())
            {
                if(ipc.command.ToLower() == command)
                {
                    return ipc;
                }
            }

            return null;
        }

        #endregion

        public readonly string key;
        public readonly string command;

        public StateInputCommand(string key, string command)
        {
            this.key = key;
            this.command = command;
        }

        public override String ToString()
        {
            return command;
        }
    }
}
