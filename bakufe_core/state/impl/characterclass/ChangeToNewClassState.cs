﻿using bakufe_core.entities;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl.characterclass
{
    /// <summary>
    /// In case of a succesful promotion this state asks the player if they want to change their
    /// character into the freshly acquired class
    /// </summary>
    public class ChangeToNewClassState : ConfirmationState
    {
        /// <summary>
        /// Reference to the newly acquired class
        /// </summary>
        private readonly CharacterClass newlyAcquiredClass;

        public ChangeToNewClassState(BakuFERealm realm, Character character, CharacterClass acquiredClass)
            : base(realm, character, IOPatterns.STATE_PROMOTION_CONFIRMATION_CHANGE_NAME)
        {
            newlyAcquiredClass = acquiredClass;
            Description = string.Format(IOPatterns.STATE_PROMOTION_CONFIRMATION_CHANGE_DESCRIPTION, character.Name, newlyAcquiredClass.Name);
        }

        protected override void HandleNegativeReply(IInputOutput io)
        {
            io.PushOutput(string.Format(IOPatterns.STATE_CLASS_CHANGE_OPTION_NO_CHANGE_DONE, character.Name, character.GetCurrentClass().Name));
            ChangeCharacterToState(io, new IdleState(realm, character));
        }

        protected override void HandlePositiveReply(IInputOutput io)
        {
            int newIndex = character.AcquieredCharacterClasses.Count - 1;
            for(int i = 0; i < character.AcquieredCharacterClasses.Count; i++)
            {
                if(character.AcquieredCharacterClasses[i].Class.CharacterClassKey == newlyAcquiredClass.CharacterClassKey)
                {
                    newIndex = i;
                    break;
                }
            }
            realm.ChangeCharactersCurrentClass(io, character.ID, newIndex);
            ChangeCharacterToState(io, new IdleState(realm, character));
        }
    }
}
