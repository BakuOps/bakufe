﻿using bakufe_core.entities.enums;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.util
{
    public class WeaponUtil
    {
        /// <summary>
        /// Checks if the character is able to equip the weapon
        /// </summary>
        /// <param name="character">The character in question</param>
        /// <param name="weapon">The weapon in question</param>
        /// <returns>if the character is able to equip the weapon</returns>
        public static bool CanCharacterEquipWeapon(Character character, Weapon weapon)
        {
            return character.WeaponRanks[(int)weapon.Type].WeaponRank >= weapon.Rank;
        }

        /// <summary>
        /// Returns if the weapon contains the given modification
        /// </summary>
        /// <param name="weapon">The weapon in question</param>
        /// <param name="weaponModification">The modification in question</param>
        /// <returns>if the weapon contains the given modification</returns>
        public static bool DoesWeaponHaveModification(Weapon weapon, WeaponModification weaponModification)
        {
            foreach(WeaponModification modification in weapon.WeaponModifications)
            {
                if(modification.key == weaponModification.key)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the weapon is effective against the given movementtype
        /// </summary>
        /// <param name="weapon">The weapon in question</param>
        /// <param name="targetMovementtype">The defenders movementtype</param>
        /// <returns>if the weapon is effective against the given movementtype</returns>
        public static bool IsWeaponEffectiveAgainstMovementtype(Weapon weapon, Movementtype targetMovementtype)
        {
            if(targetMovementtype == Movementtype.INFANTRY)
            {
                return DoesWeaponHaveModification(weapon, WeaponModification.EFFECTIVE_AGAINST_INFANTRY);
            }
            else if(targetMovementtype == Movementtype.CAVALRY)
            {
                return DoesWeaponHaveModification(weapon, WeaponModification.EFFECTIVE_AGAINST_CAVALRY);
            }
            else if (targetMovementtype == Movementtype.FLIER)
            {
                return DoesWeaponHaveModification(weapon, WeaponModification.EFFECTIVE_AGAINST_FLIERS);
            }
            else if (targetMovementtype == Movementtype.ARMORED)
            {
                return DoesWeaponHaveModification(weapon, WeaponModification.EFFECTIVE_AGAINST_ARMORED);
            }

            return false;
        }

        /// <summary>
        /// Returns the readable name for the given weapon type
        /// </summary>
        /// <param name="type">the weapon type in question</param>
        /// <returns>the readable name for the given weapon type</returns>
        public static string GetReadableNameForWeaponType(WeaponType type)
        {
            return type switch
            {
                WeaponType.SWORD => IOPatterns.WEAPON_TYPE_SWORD,
                WeaponType.LANCE => IOPatterns.WEAPON_TYPE_LANCE,
                WeaponType.AXE => IOPatterns.WEAPON_TYPE_AXE,
                WeaponType.BOW => IOPatterns.WEAPON_TYPE_BOW,
                WeaponType.GAUNTLET => IOPatterns.WEAPON_TYPE_GAUNTLET,
                WeaponType.ANIMA => IOPatterns.WEAPON_TYPE_ANIMA,
                WeaponType.DARK => IOPatterns.WEAPON_TYPE_DARK,
                WeaponType.LIGHT => IOPatterns.WEAPON_TYPE_LIGHT,
                _ => IOPatterns.WEAPON_TYPE_SWORD
            };
        }
    }
}
