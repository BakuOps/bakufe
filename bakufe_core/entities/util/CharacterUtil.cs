﻿using bakufe_core.datasources;
using bakufe_core.entities.enums;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_core.entities.util
{
    /// <summary>
    /// Util class that handles the calculation of several character related values
    /// </summary>
    public class CharacterUtil
    {
        /// <summary>
        /// Calculates the character's attack speed through the following formular:
        /// 
        ///     Attack speed = SPD - (Weapon weight + Accessory weight - ( STR / 5)) 
        ///     
        /// The attack speed can never be greater than the character's speed though.
        /// In the case that:
        ///     (Weapon weight + Accessory weight - ( STR / 5) < 0
        /// it will be considered as 0 for the calculation
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns></returns>
        public static int CalculateAttackSpeed(Character character)
        {
            int speed = character.GetAttributeValue(Attribute.SPD);
            int combinedWeight = 0;

            Weapon weapon = InventoryUtil.GetEquippedWeapon(character);
            Accessory accessory = InventoryUtil.GetEquippedAccessory(character);

            if(weapon != null)
            {
                //Add the weapon's weight in case one is equipped
                combinedWeight += weapon.WeaponAttirbutes[(int)WeaponAttribute.WEIGHT];
            }
            //#2 Unarmed combat doesn't involve any extra weapon weight => it is irrelevant for the attack speed calculation

            if(accessory != null)
            {
                //Add the accessories's weight in case one is equipped
                combinedWeight += accessory.ComabtBoni[(int)WeaponAttribute.WEIGHT];
            }

            //Calculate the weight after the strength reduction
            int weightAfterStrengthReduction = combinedWeight - (character.GetAttributeValue(Attribute.STR) / 5);
            if (weightAfterStrengthReduction < 0)
            {
                //In case the weight is less than 0, normalize it to 0
                weightAfterStrengthReduction = 0;
            }

            return speed - weightAfterStrengthReduction;
        }

        /// <summary>
        /// Returns the character's hitvalue calculated like follows:
        /// 
        ///     - For physical weapons:
        ///         hit = Weapon hit + accessory hit + DEX
        ///     
        ///     - For magical weapons:
        ///         hit = Weapon hit + accessory hit + (DEX + LCK) / 2 (rounded down)
        /// 
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns>character's hitvalue</returns>
        public static int CalculateHitValue(Character character)
        {
            Accessory accessory = InventoryUtil.GetEquippedAccessory(character);
            Weapon weapon = InventoryUtil.GetEquippedWeapon(character);

            // #2 in case the character is not wielding a weapon, replace it with the default one
            if (weapon == null)
            {
                weapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
            }

            int hit = 0;

            hit += weapon.WeaponAttirbutes[(int)WeaponAttribute.HIT];
            if (accessory != null)
            {
                hit += accessory.ComabtBoni[(int)WeaponAttribute.HIT];
            }

            //TODO: Added skill modifier

            if (weapon.DamageType == WeaponDamageType.PHYSICAL)
            {
                hit += character.GetAttributeValue(enums.Attribute.DEX);
            }
            else
            {
                //Not physical => Magical
                hit += (character.GetAttributeValue(enums.Attribute.DEX) + character.GetAttributeValue(enums.Attribute.LCK)) / 2;
            }

            return hit;
        }

        /// <summary>
        /// Calculates the character's avoid value depending on incoming attack's damage type
        /// 
        ///     Calculation for physical damage:
        ///         Avoid = Attack Speed + Accessory bonus (not yet implemented)
        ///         
        ///     Calculation for magical damage:
        ///         Avoid = (SPD + LCK) / 2 + Accessory bonus
        /// </summary>
        /// <param name="character">the given character</param>
        /// <param name="damageType">The damage type of the incoming attack</param>
        /// <returns></returns>
        public static int CalculateAvoidValue(Character character, WeaponDamageType damageType)
        {
            int avoid = 0;
            Accessory accessory = InventoryUtil.GetEquippedAccessory(character);
            
            if(damageType == WeaponDamageType.PHYSICAL)
            {
                avoid += CharacterUtil.CalculateAttackSpeed(character);
                //Potential terrain bonus?
            }
            else
            {
                //Not physical => Magical
                avoid += (character.GetAttributeValue(Attribute.SPD) + character.GetAttributeValue(Attribute.LCK)) / 2;
            }

            if(accessory != null)
            {
               //TODO: Add accessory avoid bonus 
            }

            return avoid;
        }


        /// <summary>
        /// Calculates the character's attack value depending on the equipped weapon
        /// 
        ///     For physical wepaons:
        ///         Attack value = Weapon might + STR
        ///         
        ///     For magical weapons:
        ///         Attack value = Weapon might + MAG
        ///         
        /// </summary>
        /// <param name="attacker">the given character</param>
        /// <returns>the character's attack value depending on the equipped weapon</returns>
        public static int CalculateAttackValue(Character attacker, Movementtype defenderMovementtype)
        {
            int value = 0;
            Weapon weapon = InventoryUtil.GetEquippedWeapon(attacker);

            // #2 in case the character is not wielding a weapon, replace it with the default one
            if (weapon == null)
            {
                weapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
            }

            if (WeaponUtil.IsWeaponEffectiveAgainstMovementtype(weapon, defenderMovementtype))
            {
                value += weapon.WeaponAttirbutes[(int)WeaponAttribute.MIGHT] * 3;
            }
            else
            {
                value += weapon.WeaponAttirbutes[(int)WeaponAttribute.MIGHT];
            }

            if (weapon.DamageType == WeaponDamageType.PHYSICAL)
            {
                value += attacker.GetAttributeValue(Attribute.STR);
            }
            else
            {
                //Not physical => magical
                value += attacker.GetAttributeValue(Attribute.MAG);
            }

            return value;
        }

        /// <summary>
        /// Calculated the character's defense value depending on the type of incoming damage
        /// 
        ///     Protection (against physical damage) = DEF
        ///     
        ///     Resilliance (against magical damage) = RES
        ///     
        /// </summary>
        /// <param name="character">the given character</param>
        /// <param name="damageType">The type the character defends against</param>
        /// <returns>the character's defense value depending on the type of incoming damage</returns>
        public static int CalculateDefenseValue(Character character, Weapon attackingWeapon)
        {
            int value = 0;

            // #2 in case the character is not wielding a weapon, replace it with the default one
            if (attackingWeapon == null)
            {
                attackingWeapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
            }

            if (WeaponUtil.DoesWeaponHaveModification(attackingWeapon, WeaponModification.ADAPTIVE))
            {
                //In case the weapon is adaptive return the lower of the Protection and Resilliance
                int protection = character.GetAttributeValue(Attribute.DEF);
                int resilliance = character.GetAttributeValue(Attribute.RES);

                //Return the lower of the two stats
                if(protection <= resilliance)
                {
                    return protection;
                }
                else
                {
                    return resilliance;
                }
            }
            else if (attackingWeapon.DamageType == WeaponDamageType.PHYSICAL)
            {
                //Physical => Protection
                value += character.GetAttributeValue(Attribute.DEF);
                //TODO: Add skill boni
            }
            else
            {
                //Magical => Resilliance
                value += character.GetAttributeValue(Attribute.RES);
                //TODO: Add skill boni
            }

            return value;
        }

        /// <summary>
        /// Calculates the crit value of the characters attack by the following formular:
        /// 
        ///     Crit Value = Crit Weapon + ( DEX + LCK ) / 2
        /// </summary>
        /// <param name="character">the given character</param>
        /// <returns>the crit value of the characters attack</returns>
        public static int CalculateCritValue(Character character)
        {
            int value = 0;
            Weapon weapon = InventoryUtil.GetEquippedWeapon(character);

            // #2 in case the character is not wielding a weapon, replace it with the default one
            if (weapon == null)
            {
                weapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
            }

            if (weapon != null)
            {
                value += InventoryUtil.GetEquippedWeapon(character).WeaponAttirbutes[(int)WeaponAttribute.CRIT];
            }

            //TODO: Added skill bonuses

            value += (character.GetAttributeValue(Attribute.DEX) + character.GetAttributeValue(Attribute.LCK)) / 2;

            return value;
        }

        /// <summary>
        /// Calculates the character's crit avoid....Well for now it is just the luck stat
        /// </summary>
        /// <param name="character"> the given character</param>
        /// <returns>character's crit avoid....Well for now it is just the luck stat</returns>
        public static int CalculateCritAvoidValue(Character character)
        {
            int value = 0;
            value += character.GetAttributeValue(Attribute.LCK);
            //
            return value;
        }

        /// <summary>
        /// Returns the character's percentage of remaining HP
        /// </summary>
        /// <param name="character">the character in question</param>
        /// <returns>the character's percentage of remaining HP</returns>
        public static int GetRemainingHealthPercentage(Character character)
        {
            return (int)(Math.Round((double)character.CurrentHP / (double)character.GetAttributeValue(Attribute.HP), 2) * 100);
        }

    }
}
