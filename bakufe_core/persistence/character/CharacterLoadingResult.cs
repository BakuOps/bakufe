﻿using bakufe_core.entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.persistence.character
{
    public class CharacterLoadingResult
    {
        public string CharacterID { get; private set; }

        public bool Success { get; private set; }

        public Character LoadedCharacter { get; private set; }

        public List<string> ErrorList { get; private set; }

        public CharacterLoadingResult(string characterID, bool success, Character loadedCharacter, List<string> errorList)
        {
            CharacterID = characterID;
            Success = success;
            LoadedCharacter = loadedCharacter;
            ErrorList = errorList;
        }
    }
}
