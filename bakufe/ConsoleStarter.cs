﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_core.persistence.character;
using bakufe_core.state;
using bakufe_core.state.impl;
using bakufe_test;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace bakufe
{
    public class ConsoleStarter
    {
        private static bool isDebugModeEnabled = false;

        private static readonly string DEBUG_TEST_MODE_PARAM = "debtest";

        private static readonly BakuFERealm realm = BakuFERealmProvider.Realm;

        //Exit command
        private static readonly string COMMAD_EXIT = "e";
        
        //Game commands
        private static readonly string COMMAND_LOAD = "load";
        private static readonly string COMMAND_MAIN_MENU = "mainmenu";

        //Debug commands
        private static readonly string COMMAND_SAVE = "savechar";
        private static readonly string DEBUG_COMMAD_BATTLE = "simpleBattle";
        private static readonly string DEBUG_COMMAND_SAVETEST = "saveTest";
        private static readonly string DEBUG_COMMAND_LOADTEST = "loadTest";
        private static readonly string DEBUG_COMMAND_GENERATE_TESTFILES = "gtf";

        //The currently played character if there is a session running
        private static string currentCharacterID = "";

        static void Main(string[] args)
        {
            // Check for input parameters
            for(int i = 0; i < args.Length; i++)
            {
                if (!String.IsNullOrEmpty(args[i]))
                {
                    string arg = args[i];
                    if(arg == DEBUG_TEST_MODE_PARAM)
                    {
                        isDebugModeEnabled = true;
                    }
                }
            }

            //Update Command Window titl
            UpdateCommandWindowTitle();

            // Print welcome screen and initial outputs
            Console.WriteLine("#---------------------------------------------#");
            Console.WriteLine("|  ~~~  Baku FE - Commandline Interface  ~~~  |");
            Console.WriteLine("#---------------------------------------------#");

            if (isDebugModeEnabled)
            {
                Console.WriteLine("\n--- Debug Mode enabled! ---\n");
            }

            Console.WriteLine("Please enter your query:");

            // Handle the main input loop
            string input;
            while (true)
            {
                input = Console.ReadLine();

                // Check and handle exit
                if (input == COMMAD_EXIT)
                {
                    Environment.Exit(0);
                } 
                // ...if there is a game session running, save the character to the textfile...
                else if (isDebugModeEnabled && input.StartsWith(COMMAND_SAVE) && !String.IsNullOrEmpty(currentCharacterID))
                {
                    Character character = realm.GetCharacter(currentCharacterID);
                    if(character != null)
                    {
                        SaveCharacter(character);
                    }
                    continue;
                }
                // ...if there is no game session running check if the command wants to set up one...
                else if (input.StartsWith(COMMAND_LOAD))
                {
                    LoadCharacter(input);
                    continue;
                }
                // ...if there is a game session running check if the session shall be terminated...
                else if (input.StartsWith(COMMAND_MAIN_MENU))
                {
                    ClearCurrentCharacter();
                }
                // If the game is not exited, pass the command to the gamesession if there is one running
                else if (!String.IsNullOrEmpty(currentCharacterID))
                {
                    CharacterStateBase state = realm.GetCharactersCurrentState(currentCharacterID);

                    if(state != null)
                    {
                        IInputOutput io = realm.GetEmptyInputOutput();
                        io.SetInput(input);

                        state.HandleInput(io);
                    }

                    continue;
                }
                // ...Finally check if the command is one of the debug commands
                else if (isDebugModeEnabled && input.StartsWith(DEBUG_COMMAD_BATTLE))
                {
                    StartRandomBattle();
                }
                else if(isDebugModeEnabled && input.StartsWith(DEBUG_COMMAND_SAVETEST))
                {
                    PersistenceTest(input);
                }
                else if (isDebugModeEnabled && input.StartsWith(DEBUG_COMMAND_LOADTEST))
                {
                    LoadTest(input);
                }
                else if (isDebugModeEnabled && input.StartsWith(DEBUG_COMMAND_GENERATE_TESTFILES))
                {
                    TestCharacterSavefileGenerator.GenerateTestCharacterSavefiles();
                }
                else
                {
                    Console.WriteLine("Illegal input: " + input);
                }

                Console.WriteLine("Please enter your query:");
            }
        }

        private static void UpdateCommandWindowTitle()
        {
            string newTitle = string.Format("Baku FE (v{0})", Assembly.GetEntryAssembly().GetName().Version.ToString());

            Character character = realm.GetCharacter(currentCharacterID);

            if (!string.IsNullOrEmpty(currentCharacterID) && character != null)
            {
                newTitle += " - " + character.Name;
                Console.Title = newTitle;
            }

        }

        #region Game commands

        private static void LoadCharacter(string input)
        {
            string[] inputParts = input.Split(" ");

            if (inputParts.Length < 2)
            {
                Console.WriteLine("Illegal input: " + input);
                return;
            }

            string characterID = inputParts[1];

            Character character = realm.GetCharacter(characterID);
            if(character == null)
            {
                Console.WriteLine("There was an error loading the character with the ID " + characterID);
                return;
            }

            currentCharacterID = characterID;
            UpdateCommandWindowTitle();
        }

        private static void ClearCurrentCharacter()
        {
            Character character = realm.GetCharacter(currentCharacterID);
            if (character != null)
            {
                Console.WriteLine();
                Console.WriteLine(String.Format("Closing the game session with '{0}'", character.Name));
                Console.WriteLine();

                realm.RemoveCharacter(character.ID);
            }

            currentCharacterID = "";
            UpdateCommandWindowTitle();
        }

        #endregion

        #region Debug commands

        private static void SaveCharacter(Character character)
        {
            ICharacterPersistence persistence = new CharacterPersistenceTextfile(true);
            persistence.PersistCharacter(character.ID, character);
            Console.WriteLine(String.Format("{0}'s character Data got persisted.", character.Name));
        }

        private static void StartRandomBattle()
        {
            Console.WriteLine();
            Console.WriteLine("Start Battle!");
            Console.WriteLine();

            CharacterClassKey[] characterClasses = new CharacterClassKey[]{CharacterClassKey.BASE, CharacterClassKey.FIGHTER, CharacterClassKey.MONK, CharacterClassKey.MYRMIDON, CharacterClassKey.SOLDIER };

            Random random = new Random();

            CharacterClass firstClass = CharacterClassCollection.GetCharacterClassByKey(characterClasses[random.Next(0, characterClasses.Length)]);
            CharacterClass secondClass = CharacterClassCollection.GetCharacterClassByKey(characterClasses[random.Next(0, characterClasses.Length)]);

            Character firstCharacter, secondCharacter;
            firstCharacter = CreateRandomCharacter("Prime", firstClass);
            secondCharacter = CreateRandomCharacter("Sekunda", secondClass);

            Battle battle = new Battle(new Character[] { firstCharacter, secondCharacter }, new BakuFERandom());
            battle.ExecuteBattle(realm.GetEmptyInputOutput());

            Console.WriteLine("");
            Console.WriteLine("Battle finished!");
            Console.WriteLine("");
        }

        private static Character CreateRandomCharacter(string name, CharacterClass cClass)
        {
            Character character = new Character
            {
                Name = name,
                Attributes = cClass.BaseAttributes,
                Level = 1
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(cClass));

            character.CurrentHP = character.GetAttributeValue(bakufe_core.entities.enums.Attribute.HP);

            if (cClass.CharacterClassKey == CharacterClassKey.MONK)
            {
                character.Inventory[0] = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            }
            else
            {
                character.Inventory[0] = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            }
            InventoryUtil.EquipOrUnequipItem(character, 0);

            return character;
        }

        private static void PersistenceTest(String input)
        {
            string[] inputParts = input.Split(" ");

            if(inputParts.Length < 2)
            {
                Console.WriteLine("Illegal input: " + input);
                return;
            }

            string characterName = inputParts[1];
            CharacterClass cClass = CharacterClassCollection.GetCharacterClassByKey(CharacterClassKey.SOLDIER);

            Character character = new Character
            {
                Name = characterName,
                Attributes = cClass.BaseAttributes,
                ID = characterName + "ID",
                Level = 1,
                Gold = 1000
            };
            character.AcquieredCharacterClasses.Add(new AcquiredCharacterClass(cClass));

            character.CurrentHP = character.GetAttributeValue(bakufe_core.entities.enums.Attribute.HP);
            character.WeaponRanks[(int)WeaponType.AXE] = new WeaponHandling(WeaponType.AXE, WeaponRank.C);

            if (cClass.CharacterClassKey == CharacterClassKey.MONK)
            {
                character.Inventory[0] = WeaponCollection.GetWeaponByKey(WeaponKey.FIRE);
            }
            else
            {
                character.Inventory[0] = WeaponCollection.GetWeaponByKey(WeaponKey.IRON_SWORD);
            }
            ((Weapon)character.Inventory[0]).RemainingUses -= 13;
            InventoryUtil.EquipOrUnequipItem(character, 0);
            character.Inventory[3] = AccessoryCollection.GetAccessoryByKey(AccessoryKey.SPEED_RING);

            character.CurrentHP -= 5;

            character.EquippedSkills[0] = SkillKey.VANTAGE;
            character.LearnedSkills.Add(SkillKey.MAG_PLUS);
            character.LearnedSkills.Add(SkillKey.STR_PLUS);

            SaveCharacter(character);
        }

        private static void LoadTest(String input)
        {
            string[] inputParts = input.Split(" ");

            if (inputParts.Length < 2)
            {
                Console.WriteLine("Illegal input: " + input);
                return;
            }

            string characterID = inputParts[1];
            
            ICharacterPersistence persistence = new CharacterPersistenceTextfile(true);

            Character character = persistence.LoadCharacter(characterID);

            Console.WriteLine(String.Format("{0}'s character Data got loaded.", character.Name));

            Console.WriteLine(InventoryUtil.GetInventoryString(character));

            Console.WriteLine("");

        }

        #endregion
    }
}
