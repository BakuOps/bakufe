﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using bakufe_core.io.log;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    public class Battle
    {
        //Logger config
        private readonly ILogger logger = BakuFELogProvider.Logger;
        private readonly string logPrefix;

        /// <summary>
        /// Number of rounds the battle will last
        /// </summary>
        private static readonly int NUMBER_OF_COMABT_ROUNDS = 5;

        /// <summary>
        /// Default value, in case the battle is not determined yet
        /// </summary>
        private static readonly int NO_WINNER_DETERMINED_YET = -1;

        /// <summary>
        /// The two characters fighting each other, this will always be an array with two entrances for now
        /// </summary>
        public Character[] Characters { get; set; }

        /// <summary>
        /// Randomobject that is used for the calculations
        /// </summary>
        private readonly BakuFERandom bakuFERandom;

        /// <summary>
        /// Pointer targeting the slot of the character that has the initiative and therefore get's to act first
        /// Changes after each round of combat
        /// </summary>
        private int initiativePointer = 0;

        /// <summary>
        /// Points at the slot that contains the winner of this battle, once it was executed
        /// Until then, it points at the value set in <see cref="NO_WINNER_DETERMINED_YET"/>
        /// </summary>
        private int winner = NO_WINNER_DETERMINED_YET;

        /// <summary>
        /// Creates a battle object
        /// </summary>
        /// <param name="characters">The two characters that shall fight eachother</param>
        public Battle(Character[] characters, BakuFERandom fERandom)
        {
            Characters = characters;
            logPrefix = Characters[0].Name + Characters[1].Name;
            bakuFERandom = fERandom;

            // Check who get's to act first, in case of a tie the character in slot 0 get's to act first
            if (Characters[1].GetAttributeValue(enums.Attribute.SPD) > Characters[1].GetAttributeValue(enums.Attribute.SPD))
            {
                initiativePointer = 1;
            }
        }

        public void ExecuteBattle(IInputOutput io)
        {
            Weapon weapon1 = InventoryUtil.GetEquippedWeapon(Characters[0]);
            Weapon weapon2 = InventoryUtil.GetEquippedWeapon(Characters[1]);

            //Push initial output into IO
            io.PushOutput(String.Format(
                IOPatterns.BATTLE_VERSUS_STRING,
                Characters[0].Name,
                Characters[0].Level,
                Characters[0].GetCurrentClass().Name,
                weapon1.Name,
                Characters[1].Name,
                Characters[1].Level,
                Characters[1].GetCurrentClass().Name,
                weapon2.Name
            ));
            io.PushEmptyLine();

            Character attacker, defender;
            for(int i = 1; i <= NUMBER_OF_COMABT_ROUNDS; i++)
            {
                //Push round start output into IO
                io.PushOutput(string.Format(IOPatterns.BATTLE_ROUND_START, i));
                io.PushOutput(string.Format(
                    IOPatterns.BATTLE_ROUND_START_STATUS,
                    Characters[0].Name,
                    Characters[0].CurrentHP,
                    Characters[0].GetAttributeValue(enums.Attribute.HP),
                    Characters[1].Name,
                    Characters[1].CurrentHP,
                    Characters[1].GetAttributeValue(enums.Attribute.HP)
                ));
                io.PushEmptyLine();

                // -- Handle Round of combat
                // - First decide who is the attacker and who is the defender
                attacker = Characters[initiativePointer];
                defender = Characters[GetInvertedInitiativePointer()];
                Weapon attackerWeapon = InventoryUtil.GetEquippedWeapon(attacker);

                // #2 in case the character is not wielding a weapon, replace it with the default one
                if (attackerWeapon == null)
                {
                    attackerWeapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
                }

                bool isAttackerWeaponBrave = attackerWeapon != null && WeaponUtil.DoesWeaponHaveModification(attackerWeapon, WeaponModification.BRAVE);

                // #6 checks if Vantage is active
                if(SkillUtil.DoesCharacterHaveSkillEquipped(defender, SkillKey.VANTAGE) && CharacterUtil.GetRemainingHealthPercentage(defender) <= 50)
                {
                    // Vantage is active, the defender acts first
                    io.PushOutput(String.Format(IOPatterns.BATTLE_SKILL_ACTIVATION, defender, SkillCollection.SkillDictionary[SkillKey.VANTAGE].Name));
                    // Handle the defender's retaliation
                    if (BattleUtil.CanAttackOrRetatilate(this, defender, attacker))
                    {
                        // Execute the defenders retaliation in case they are able to do so
                        ExecuteAttackAction(io, defender, attacker);
                        if (IsBattleResolvedByCharacterDefeat(io))
                        {
                            // End the battle in case one of the characters is defeated
                            break;
                        }
                    }

                    // Handle the initial attack
                    if (BattleUtil.CanAttackOrRetatilate(this, attacker, defender))
                    {
                        //Execute the attackers attack in case they are able to do so
                        ExecuteAttackAction(io, attacker, defender);
                        if (IsBattleResolvedByCharacterDefeat(io))
                        {
                            // End the battle in case one of the characters is defeated
                            break;
                        }

                        if (isAttackerWeaponBrave)
                        {
                            //Execute 
                            ExecuteAttackAction(io, attacker, defender);
                            if (IsBattleResolvedByCharacterDefeat(io))
                            {
                                // End the battle in case one of the characters is defeated
                                break;
                            }
                        }
                    }
                }
                else
                {
                    // Business as usual
                    // Handle the initial attack
                    if (BattleUtil.CanAttackOrRetatilate(this, attacker, defender))
                    {
                        // Execute the attackers attack in case they are able to do so
                        ExecuteAttackAction(io, attacker, defender);
                        if (IsBattleResolvedByCharacterDefeat(io))
                        {
                            // End the battle in case one of the characters is defeated
                            break;
                        }

                        if (isAttackerWeaponBrave)
                        {
                            // Execute 
                            ExecuteAttackAction(io, attacker, defender);
                            if (IsBattleResolvedByCharacterDefeat(io))
                            {
                                // End the battle in case one of the characters is defeated
                                break;
                            }
                        }
                    }

                    // Handle the defender's retaliation
                    if (BattleUtil.CanAttackOrRetatilate(this, defender, attacker))
                    {
                        // Execute the defenders retaliation in case they are able to do so
                        ExecuteAttackAction(io, defender, attacker);
                        if (IsBattleResolvedByCharacterDefeat(io))
                        {
                            // End the battle in case one of the characters is defeated
                            break;
                        }
                    }
                }

                // Handle the potential follow up attack
                if (BattleUtil.CanAttackOrRetatilate(this, attacker, defender) && BattleUtil.IsAbleToDoFollowUpAttack(attacker, defender))
                {
                    // Execute the attackers attack in case they are able to do so
                    ExecuteAttackAction(io, attacker, defender);
                    if (IsBattleResolvedByCharacterDefeat(io))
                    {
                        // End the battle in case one of the characters is defeated
                        break;
                    }

                    if (isAttackerWeaponBrave)
                    {
                        // Execute 
                        ExecuteAttackAction(io, attacker, defender);
                        if (IsBattleResolvedByCharacterDefeat(io))
                        {
                            // End the battle in case one of the characters is defeated
                            break;
                        }
                    }
                }

                // Handle the defender's potential follow up retaliation
                if (BattleUtil.CanAttackOrRetatilate(this, defender, attacker) && BattleUtil.IsAbleToDoFollowUpRetaliation(defender, attacker))
                {
                    // Execute the defenders retaliation in case they are able to do so
                    ExecuteAttackAction(io, defender, attacker);
                    if (IsBattleResolvedByCharacterDefeat(io))
                    {
                        // End the battle in case one of the characters is defeated
                        break;
                    }
                }

                // -- Invert the initiative Pointer
                initiativePointer = GetInvertedInitiativePointer();

                //End of round: Push empty line into io
                io.PushEmptyLine();
            }

            // Once all the necessary rounds are resolved check if there is a winner yet
            if(winner == NO_WINNER_DETERMINED_YET)
            {
                ResolveBattleTimeout(io);
            }
        }

        /// <summary>
        /// !!This method is only public for testing reasons, don't use this outside of it!!
        /// 
        /// Compiles an attack with the following steps:
        ///     - Calculating the attacker's hit value and the defender's avoid value
        ///     - Doing the roll if the attack hits
        ///     - In case it does, calculated the attacker's defence or resilliance
        ///     - Calculating and applying the damage
        /// </summary>
        /// <param name="io">IO Object to push output into</param>
        /// <param name="attacker">The attacking character</param>
        /// <param name="defender">The defending character</param>
        public void ExecuteAttackAction(IInputOutput io, Character attacker, Character defender)
        {
            //Push attack output into io
            io.PushOutput(String.Format(IOPatterns.BATTLE_ATTACK_START, attacker.Name));

            Weapon weapon = InventoryUtil.GetEquippedWeapon(attacker);

            // #2 in case the character is not wielding a weapon, replace it with the default one
            if (weapon == null)
            {
                weapon = WeaponCollection.GetWeaponByKey(datasources.keys.WeaponKey.UNARMED);
            }

            //Damagetype is default physical in case of unarmed combat
            WeaponDamageType damageType = WeaponDamageType.PHYSICAL;
            if(weapon != null)
            {
                damageType = weapon.DamageType;
            }

            //Calculate the true hit value
            int hitValue = CharacterUtil.CalculateHitValue(attacker);
            int avoidValue = CharacterUtil.CalculateAvoidValue(defender, damageType);
            int trueHitValue = hitValue - avoidValue;

            //Roll the dices...
            int doubleRandomResult = bakuFERandom.RollDoubleRandom(0, 100);

            //...and check 
            bool hit = doubleRandomResult < trueHitValue;

            //Log hit calculation information
            logger.Information(
                IOPatterns.LOGGER_BATTLE_ATTACK_HIT_CALCULATION,
                logPrefix,
                hitValue,
                avoidValue,
                trueHitValue,
                hit
            );

            if (hit)
            {
                //In case the attack hits, check if it's a crit
                int crit = CharacterUtil.CalculateCritValue(attacker);
                int critAvoid = CharacterUtil.CalculateCritAvoidValue(defender);
                int critChance = crit - critAvoid;

                bool isCritical = false;
                if(critChance > 0)
                {
                    //In case there is a crit chance
                    int rollCrit = bakuFERandom.Next(0, 100);
                    if(rollCrit < critChance)
                    {
                        isCritical = true;
                        //In case of a critical push a special output
                        io.PushOutput(IOPatterns.BATTLE_ATTACK_CRITICAL_HIT);
                    }

                    //Log crit calculation information
                    logger.Information(
                        IOPatterns.LOGGER_BATTLE_ATTACK_CRIT_CALCULATION,
                        logPrefix,
                        crit,
                        critAvoid,
                        critChance,
                        rollCrit,
                        isCritical
                    );
                }

                //Calculate damage
                int attackValue = CharacterUtil.CalculateAttackValue(attacker, defender.GetCurrentClass().Movementtype);
                int defenseValue = CharacterUtil.CalculateDefenseValue(defender, weapon);
                int damage = attackValue - defenseValue;
                if(damage < 0)
                {
                    damage = 0;
                }

                if (isCritical)
                {
                    damage *= 3;
                }

                //Log damage calculation information
                logger.Information(
                    IOPatterns.LOGGER_BATTLE_ATTACK_DAMAGE_CALCULATION,
                    logPrefix,
                    attackValue,
                    defenseValue,
                    damage
                );

                //Subtract the damage from the defender's currentHP
                defender.CurrentHP -= damage;

                //Push damage output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_ATTACK_DAMAGE, attacker.Name, defender.Name, damage));

                //In case the weapon is vampiric, heal the attacker...
                if (weapon != null && WeaponUtil.DoesWeaponHaveModification(weapon, WeaponModification.VAMPIRIC))
                {
                    //...heal the attacker by half the damage cause rounded up
                    int healedHP = (int)(Math.Ceiling((double)damage / 2.0));
                    attacker.CurrentHP += healedHP;
                    //Push healing output into io
                    io.PushOutput(String.Format(IOPatterns.BATTLE_SKILL_VAMPIRIC_HEALING, attacker.Name, healedHP));
                }
            }
            else
            {
                //Push miss output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_ATTACK_MISS, attacker.Name));
            }
        }

        /// <summary>
        /// Checks if the battle got decided by one character getting defeated.
        /// In the rare occasion both characters get defeated in the same attack action, the current attackers wins the battle
        /// </summary>
        /// <returns>if the battle got decided by one character getting defeated</returns>
        private bool IsBattleResolvedByCharacterDefeat(IInputOutput io)
        {
            if(Characters[GetInvertedInitiativePointer()].CurrentHP <= 0)
            {
                winner = initiativePointer;

                //Push winning output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, Characters[GetInvertedInitiativePointer()].Name, Characters[initiativePointer].Name));

                return true;
            } 
            else if (Characters[initiativePointer].CurrentHP <= 0)
            {
                winner = GetInvertedInitiativePointer();

                //Push winning output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_CHARACTER_DEFEATED, Characters[initiativePointer].Name, Characters[GetInvertedInitiativePointer()].Name));

                return true;
            } 
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determines a winner in case the battle timed out
        /// </summary>
        private void ResolveBattleTimeout(IInputOutput io)
        {
            Character firstCharacter = Characters[0];
            Character secondCharacter = Characters[1];
            double firstCharacterHealthPercentageLeft = Math.Round(((double)firstCharacter.CurrentHP / (double)firstCharacter.Attributes[(int)enums.Attribute.HP]) * 100, 2);
            double secondCharacterHealthPercentageLeft = Math.Round(((double)secondCharacter.CurrentHP / (double)secondCharacter.Attributes[(int)enums.Attribute.HP]) * 100, 2) ;
            
            //Compare the character's percentage of health left, the one with the higher percentage left wins
            if(firstCharacterHealthPercentageLeft > secondCharacterHealthPercentageLeft)
            {
                winner = 0;
                //Push timeout output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_TIMEOUT, firstCharacterHealthPercentageLeft, firstCharacter.Name, secondCharacter.Name, secondCharacterHealthPercentageLeft));
            } 
            else if(firstCharacterHealthPercentageLeft < secondCharacterHealthPercentageLeft)
            {
                winner = 1;
                //Push timeout output into io
                io.PushOutput(String.Format(IOPatterns.BATTLE_TIMEOUT, secondCharacterHealthPercentageLeft, secondCharacter.Name, firstCharacter.Name, firstCharacterHealthPercentageLeft));
            }
            else
            {
                //In case of a true tie, the character in slot 0 wins the battle
                io.PushOutput(String.Format(IOPatterns.BATTLE_TIMEOUT_TRUE_TIE, firstCharacterHealthPercentageLeft, firstCharacter.Name));
                winner = 0;
            }
        }

        /// <summary>
        /// Returns the character that won the battle or null if the battle has not been resolved/executed yet
        /// </summary>
        /// <returns>the character that won the battle or null if the battle has not been resolved/executed yet</returns>
        public Character GetWinningCharacter()
        {
            if(winner == NO_WINNER_DETERMINED_YET)
            {
                return null;
            }
            else
            {
                return Characters[winner];
            }
        }

        /// <summary>
        /// Returns the inverted initiativePointer resulting in:
        /// Pointer = 0 => returns 1
        /// Pointer = 1 => returns 0
        /// </summary>
        /// <returns>the inverted initiativePointer</returns>
        private int GetInvertedInitiativePointer()
        {
            return (initiativePointer + 1) % 2;
        }
    }
}
