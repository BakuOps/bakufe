﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities.util
{
    public class BattleUtil
    {

        /// <summary>
        /// Datermines if the attacker is able to attack the defender
        /// </summary>
        /// <param name="attacker">the attacking character</param>
        /// <param name="defender">the defending character</param>
        /// <returns>if the attacker is able to attack the defender</returns>
        public static bool CanAttackOrRetatilate(Battle battle, Character attacker, Character defender)
        {
            //TODO: Implement proper checks, returns true for now
            //Possible factors could be distance vs range etc. 
            return true;
        }

        /// <summary>
        /// Datermines if the attacker is able to execute a follow up attack
        /// </summary>
        /// <param name="attacker">the attacking character</param>
        /// <param name="defender">the defending character</param>
        /// <returns>if the attacker is able to execute a follow up attack</returns>
        public static bool IsAbleToDoFollowUpAttack(Character attacker, Character defender)
        {
            //TODO: Add skills
            return BattleUtil.DoesInitiatorWinFollowUpSpeedcheck(attacker, defender);
        }

        /// <summary>
        /// Datermines if the defender is able to execute a follow up retaliation
        /// </summary>
        /// <param name="defender">the defending character</param>
        /// <param name="attacker">the attacking character</param>
        /// <returns>if the defender is able to execute a follow up retaliation</returns>
        public static bool IsAbleToDoFollowUpRetaliation(Character defender, Character attacker)
        {
            //TODO: Add skills
            return BattleUtil.DoesInitiatorWinFollowUpSpeedcheck(defender, attacker);
        }

        /// <summary>
        /// Compares the initiator's attackspeed to the defender's attack speed
        /// if the attacker has 4 or more attack speed more than the defender, the attacker wins the speed check
        /// </summary>
        /// <param name="initiator">the initiating character</param>
        /// <param name="target">the target character</param>
        /// <returns></returns>
        public static bool DoesInitiatorWinFollowUpSpeedcheck(Character initiator, Character target)
        {
            return CharacterUtil.CalculateAttackSpeed(initiator) - CharacterUtil.CalculateAttackSpeed(target) >= 4;
        }
    }
}
