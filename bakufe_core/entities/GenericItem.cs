﻿using bakufe_core.datasources.keys;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.entities
{
    public class GenericItem : ItemBase
    {
        public GenericItemKey GenericItemKey { get; private set; }

        public GenericItem(string name, string description, int maxStackSize, int price, GenericItemKey genericItemKey) :
            base(genericItemKey.ToString(), name, description, maxStackSize, price)
        {
            GenericItemKey = genericItemKey;
        }

        public GenericItem(string itemID, string name, string description, int currentStackSize, int maxStackSize, int price, GenericItemKey genericItemKey) :
            base (itemID, name, description, currentStackSize, maxStackSize, price)
        {
            GenericItemKey = genericItemKey;
        }

        public GenericItem ShallowCopy()
        {
            return (GenericItem)MemberwiseClone();
        }

        public override string GetItemStringRepresentation()
        {
            return String.Format("{0} ({1})", Name, CurrentStackSize);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
