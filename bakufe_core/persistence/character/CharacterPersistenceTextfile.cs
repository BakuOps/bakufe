﻿using bakufe_core.datasources;
using bakufe_core.datasources.keys;
using bakufe_core.entities;
using bakufe_core.entities.enums;
using bakufe_core.entities.util;
using bakufe_core.io;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using Attribute = bakufe_core.entities.enums.Attribute;

namespace bakufe_core.persistence.character
{
    public class CharacterPersistenceTextfile : ICharacterPersistence
    {

        public static readonly string DEFAULT_CHARACTER_PATH = "characters";
        public static readonly string FILE_TYPE = ".bfec";

        private readonly string directory;
        private readonly bool prettyPrint = false;

        #region Attribute Marker + Format Parameter

        private static readonly char ATTRIBUTE_SEPARATOR = ';';
        private static readonly char PREFIX_VALUE_SEPARATOR = ':';
        private static readonly char ARRAY_VALUE_SEPARATOR = ',';
        private static readonly char LIST_ITEM_ATTRIBUTE_SEPERATOR = '/';

        private static readonly string ATTRIBUTE_PREFIX_ID = "ID";
        private static readonly string ATTRIBUTE_PREFIX_NAME = "N";
        private static readonly string ATTRIBUTE_PREFIX_LEVEL = "L";
        private static readonly string ATTRIBUTE_PREFIX_EQUIPPED_WEAPON = "EW";
        private static readonly string ATTRIBUTE_PREFIX_EQUIPPED_ACCESSORY = "EA";
        private static readonly string ATTRIBUTE_PREFIX_CURRENT_HP = "CHP";
        private static readonly string ATTRIBUTE_PREFIX_CURRENT_EXP = "CEP";
        private static readonly string ATTRIBUTE_PREFIX_ATTRIBUTES = "A";
        private static readonly string ATTRIBUTE_PREFIX_WEAPONRANKS = "WR";
        private static readonly string ATTRIBUTE_PREFIX_EQUIPPED_SKILLS = "ES";
        private static readonly string ATTRIBUTE_PREFIX_LEARNED_SKILLS = "LS";
        private static readonly string ATTRIBUTE_PREFIX_CLASS = "C";
        private static readonly string ATTRIBUTE_PREFIX_AQUIRED_CLASSES = "AC";
        private static readonly string ATTRIBUTE_PREFIX_INVENTORY = "I";
        private static readonly string ATTRIBUTE_PREFIX_GOLD = "G";

        private static readonly string ATTRIBUTE_PREFIX_WEAPON = "W";
        private static readonly string ATTRIBUTE_PREFIX_ACCESORY = "AC";
        private static readonly string ATTRIBUTE_PREFIX_GENERICITEM = "GI";

        private static readonly int LEVEL_NOT_YET_EXTRACTED = 0;
        private static readonly int GOLD_NOT_YET_EXTRACTED = -1;
        private static readonly int CURRENT_HP_NOT_YET_EXTRACTED = -1;
        private static readonly int CURRENT_EXP_NOT_YET_EXTRACTED = -1;
        private static readonly int CURRENT_CLASS_INDEX_NOT_EXTRACTED = -1;
        private static readonly int EQUIPPED_SLOT_NOT_YET_EXTRACTED = -2;

        #endregion

        public CharacterPersistenceTextfile()
        {
            directory = DEFAULT_CHARACTER_PATH;
            Directory.CreateDirectory(directory);
            prettyPrint = false;
        }

        public CharacterPersistenceTextfile(bool prettyPrint)
        {
            directory = DEFAULT_CHARACTER_PATH;
            Directory.CreateDirectory(directory);
            this.prettyPrint = prettyPrint;
        }

        public CharacterPersistenceTextfile(string directoryName, bool prettyPrint)
        {
            directory = directoryName;
            Directory.CreateDirectory(directory);
            this.prettyPrint = prettyPrint;
        }

        public bool DoesCharacterExist(string characterId)
        {
            return File.Exists(directory + "/" + characterId + FILE_TYPE);
        }

        public Character LoadCharacter(string characterId)
        {
            if (DoesCharacterExist(characterId))
            {
                string characterDataString = File.ReadAllText(directory + "/" + characterId + FILE_TYPE);
                CharacterLoadingResult loadedCharacter = ConvertDataStringToCharacter(characterId, characterDataString);
                return loadedCharacter.LoadedCharacter;
            }
            else
            {
                return null;
            }
        }

        public List<Character> LoadAllCharacters()
        {
            string[] characterfiles = Directory.GetFiles(directory + "/");
            List<Character> result = new List<Character>();

            foreach (string filenamewithpath in characterfiles)
            {
                string file = Path.GetFileNameWithoutExtension(filenamewithpath);
                string rawJson = File.ReadAllText(filenamewithpath);
                Character loadedCharacter = JsonSerializer.Deserialize<Character>(rawJson);
                loadedCharacter.ID = file;
                result.Add(loadedCharacter);
            }
            return result;
        }

        public bool PersistCharacter(string characterId, Character character)
        {
            string characterString = GetCharacterPersistenceString(character);
            File.WriteAllText(directory + "/" + characterId + FILE_TYPE, characterString);
            return true;
        }

        #region Serialization

        /// <summary>
        /// Builds the string representation of the given character that will be written into the textfile the character will be persisted in
        /// </summary>
        /// <param name="character">The character in question</param>
        /// <returns>the string representation of the given character that will be written into the textfile the character will be persisted in</returns>
        private string GetCharacterPersistenceString(Character character)
        {
            string result = "";

            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_ID, character.ID);
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_NAME, character.Name);
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_LEVEL, character.Level.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_EQUIPPED_WEAPON, character.EquippedWeaponSlot.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_EQUIPPED_ACCESSORY, character.EquippedAccessorySlot.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_CURRENT_HP, character.CurrentHP.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_CURRENT_EXP, character.CurrentEXP.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_ATTRIBUTES, GetAttributeStringRepresentation(character.Attributes));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_WEAPONRANKS, GetWeaponranksStringRepresentation(character.WeaponRanks));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_EQUIPPED_SKILLS, GetEquippedSkillsStringRepresentation(character.EquippedSkills));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_LEARNED_SKILLS, String.Join(",", character.LearnedSkills));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_CLASS, character.CurrentClassIndex.ToString());
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_AQUIRED_CLASSES, GetAcquiredClassesString(character.AcquieredCharacterClasses));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_INVENTORY, GetInventroyString(character.Inventory));
            result += GetAttributeStringRepresentation(ATTRIBUTE_PREFIX_GOLD, character.Gold.ToString());

            return result;
        }

        /// <summary>
        /// Builds the string representation of one of the character's attributes
        /// Adds a linebreak at the end in case prettyPrint == true
        /// </summary>
        /// <param name="attributePrefix">The attribute's prefix</param>
        /// <param name="attributeValue">The attribute's value's string representation</param>
        /// <returns>the string representation of one of the character's attributes</returns>
        private string GetAttributeStringRepresentation(string attributePrefix, string attributeValue)
        {
            string baseString = attributePrefix + PREFIX_VALUE_SEPARATOR + attributeValue + ATTRIBUTE_SEPARATOR;

            if (prettyPrint)
            {
                baseString += "\n";
            }

            return baseString;
        }

        /// <summary>
        /// Creates a string representation of the character's current attributes that can be persisted
        /// </summary>
        /// <param name="attributes">The given character's attributes</param>
        /// <returns>a string representation of the character's current attributes</returns>
        private string GetAttributeStringRepresentation(int[] attributes)
        {
            string result = "";
            int attributeLength = attributes.Length;

            for (int i = 0; i < attributeLength; i++)
            {
                result += attributes[i].ToString();
                if (i < attributeLength - 1)
                {
                    result += ARRAY_VALUE_SEPARATOR;
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a string representation of the character's current weapon ranks that can be persisted
        /// </summary>
        /// <param name="weaponRanks">the character's current weapon ranks</param>
        /// <returns>a string representation of the character's current weapon ranks</returns>
        private string GetWeaponranksStringRepresentation(WeaponHandling[] weaponRanks)
        {
            string result = "";
            int attributeLength = weaponRanks.Length;

            for (int i = 0; i < attributeLength; i++)
            {
                WeaponHandling wph = weaponRanks[i];
                result += wph.WeaponType.ToString() + LIST_ITEM_ATTRIBUTE_SEPERATOR + wph.WeaponRank.ToString() + LIST_ITEM_ATTRIBUTE_SEPERATOR + wph.CurrentWeaponExperience.ToString();
                if (i < attributeLength - 1)
                {
                    result += ARRAY_VALUE_SEPARATOR;
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a string representation of the array of equipped skills that can be persisted
        /// </summary>
        /// <param name="equippedSkillKeys">the currently equipped skills</param>
        /// <returns>a string representation of the array of equipped skills</returns>
        private string GetEquippedSkillsStringRepresentation(SkillKey[] equippedSkillKeys)
        {
            string result = "";
            int attributeLength = equippedSkillKeys.Length;

            for (int i = 0; i < attributeLength; i++)
            {
                if(equippedSkillKeys[i] != SkillKey.DUMMY)
                {
                    result += equippedSkillKeys[i];
                }
                
                if (i < attributeLength - 1)
                {
                    result += ARRAY_VALUE_SEPARATOR;
                }
            }

            return result;
        }

        /// <summary>
        /// Creates the string representation of an AcquiredClass object that can be persisted
        /// </summary>
        /// <param name="acClass">the given AcquiredClass object</param>
        /// <returns>the string representation of an AcquiredClass object</returns>
        private string GetAcquiredClassRepresentation(AcquiredCharacterClass acClass)
        {
            return "" + acClass.Class.CharacterClassKey + LIST_ITEM_ATTRIBUTE_SEPERATOR + acClass.CurrentClassExperience + LIST_ITEM_ATTRIBUTE_SEPERATOR + acClass.IsMastered;
        }

        private string GetAcquiredClassesString(List<AcquiredCharacterClass> acquiredCharacterClasses)
        {
            string result = "";

            for(int i = 0; i < acquiredCharacterClasses.Count; i++)
            {
                result += GetAcquiredClassRepresentation(acquiredCharacterClasses[i]);
                
                if(i < acquiredCharacterClasses.Count - 1)
                {
                    result += ARRAY_VALUE_SEPARATOR;
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a string representation of the given inventory that can be persisted
        /// </summary>
        /// <param name="inventory">the given inventory</param>
        /// <returns>a string representation of the given inventory</returns>
        private string GetInventroyString(ItemBase[] inventory)
        {
            string result = "";

            ItemBase currentItem;
            for (int i = 0; i < inventory.Length; i++)
            {
                currentItem = inventory[i];
                if(currentItem != null)
                {
                    if (currentItem.GetType() == typeof(Weapon))
                    {
                        Weapon weapon = (Weapon)currentItem;
                        result += ATTRIBUTE_PREFIX_WEAPON + LIST_ITEM_ATTRIBUTE_SEPERATOR + weapon.WeaponKey + LIST_ITEM_ATTRIBUTE_SEPERATOR + weapon.RemainingUses;
                    }
                    else if (currentItem.GetType() == typeof(Accessory))
                    {
                        Accessory accessory = (Accessory)currentItem;
                        result += ATTRIBUTE_PREFIX_ACCESORY + LIST_ITEM_ATTRIBUTE_SEPERATOR + accessory.AccessoryKey;
                    }
                    else if (currentItem.GetType() == typeof(GenericItem))
                    {
                        GenericItem genericItem = (GenericItem)currentItem;
                        result += ATTRIBUTE_PREFIX_GENERICITEM + LIST_ITEM_ATTRIBUTE_SEPERATOR + genericItem.GenericItemKey + LIST_ITEM_ATTRIBUTE_SEPERATOR + genericItem.CurrentStackSize;
                    }
                }
                
                if (i < inventory.Length - 1)
                {
                    result += ARRAY_VALUE_SEPARATOR;
                }
            }

            return result;
        }

        #endregion

        #region Deserialization

        public CharacterLoadingResult ConvertDataStringToCharacter(string characterId, string dataString)
        {
            List<string> errorList = new List<string>();

            // Remove linebreaks from the string
            string replaceWith = "";
            string rawDataString = dataString.Replace("\r\n", replaceWith).Replace("\n", replaceWith).Replace("\r", replaceWith);

            //Split string into different lines
            string[] lines = rawDataString.Split(';');

            //Create placeholder variables
            string extractedName = "";
            string extractedID = "";
            int extractedLevel = LEVEL_NOT_YET_EXTRACTED;
            int extractedCurrentHP = CURRENT_HP_NOT_YET_EXTRACTED;
            int extractedCurrentEXP = CURRENT_EXP_NOT_YET_EXTRACTED;
            int extractedEquippedWeaponSlot = EQUIPPED_SLOT_NOT_YET_EXTRACTED;
            int extractedEquippedAccessorySlot = EQUIPPED_SLOT_NOT_YET_EXTRACTED;
            int extractedGold = GOLD_NOT_YET_EXTRACTED;
            int extractedCurrentClassIndex = CURRENT_CLASS_INDEX_NOT_EXTRACTED;
            int[] extractedAttributes = null;
            WeaponHandling[] extractedWeaponRanks = null;
            SkillKey[] extractedSkills = null;
            ItemBase[] extractedInventory = null;
            List<SkillKey> extractedLearnedSkills = new List<SkillKey>();
            List<AcquiredCharacterClass> extractedAcquiredCharacterClasses = new List<AcquiredCharacterClass>();

            //Loop over the lines and extract the fitting attributes
            foreach (string line in lines)
            {
                if (!line.Contains(PREFIX_VALUE_SEPARATOR))
                {
                    //Invalid input => ignore part for now
                    //TODO: Log error
                    continue;
                }

                if (line.StartsWith(ATTRIBUTE_PREFIX_NAME + PREFIX_VALUE_SEPARATOR))
                {
                    extractedName = line.Split(PREFIX_VALUE_SEPARATOR)[1];
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_ID + PREFIX_VALUE_SEPARATOR))
                {
                    extractedID = line.Split(PREFIX_VALUE_SEPARATOR)[1];
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_LEVEL + PREFIX_VALUE_SEPARATOR))
                {
                    extractedLevel = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_CURRENT_HP + PREFIX_VALUE_SEPARATOR))
                {
                    extractedCurrentHP = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_CURRENT_EXP + PREFIX_VALUE_SEPARATOR))
                {
                    extractedCurrentEXP = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_EQUIPPED_WEAPON + PREFIX_VALUE_SEPARATOR))
                {
                    extractedEquippedWeaponSlot = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_EQUIPPED_ACCESSORY + PREFIX_VALUE_SEPARATOR))
                {
                    extractedEquippedAccessorySlot = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_GOLD + PREFIX_VALUE_SEPARATOR))
                {
                    extractedGold = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_ATTRIBUTES + PREFIX_VALUE_SEPARATOR))
                {
                    extractedAttributes = ExtractAttributesFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_WEAPONRANKS + PREFIX_VALUE_SEPARATOR))
                {
                    extractedWeaponRanks = ExtractWeaponranksFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1], errorList);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_EQUIPPED_SKILLS + PREFIX_VALUE_SEPARATOR))
                {
                    extractedSkills = ExtractEquippedSkillsFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1], errorList);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_LEARNED_SKILLS + PREFIX_VALUE_SEPARATOR))
                {
                    extractedLearnedSkills.AddRange(ExtractLearnedSkillsFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1], errorList));
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_CLASS + PREFIX_VALUE_SEPARATOR))
                {
                    extractedCurrentClassIndex = Int32.Parse(line.Split(PREFIX_VALUE_SEPARATOR)[1]);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_AQUIRED_CLASSES + PREFIX_VALUE_SEPARATOR))
                {
                    extractedAcquiredCharacterClasses = ExtractAcquiredCharacterClassesFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1], errorList);
                    continue;
                }
                else if (line.StartsWith(ATTRIBUTE_PREFIX_INVENTORY + PREFIX_VALUE_SEPARATOR))
                {
                    extractedInventory = ExtractInventoryFromString(line.Split(PREFIX_VALUE_SEPARATOR)[1], errorList);
                    continue;
                }
            }

            //Once all the lines are read, check if the required fields are set
            bool success = true;
            if (String.IsNullOrEmpty(extractedName))
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Name", extractedName));
                success = false;
            }
            if (String.IsNullOrEmpty(extractedID))
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "ID", extractedID));
                success = false;
            }
            if (extractedLevel <= LEVEL_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Level", extractedLevel));
                success = false;
            }
            if (extractedCurrentHP <= CURRENT_HP_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Current HP", extractedCurrentHP));
                success = false;
            }
            if (extractedCurrentEXP <= CURRENT_EXP_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Current EXP", extractedCurrentHP));
                success = false;
            }
            if (extractedEquippedWeaponSlot <= EQUIPPED_SLOT_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Equipped Weapon slot", extractedEquippedWeaponSlot));
                success = false;
            }
            if (extractedEquippedAccessorySlot <= EQUIPPED_SLOT_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Equipped Accessory slot", extractedEquippedAccessorySlot));
                success = false;
            }
            if (extractedGold <= GOLD_NOT_YET_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Gold", extractedGold));
                success = false;
            }
            if (extractedCurrentClassIndex == CURRENT_CLASS_INDEX_NOT_EXTRACTED)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Class", "null"));
                success = false;
            }
            if (extractedAttributes == null || extractedAttributes.Length != Enum.GetNames(typeof(Attribute)).Length)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Attribute", extractedAttributes));
                success = false;
            }
            if (extractedWeaponRanks == null || extractedWeaponRanks.Length != Enum.GetNames(typeof(WeaponType)).Length)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Weaponranks", extractedWeaponRanks));
                success = false;
            }
            foreach(WeaponType wpt in Enum.GetValues(typeof(WeaponType)))
            {
                if(extractedWeaponRanks[(int)wpt] == null)
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Weaponrank for " + wpt.ToString(), extractedWeaponRanks));
                    success = false;
                }
            }

            if (extractedSkills == null || extractedSkills.Length != SkillUtil.MAX_EQUIPPED_SKILLS)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "EquippedSkills", extractedSkills));
                success = false;
            }
            if (extractedInventory == null || extractedInventory.Length != InventoryUtil.MAX_INVENTORY_SLOTS)
            {
                errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_OR_MISSING_VALUE, "Inventory", extractedInventory));
                success = false;
            }

            if(extractedEquippedWeaponSlot != InventoryUtil.NO_SLOT_AVAILABLE)
            {
                if(extractedInventory[extractedEquippedWeaponSlot] == null || extractedInventory[extractedEquippedWeaponSlot].GetType() != typeof(Weapon))
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_SLOT_DOESNT_MATCH_INVENTORY_STATE, "Equipped Weapon slot"));
                    success = false;
                }

            }

            if (extractedEquippedAccessorySlot != InventoryUtil.NO_SLOT_AVAILABLE)
            {
                if (extractedInventory[extractedEquippedAccessorySlot] == null || extractedInventory[extractedEquippedAccessorySlot].GetType() != typeof(Accessory))
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_SLOT_DOESNT_MATCH_INVENTORY_STATE, "Equipped Accessory slot"));
                    success = false;
                }
            }

            if (success)
            {
                //Finally...Create the character
                Character character = new Character(
                    extractedName,
                    extractedID,
                    extractedLevel,
                    extractedAttributes,
                    extractedCurrentClassIndex,
                    extractedWeaponRanks,
                    extractedSkills,
                    extractedAcquiredCharacterClasses,
                    extractedCurrentHP,
                    extractedCurrentEXP,
                    extractedEquippedWeaponSlot,
                    extractedEquippedAccessorySlot,
                    extractedGold
                );
                character.LearnedSkills.AddRange(extractedLearnedSkills);
                for (int i = 0; i < character.Inventory.Length; i++)
                {
                    character.Inventory[i] = extractedInventory[i];
                }

                return new CharacterLoadingResult(character.ID, true, character, errorList);
            }
            else
            {
                return new CharacterLoadingResult(characterId, false, null, errorList);
            }
            
        }

        private int[] ExtractAttributesFromString(string attributeData)
        {
            string[] stringData = attributeData.Split(ARRAY_VALUE_SEPARATOR);
            int[] result = new int[stringData.Length];

            for(int i = 0; i < stringData.Length; i++)
            {
                result[i] = Int32.Parse(stringData[i]);
            }

            return result;
        }

        private WeaponHandling[] ExtractWeaponranksFromString(string attributeData, List<string> errorList)
        {
            string[] stringData = attributeData.Split(ARRAY_VALUE_SEPARATOR);
            WeaponHandling[] result = new WeaponHandling[stringData.Length];


            for (int i = 0; i < stringData.Length; i++)
            {
                if (String.IsNullOrEmpty(stringData[i]))
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_WEAPONHANDLING_DATA, stringData[i]));
                    continue;
                }

                string[] handlingData = stringData[i].Split(LIST_ITEM_ATTRIBUTE_SEPERATOR);

                if(handlingData.Length >= 3) {
                    WeaponType wpt = (WeaponType)Enum.Parse(typeof(WeaponType), handlingData[0], true);
                    WeaponRank wpr = (WeaponRank)Enum.Parse(typeof(WeaponRank), handlingData[1], true);
                    if (Enum.IsDefined(typeof(WeaponType), wpt) && Enum.IsDefined(typeof(WeaponRank), wpr))
                    {
                        result[i] = new WeaponHandling(wpt, wpr, int.Parse(handlingData[2]));
                    }
                    else
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, stringData[i], typeof(WeaponRank).ToString()));
                        return null;
                    }
                }
                else
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_WEAPONHANDLING_DATA, stringData[i]));
                }
            }

            return result;
        }

        private SkillKey[] ExtractEquippedSkillsFromString(string attributeData, List<string> errorList)
        {
            string[] stringData = attributeData.Split(ARRAY_VALUE_SEPARATOR);
            SkillKey[] result = new SkillKey[stringData.Length];

            for (int i = 0; i < stringData.Length; i++)
            {
                if (String.IsNullOrEmpty(stringData[i]))
                {
                    //In case it is empty, we can skip the rest of the loop
                    continue;
                }

                SkillKey enumValue = (SkillKey)Enum.Parse(typeof(SkillKey), stringData[i], true);
                if (Enum.IsDefined(typeof(SkillKey), enumValue))
                {
                    result[i] = enumValue;
                }
                else
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, stringData[i], typeof(SkillKey).ToString()));
                    return null;
                }
            }

            return result;
        }

        private List<SkillKey> ExtractLearnedSkillsFromString(string attributeData, List<string> errorList)
        {
            string[] stringData = attributeData.Split(ARRAY_VALUE_SEPARATOR);
            List<SkillKey> result = new List<SkillKey>();

            for (int i = 0; i < stringData.Length; i++)
            {
                if (String.IsNullOrEmpty(stringData[i]))
                {
                    //In case it is empty, we can skip the rest of the loop
                    continue;
                }

                SkillKey enumValue = (SkillKey)Enum.Parse(typeof(SkillKey), stringData[i], true);
                if (Enum.IsDefined(typeof(SkillKey), enumValue))
                {
                    result.Add(enumValue);
                }
                else
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, stringData[i], typeof(SkillKey).ToString()));
                }
            }

            return result;
        }

        private List<AcquiredCharacterClass> ExtractAcquiredCharacterClassesFromString(string attributeData, List<string> errorList)
        {
            List<AcquiredCharacterClass> result = new List<AcquiredCharacterClass>();
            string[] stringData = attributeData.Split(ARRAY_VALUE_SEPARATOR);

            for(int i = 0; i < stringData.Length; i++)
            {
                if (String.IsNullOrEmpty(stringData[i]))
                {
                    //In case of an empty slot, skip the rest of the loop
                    continue;
                }

                string[] acquiredClassData = stringData[i].Split(LIST_ITEM_ATTRIBUTE_SEPERATOR);

                if (acquiredClassData.Length < 3)
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_ACQUIREDCLASS_DATA, stringData[i]));
                    continue;
                }

                bool isMastered = bool.Parse(acquiredClassData[2].ToLower());
                CharacterClassKey enumValue = (CharacterClassKey)Enum.Parse(typeof(CharacterClassKey), acquiredClassData[0], true);
                if (Enum.IsDefined(typeof(CharacterClassKey), enumValue))
                {
                    result.Add(new AcquiredCharacterClass(CharacterClassCollection.GetCharacterClassByKey(enumValue), int.Parse(acquiredClassData[1]), isMastered));
                }
                else
                {
                    errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, attributeData, typeof(CharacterClassKey).ToString()));
                }
            }

            return result;
        }

        private ItemBase[] ExtractInventoryFromString(string inventoryData, List<string> errorList)
        {
            string[] stringData = inventoryData.Split(ARRAY_VALUE_SEPARATOR);
            ItemBase[] inventory = new ItemBase[InventoryUtil.MAX_INVENTORY_SLOTS];

            for (int i = 0; i < stringData.Length; i++)
            {
                if (String.IsNullOrEmpty(stringData[i]))
                {
                    //In case of an empty slot, skip the rest of the loop
                    continue;
                }

                string[] itemData = stringData[i].Split(LIST_ITEM_ATTRIBUTE_SEPERATOR);
                if(itemData[0] == ATTRIBUTE_PREFIX_WEAPON)
                {
                    if(itemData.Length < 3)
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_ITEM_DATA, stringData[i]));
                        continue;
                    }

                    WeaponKey enumValue = (WeaponKey)Enum.Parse(typeof(WeaponKey), itemData[1], true);
                    if (Enum.IsDefined(typeof(WeaponKey), enumValue))
                    {
                        Weapon weapon = WeaponCollection.GetWeaponByKey(enumValue);
                        weapon.RemainingUses = Int32.Parse(itemData[2]);
                        inventory[i] = weapon;
                    }
                    else
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, itemData[1], typeof(WeaponKey).ToString()));
                    }
                }
                else if (itemData[0] == ATTRIBUTE_PREFIX_ACCESORY)
                {
                    if (itemData.Length < 2)
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_ITEM_DATA, stringData[i]));
                        continue;
                    }

                    AccessoryKey enumValue = (AccessoryKey)Enum.Parse(typeof(AccessoryKey), itemData[1], true);
                    if (Enum.IsDefined(typeof(AccessoryKey), enumValue))
                    {
                        Accessory accessory = AccessoryCollection.GetAccessoryByKey(enumValue);
                        inventory[i] = accessory;
                    }
                    else
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, itemData[1], typeof(AccessoryKey).ToString()));
                    }
                }
                else if (itemData[0] == ATTRIBUTE_PREFIX_GENERICITEM)
                {
                    if (itemData.Length < 3)
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_INVALID_ITEM_DATA, stringData[i]));
                        continue;
                    }

                    GenericItemKey enumValue = (GenericItemKey)Enum.Parse(typeof(GenericItemKey), itemData[1], true);
                    if (Enum.IsDefined(typeof(GenericItemKey), enumValue))
                    {
                        GenericItem genericItem = GenericItemCollection.GetGenericItemByKey(enumValue);
                        genericItem.CurrentStackSize = int.Parse(itemData[2]);
                        inventory[i] = genericItem;
                    }
                    else
                    {
                        errorList.Add(String.Format(IOPatterns.CHARACTER_LOADING_UNABLE_TO_PARSE_ENUM, itemData[1], typeof(AccessoryKey).ToString()));
                    }
                }
            }

            return inventory;
        }

        #endregion
    }
}
