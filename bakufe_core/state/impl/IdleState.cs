﻿using bakufe_core.entities;
using bakufe_core.io;
using bakufe_core.state.impl.adventure;
using bakufe_core.state.impl.characterclass;
using bakufe_core.state.impl.church;
using bakufe_core.state.impl.inventory;
using bakufe_core.state.impl.shop;
using System;
using System.Collections.Generic;
using System.Text;

namespace bakufe_core.state.impl
{
    public class IdleState : CharacterStateBase
    {
        public IdleState(BakuFERealm realm, Character character)
            : base(realm, character, IOPatterns.STATE_IDLE_NAME)
        {
            Description = String.Format(IOPatterns.STATE_IDLE_DESCRIPTION, character.Name);

            AllowedInput.Add(StateInputCommand.SAVE.command);
            AllowedInput.Add(StateInputCommand.CHURCH.command);
            AllowedInput.Add(StateInputCommand.SHOP.command);
            AllowedInput.Add(StateInputCommand.ADVENTURE.command);
            AllowedInput.Add(StateInputCommand.HOSPITAL.command);
            AllowedInput.Add(StateInputCommand.CHANGE_CLASS.command);
            AllowedInput.Add(StateInputCommand.PROMOTE.command);
            AllowedInput.Add(StateInputCommand.INVENTORY.command);
            AllowedInput.Add(StateInputCommand.STATUS.command);
        }

        public override void ChangeToThisCharacterState(IInputOutput io)
        {
            base.ChangeToThisCharacterState(io);
        }

        public override void HandleInputValidCommand(IInputOutput io)
        {
            if (inputCommand.key == StateInputCommand.ADVENTURE.key)
            {
                AdventureState adventureState = new AdventureState(realm, character);
                adventureState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.CHURCH.key)
            {
                ChurchState churchState = new ChurchState(realm, character);
                churchState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.SHOP.key)
            {
                ShopState shopState = new ShopState(realm, character, this);
                shopState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.SAVE.key)
            {
                SaveCharacterState saveState = new SaveCharacterState(realm, character);
                saveState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.HOSPITAL.key)
            {
                HospitalState hospitalState = new HospitalState(realm, character, this);
                hospitalState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.CHANGE_CLASS.key)
            {
                ChangeClassState changeClassState = new ChangeClassState(realm, character);
                changeClassState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.PROMOTE.key)
            {
                PromotionSelectionState promotionSelectionState = new PromotionSelectionState(realm, character);
                promotionSelectionState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.INVENTORY.key)
            {
                InventoryOverviewState inventoryOverviewState = new InventoryOverviewState(realm, character);
                inventoryOverviewState.ChangeToThisCharacterState(io);
            }
            else if (inputCommand.key == StateInputCommand.STATUS.key)
            {
                CharacterDetailState characterDetail = new CharacterDetailState(realm, character);
                characterDetail.ChangeToThisCharacterState(io);
            }
        }
    }
}
